import { AppRegistry } from 'react-native';
import FMSREACT from './src/FMSREACT';

//Forwards and Registers the FMSREACT.js (within src/) components
AppRegistry.registerComponent('FMSREACT', () => FMSREACT);