import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, KeyboardAvoidingView, Button } from 'react-native';

//import realm from 'realm';
import realm from 'realm';
import deepcopy from 'deepcopy';

/**
 * DBHelper allows other classes to easily access the Database.
 * It provides CRUD functionality:
 * - Adding, create a new row for the given table
 * - Removing, delete's a row based on its id
 * - Updating, finds a certain row and changes the information
 * - Getting, requests information
 * - Print, console.log's information
 * Also there is no instantiation needed for the DBHelper to work.
 * All you must do is import the DBHelper
 */
export default class DBHelper extends Component {
    constructor(props) {
        super(props);
    }

    //LATER USE: return realm.objects("Message").sorted('date', true);
    

    //------------------------------------
    //             Example
    //------------------------------------
    //Example Schema
    //schema: [{name: 'Dog', properties: {name: 'string'}}]

    //Example functions
    // static printDog() {
    //     console.log(this.DB.objects('Dog').length)
    // }

    // static addDog() {
    //     this.DB.write(() => {
    //         this.DB.create('Dog', {name: 'Rex'});
    //     });
    // }

    static runTests() {
        console.warn(
            '\n\n\n--------------------------------------------' +
            '\nRUNNING DATABASE TESTS' +
            '\n--------------------------------------------\n\n' +
            '\n Ignore all warnings during this time\n\n'
        );
        let compeleted = 0;
        let tests = 23;

        //user
        if (this.addUser('test', 'test213', 'test', 'test', 'test company', 23) != -1) {
            compeleted += 1;
            console.log('\n\n 1. Completed Test: ' + compeleted + ' of ' + tests + '\n');
        }

        console.warn(
            '\n\n\n--------------------------------------------' +
            '\nEND OF TESTS\n' +
            'Completed: ' + compeleted + ' of ' + tests +
            '\n--------------------------------------------\n\n'
        );
    }

    // EXAMPLE:
    //
    // const CarSchema = {
    //   name: 'Car',
    //   properties: {
    //     make:  'string',
    //     model: 'string',
    //     miles: {type: 'int', default: 0},
    //   }
    // };
    // const PersonSchema = {
    //   name: 'Person',
    //   properties: {
    //     name:     'string',
    //     birthday: 'date',
    //     cars:     {type: 'list', objectType: 'Car'},
    //     picture:  {type: 'data', optional: true}, // optional property
    //   }
    // };

    // // Initialize a Realm with Car and Person models
    // let realm = new Realm({ sche a: [CarSchema, PersonSchema] });


    /**
     * ------------------------------------------------------------
     *-------------------- USED FOR TABLE CHECKS ------------------
     * ------------------------------------------------------------
     */
    static Driver = {
        id: 'int',    // primary key
        username: 'string',
        password: 'string',
        phone: 'string',
        email: 'string',
        name: 'string',
        truck_id: 'int',
        office_id: 'string',
        route_id: 'int',
    }

    static Truck = {
        id: 'int',    // primary key
        compartments: 'int',
        checklist: 'string',
    }

    static Compartment = {
        id: 'int',    // primary key
        sequence: 'int',
        truck_id: 'int',
        fuel_id: 'int',
        allowed_capacity: 'int',
        current_amount: 'int',
    }

    static WorkOrder = {
        id: 'int',    // primary key
        order_number: 'int', //Work Order Identifier for FMS Database
        sequence: 'int',
        trip_id: 'int',
        location_id: 'int',
        vendor_id:'int',
        coordinate: 'string',
        before_note: 'string',
        start_note: 'string',
        end_note: 'string',
        workorder_note: 'string',
        workorder_note_sit1: 'int',
        workorder_note_sit2: 'int',
        workorder_note_sit3: 'int',
        driver_note: 'string',
        type: 'string',
        status: 'int',
        time_note: 'string',
        checklist: 'string',
        note: 'string',
        client_id: 'int',
        start_time: 'string',
        end_time: 'string'
    }

    static Documents = {
        id: 'int',

        workorder_id: 'int', //can be set
        compartment_id: 'int', //can be set
        type: 'string', //what type: photo or signature (workorder)
        data: 'string', //base64
        url: 'string', //internet link: only for photo
        trip_id: 'int',
    }

    static Task = {
        id: 'int',    // primary key
        workorder_id: 'int', // could be foreign key
        unit_number: 'int',
        amount: 'int',
        fill: 'int',
        fuel_id: 'int',
        status: 'string'
    }

    static Clients = {
        id: 'int',    // primary key
        name: 'string',
    }

    static Contacts = {
        id: 'int', //primary key
        client_id: 'int',
        contact_no: 'string',
        email: 'string'
    }

    static Fuel = {
        id: 'int',    // primary key
        name: 'string'
    }

    static Refinery_Locations = {
        id: 'int',    // primary key
        refinery_id: 'int',
        name: 'string',
        address: 'string'
    }

    static Log = {
        id: 'int',    // primary key
        user_id: 'int',
        trip_id: 'int',
        level: 'int',
        message: 'string',
        created_at: 'string'
    }

    static Trip = {
        id: 'int',    // primary key
        date: 'string',
        shift: 'string',
        route: 'string',
        driver_id: 'int',
        truck_id: 'int',
        note: 'string',
        status: 'string',
        active: 'int'
    }

    static Refinery = {
        id: 'int',    // primary key
        name: 'string'
    }

    static Checklist = {
        id: 'int',    // primary key
        type: 'string'
    }

    static Checkitems = {
        id: 'int',    // primary key
        list_id: 'int',
        description: 'string',
        type: 'string',
        sequence: 'int'
    }

//UNCOMMENT WHEN DUMMY API IS UPDATED!
    static Unit = {
        id: 'int',
        client_id: 'string',
        location_id: 'string',
        fuel_id: 'string',
        type_id: 'string',
        name: 'string',
        min_fuel: 'string',
        max_fuel: 'string',
        rfid: 'string'
    }

    static Unit_Types = {
        id: 'int',
        description: 'string'
    }
    static Vendor = {
        id: 'int',
        name: 'string',
        address: 'string'
    }
    static Locations = {
        id: 'int',
        client_id: 'int',
        name: 'string',
        address: 'string',
        postal_code: 'string'
    }
    static Dip_Chart = {
        id: 'int',
        compartment_id: 'int',
        truck_id: 'int',
        depth: 'int',
        volume: 'int'
    }
    static Trip_Checklist = {
        id: 'int',    // primary key
        checklist_id: 'int',
        trip_id: 'int',
        truck_id: 'int',
        notes: 'string'
    }
    static Trip_Checkitems = {
        id: 'int',    // primary key
        trip_checklist_id: 'int',
        checkitem_id: 'int',
        result: 'string'
    }

    static Inventory = {
        id: 'int',
        date: 'string',
        driver_id: 'int',
        trip_id: 'int',
        vendor_id: 'int',
        order_id: 'int',
        location_id: 'int',
        unit_id: 'int',
        task_id: 'int',
        truck_origin_id: 'int',
        compartment_origin_id: 'int',
        truck_id: 'int',
        compartment_id: 'int',
        dip_chart_id: 'int',
        fuel_id: 'int',
        operation: 'string',
        quantity: 'int'
    }



    // static Locations = {
    //     id: 'int',
    //     client_id: 'int',
    //     name: 'string',
    //     address: 'string',
    //     postal_code: 'string'
    // }


    //------------------------------------
    //    Database Table Definitions
    //------------------------------------
    static DB = new Realm({
        schema: [
            {
                //------------------------------------
                //            Driver Table
                //------------------------------------
                name: 'Driver',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    username: 'string',
                    password: 'string',
                    phone: 'string',
                    email: 'string',
                    name: 'string',
                    truck_id: 'int',
                    office_id: 'string',
                    route_id: 'int'
                }
            },
            {
                //------------------------------------
                //            Truck Table
                //------------------------------------
                name: 'Truck',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    compartments: 'int',
                    checklist: 'string'
                }
            },
            {
                //------------------------------------
                //        Compartment Table
                //------------------------------------
                name: 'Compartment',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    sequence: 'int',
                    truck_id: 'int',
                    fuel_id: 'int',
                    allowed_capacity: 'int',
                    current_amount: 'int',
                }
            },
            {
                //------------------------------------
                //         WorkOrder Table
                //------------------------------------
                name: 'WorkOrder',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    order_number: 'int', //Work Order Identifier for FMS Database
                    sequence: 'int',
                    trip_id: 'int',
                    location_id: 'int',
                    vendor_id:'int',
                    coordinate: 'string',
                    before_note: 'string',
                    start_note: 'string',
                    end_note: 'string',
                    workorder_note: 'string',
                    workorder_note_sit1: 'int',
                    workorder_note_sit2: 'int',
                    workorder_note_sit3: 'int',
                    driver_note: 'string',
                    type: 'string',
                    status: 'int',
                    time_note: 'string',
                    checklist: 'string',
                    note: 'string',
                    client_id: 'int',
                    start_time: 'string',
                    end_time: 'string'
                }
            },
            {
                //------------------------------------
                //         Documents Table
                //------------------------------------
                name: 'Documents',
                primaryKey: 'id',
                properties: {
                    id: 'int',

                    workorder_id: 'int', //can be set
                    compartment_id: 'int', //can be set
                    type: 'string', //what type: photo or signature (workorder)
                    data: 'string', //base64
                    url: 'string', //internet link: only for photo
                    trip_id: 'int', //can be set
                }
            },
            {
                //------------------------------------
                //           Task Table
                //------------------------------------
                name: 'Task',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    workorder_id: 'int', // could be foreign key
                    unit_number: 'int',
                    amount: 'int',
                    fill: 'int',
                    fuel_id: 'int',
                    status: 'string'
                }
            },
            {
                //------------------------------------
                //           Clients Table
                //------------------------------------
                name: 'Clients',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    name: 'string',
                    
                }
            },
            {
                //------------------------------------
                //           Contacts Table
                //------------------------------------
                name: 'Contacts',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    cient_id: 'int',
                    contact_no: 'string',
                    email: 'string'

                }
            },
            {
                //------------------------------------
                //            Fuel Table
                //------------------------------------
                name: 'Fuel',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    name: 'string'
                }
            },
            {
                //------------------------------------
                //      Refinery Locations Table
                //------------------------------------
                name: 'Refinery_Locations',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    refinery_id: 'int',
                    name: 'string',
                    address: 'string'
                }
            },
            {
                //------------------------------------
                //           Log Table
                //------------------------------------
                name: 'Log',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    user_id: 'int',
                    trip_id: 'int',
                    level: 'int',
                    message: 'string',
                    created_at: 'string'
                }
            },
            {
                //------------------------------------
                //           Trip Table
                //------------------------------------
                name: 'Trip',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    date: 'string',
                    shift: 'string',
                    route: 'string',
                    driver_id: 'int',
                    truck_id: 'int',
                    note: 'string',
                    status: 'string',
                    active: 'int',
//UNCOMMENT WHEN API IS UPDATED!
                    //complete: 'int,
                }
            },
            {
                //------------------------------------
                //           Refinery Table
                //------------------------------------
                name: 'Refinery',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    name: 'string'
                }
            },
            {
                //------------------------------------
                //           Checklist Table
                //------------------------------------
                name: 'Checklist',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    type: 'string'
                }
            },
            {
                //------------------------------------
                //           Checkitems Table
                //------------------------------------
                name: 'Checkitems',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    list_id: 'int',
                    description: 'string',
                    type: 'string',
                    sequence: 'int'
                }
            },

            {
                //------------------------------------
                //           Trip Checklist Table
                //------------------------------------
                name: 'Trip_Checklist',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    checklist_id: 'int',
                    trip_id: 'int',
                    truck_id: 'int',
                    notes: 'string'
                }
            },



            {
                //------------------------------------
                //           Trip Check Items Table
                //------------------------------------
                name: 'Trip_Checkitems',
                primaryKey: 'id',
                properties: {
                    id: 'int',    // primary key
                    trip_checklist_id: 'int',
                    checkitem_id: 'int',
                    result: 'string'
                }
            },
            {
                //------------------------------------
                //           Vendor Table
                //------------------------------------
                name: 'Vendor',
                primaryKey: 'id',
                properties: {
                    id: 'int',
                    name: 'string',
                    address: 'string'
                }
            },
            {
                //------------------------------------
                //           Location Table
                //------------------------------------
                name: 'Locations',
                primaryKey: 'id',
                properties: {
                    id: 'int',
                    client_id: 'int',
                    name: 'string',
                    address: 'string',
                    postal_code: 'string'
                }
            },
            {
                //------------------------------------
                //           Dip Chart Table
                //------------------------------------
                name: 'Dip_Chart',
                primaryKey: 'id',
                properties: {
                    id: 'int',
                    compartment_id: 'int',
                    truck_id: 'int',
                    depth: 'int',
                    volume: 'int'
                }
            },
            {
                //------------------------------------
                //           Unit Table
                //------------------------------------
                name: 'Unit',
                primaryKey: 'id',
                properties: {
                    id: 'int',
                    client_id: 'string',
                    location_id: 'string',
                    fuel_id: 'string',
                    type_id: 'string',
                    name: 'string',
                    min_fuel: 'string',
                    max_fuel: 'string',
                    rfid: 'string'
                }
            },
            {
                //------------------------------------
                //           Inventory Table
                //------------------------------------
                name: 'Inventory',
                primaryKey: 'id',
                properties: {
                    id: 'int',
                    date: 'string',
                    driver_id: 'int',
                    trip_id: 'int',
                    vendor_id: 'int',
                    order_id: 'int',
                    location_id: 'int',
                    unit_id: 'int',
                    task_id: 'int',
                    truck_origin_id: 'int',
                    compartment_origin_id: 'int',
                    truck_id: 'int',
                    compartment_id: 'int',
                    dip_chart_id: 'int',
                    fuel_id: 'int',
                    operation: 'string',
                    quantity: 'int'
                }
            },
            {
                //------------------------------------
                //           Unit_Types Table
                //------------------------------------
                name: 'Unit_Types',
                primaryKey: 'id',
                properties: {
                    id: 'int',
                    description: 'string',
                }
            }
        ]
    });

    //------------------------------------
    //          CRUD Functions
    //------------------------------------

    /**
     * Copies the array temple to be able to add the new row
     * into the database.
     *
     * NOTE: all integer will be parsed by parseInt(),
     * it will not add a row if the parseInt() fails.
     * Make sure you have the correct types.
     */
    static generateKeyArray(table, array) {
        try {
            var template;
            switch (table) {
                case 'Driver':
                    template = deepcopy(this.Driver);
                    break;
                case 'Clients':
                    template = deepcopy(this.Clients);
                    break;
                case 'Contacts':
                    template = deepcopy(this.Contacts);
                    break;
                case 'Compartment':
                    template = deepcopy(this.Compartment);
                    break;
                case 'Fuel':
                    template = deepcopy(this.Fuel);
                    break;
                case 'Refinery_Locations':
                    template = deepcopy(this.Refinery_Locations);
                    break;
                case 'Log':
                    template = deepcopy(this.Log);
                    break;
                case 'Trip':
                    template = deepcopy(this.Trip);
                    break;
                case 'Task':
                    template = deepcopy(this.Task);
                    break;
                case 'Truck':
                    template = deepcopy(this.Truck);
                    break;
                case 'WorkOrder':
                    template = deepcopy(this.WorkOrder)
                    break;
                case 'Refinery':
                    template = deepcopy(this.Refinery);
                    break;
                case 'Checklist':
                    template = deepcopy(this.Checklist);
                    break;
                case 'Checkitems':
                    template = deepcopy(this.Checkitems);
                    break;
                case 'Documents':
                    template = deepcopy(this.Documents);
                    break;
                case 'Unit':
                    template = deepcopy(this.Unit);
                    break;
                case 'Unit_Types':
                    template = deepcopy(this.Unit_Types);
                    break;
                case 'Vendor':
                    template = deepcopy(this.Vendor);
                    break;
                case 'Inventory':
                    template = deepcopy(this.Inventory);
                    break;


                case 'Locations':
                    template = deepcopy(this.Locations);
                    break;
                case 'Dip_Chart':
                    template = deepcopy(this.Dip_Chart);
                    break;
                case 'Trip_Checklist':
                    template = deepcopy(this.Trip_Checklist);
                    break;
                case 'Trip_Checkitems':
                    template = deepcopy(this.Trip_Checkitems);
                    break;
            }

            console.warn("number of keys: " + Object.keys(template).length);
            console.warn('size of array ' + array.length);
            count = 0
            for (var key in template) {
                if (array.length > count) {
                    if (template[key] == 'int') {
                        console.log('Converting: ' + array[count]);
                        template[key] = parseInt(array[count]);
                    } else {
                        template[key] = array[count];
                    }
                    count += 1;
                }
            }
            return template;
        } catch (error) {
            throw (error)
        }
    }

    static checkTable(table) {
        if (
            table == 'Driver' |
            table == 'Clients' |
            table == 'Contacts' |
            table == 'Compartment' |
            table == 'Fuel' |
            table == 'Refinery_Locations' |
            table == 'Log' |
            table == 'Trip' |
            table == 'Task' |
            table == 'Truck' |
            table == 'WorkOrder' |
            table == 'Refinery' |
            table == 'Checklist' |
            table == 'Checkitems' |
            table == 'Documents' |
            table == 'Unit' |
            table == 'Vendor' |
            table == 'Locations' |
            table == 'Inventory' |
            table == 'Dip_Chart' |
            table == 'Trip_Checklist' |
            table == 'Trip_Checkitems' |
            table == 'Unit_Types'
        ) {
            return true;
        } else {
            throw ('Invalid Table: ' + table);
        }



    }

    /**
     * Add new row data into a given table within
     * the database.
     *
     * NOTE: driver table auto increments its id.
     *
     * @param {String} table - Table to add the new data
     * @param {Array} array - New Row data
     */
    static add(table, array) {
        try {
            this.checkTable(table);

            if (table == 'Driver') {
                let id = this.DB.objects(table).length + 1;
                array[0] = id;
            }

            let keyValuedArray = this.generateKeyArray(table, array);
            
            this.DB.write(() => {
                this.DB.create(table, keyValuedArray);
            });
            console.warn('Added ' + table);
            return keyValuedArray['id'];
        } catch (error) {
            console.warn('Failed to add ' + table + '\n' + error);
            throw (error); //rethrowing
        }
    }

    /**
     * Add new row data into a given table within
     * the database.
     *
     * NOTE: driver table auto increments its id.
     *
     * @param {String} table - Table to add the new data
     * @param {Array} array - New Row data
     */
    static addDriver(table, data) {
        try {

            this.DB.write(() => {
                this.DB.create(table, {id:parseInt(data.id),username:data.username,password:data.password,
                    phone:data.phone,email:data.email,name:data.name,
                    truck_id:parseInt(data.truck_id),office_id:data.office_id,route_id:parseInt(data.route_id)});
            });
            //this.DB.write(() => {
             //   this.DB.create(table, array);
            //});
        } catch (error) {
            console.warn('Failed to addDriver:\n' + error);
            throw (error); //rethrowing
        }
    }
    /**
     * Add new row data into a given Trip table within
     * the database.
     *
     *
     * @param {String} table - Table to add the new data
     * @param {Array} array - New Row data
     */
    static addTrip(table, data) {
        try {

            this.DB.write(() => {
                this.DB.create(table, {"id":parseInt(data.id),"date":data.date,"shift":data.shift,"route":data.route_name,
                    "driver_id":parseInt(data.driver_id),"truck_id":parseInt(data.truck_id),"note":data.notes,"status":data.status,"active":0});
            });
            //this.DB.write(() => {
            //   this.DB.create(table, array);
            //});
        } catch (error) {
            console.warn('Failed to addTrip:\n' + error);
            throw (error); //rethrowing
        }
    }

    /**
     * Add new row data into a given WorkOrder table within
     * the database.
     *
     *
     * @param {String} table - Table to add the new data
     * @param {Array} array - New Row data
     */
    static addUnit(table, data) {
        try {

            this.DB.write(() => {
                this.DB.create(table, {"id":parseFloat(data.id),

                    'name': data.name,
                    'location': data.location,
                    'maximum': parseFloat(data.maximum),
                    'minimum': parseFloat(data.minximum),
                    'type': parseFloat(data.type),
                    'rfid': data.rfid});
            });
            //this.DB.write(() => {
            //   this.DB.create(table, array);
            //});

        } catch (error) {
            console.warn('Failed to addUnit:\n' + error);
            throw (error); //rethrowing
        }
    }

    /**
     * Add new row data into a given Log table within
     * the database.
     *
     *
     * @param {String} table - Table to add the new data
     * @param {Array} array - New Row data
     */
    static addLog(table, data) {
        try {

            this.DB.write(() => {
                this.DB.create(table, {"id":parseFloat(data.id),
                    'user_id': parseFloat(data.user_id),
                    'trip_id': parseFloat(data.trip_id),
                    'level': parseFloat(data.level),
                    'message': data.message,
                    'created_at': data.created_at
                });
            });
            //this.DB.write(() => {
            //   this.DB.create(table, array);
            //});

        } catch (error) {
            console.warn('Failed to addLog:\n' + error);
            throw (error); //rethrowing
        }
    }
    /**
     * Add new row data into a given WorkOrder table within
     * the database.
     *
     *
     * @param {String} table - Table to add the new data
     * @param {Array} array - New Row data
     */
    static strToNum(string) {
        if(string == ''){
            return 0;
        }else{
            return parseInt(string);
        }
    }
    /**
     * Add new row data into a given WorkOrder table within
     * the database.
     *
     *
     * @param {String} table - Table to add the new data
     * @param {Array} array - New Row data
     */
    static addWorkOrder(table, data) {


        try {

            this.DB.write(() => {
                this.DB.create(table, {"id":parseInt(data.id),
                    "order_number":this.strToNum(data.order_id),
                    "sequence":parseInt(data.sequence),
                    "trip_id":parseInt(data.trip_id),
                    "location_id":this.strToNum(data.location_id),
                    "vendor_id":this.strToNum(data.vendor_id),
                    "coordinate":data.coordinates,
                    "before_note":data.before_note,
                    "start_note":data.start_note,
                    "end_note":data.end_note,
                    "workorder_note":data.workorder_note,
                    "workorder_note_sit1":this.strToNum(data.workorder_note_sit1),
                    "workorder_note_sit2":this.strToNum(data.workorder_note_sit2),
                    "workorder_note_sit3":this.strToNum(data.workorder_note_sit3),
                    "driver_note":data.driver_note,
                    "type":data.type,
                    "status":this.strToNum(data.status),
                    "time_note":data.delivery_time_note,
                    "checklist":"",
                    "note":'',
                    "client_id":1,
                    "start_time":data.start_time,
                    "end_time":data.end_time});
            });
            //this.DB.write(() => {
            //   this.DB.create(table, array);
            //});

        } catch (error) {
            console.warn('Failed to addWorkOrder Test:\n' + error);
            throw (error); //rethrowing
        }


    }
     /* Removes a row form the given table by the array id.
     *
     * @param {String} table - Table to remove row
     * @param {Int} id - internal array id
     */
    static remove(table, id) {
        try {
            this.checkTable(table);

            let data = this.DB.objects(table)[id];

            this.DB.write(() => {
                this.DB.delete(data);
                console.log('Removed ' + table + ': ' + id)
            });
        } catch (error) {
            console.warn('Failed to remove' + table + ': ' + id + '\n' + error);
            throw (error); //rethrowing
        }
    }

    /**
     * Removes a row from the given table by the id within the row data.
     *
     * @param {String} table - Table to remove row 
     * @param {Int} id - Id of the row data
     */
    static removeById(table, id) {
        try {
            this.checkTable(table);

            let data = this.DB.objects(table).filtered('id = ' + id)[0];

            this.DB.write(() => {
                this.DB.delete(data);
                console.log('Removed ' + table + ': ' + id)
            });
        } catch (error) {
            console.warn('Failed to remove' + table + ': ' + id + '\n' + error);
            throw (error); //rethrowing
        }
    }

    /**
     * Removes all the rows from the given table.
     *
     * @param {String} - Table to remove all rows
     */
    static removeAll(table) {
        try {
            this.checkTable(table);

            let users = this.DB.objects(table);
            let length = this.DB.objects(table).length
            if (length > 0) {
                this.DB.write(() => {
                    for (let i = 0; i < length; i++) {
                        console.log('Length Before: ' + this.DB.objects(table).length);
                        this.DB.delete(users[0]);
                        console.log('Removed ' + table + ': ' + i);
                        console.log('Length After: ' + this.DB.objects(table).length);
                    }
                });
            } else {
                console.log('Nothing to remove');
                console.log('Table Length: ' + this.DB.objects(table).length);
            }
        } catch (error) {
            console.warn('Failed to remove all ' + table + '\n' + error);
            throw (error); //rethrowing
        }
    }

    //updates by internal array id
    static update(table, id, array) {
        try {
            this.checkTable(table);

            let data = this.DB.objects(table)[id];
            count = 1;
            this.DB.write(() => {
                for (key in data) {
                    if (key != 'id') {
                        data[key] = array[count];
                        count += 1;
                    }
                }
            });
            console.warn('Updated ' + table + ': ' + id);
        } catch (error) {
            console.warn('Failed to update ' + table + ': ' + id + '\n' + error);
            throw (error); //rethrowing
        }
    }

    //updates by id of the row data
    static updateById(table, id, array) {
        try {
            this.checkTable(table);

            let data = this.DB.objects(table).filtered('id = ' + id)[0];
            count = 1;
            this.DB.write(() => {
                for (key in data) {
                    if (key != 'id') {
                        data[key] = array[count];
                        count += 1;
                    }
                }
            });
            console.warn('Updated ' + table + ': ' + id);
        } catch (error) {
            console.warn('Failed to update ' + table + ': ' + id + '\n' + error);
            throw (error); //rethrowing
        }
    }

    //updates by id of the row data and by a column
    static updateElementAndById(table, id, element, value) {
        try {
            this.checkTable(table);

            let data = this.DB.objects(table).filtered('id = ' + id)[0];
            this.DB.write(() => {
                for (key in data) {
                    if (key != 'id' && key == element) {
                        data[key] = value;
                    }
                }
            });
            console.warn('updateElementAndById---Updated ' + table + ': ' + id + ' : ' + element);
        } catch (error) {
            console.warn('Failed to update ' + table + ': ' + id + '\n' + error);
            throw (error); //rethrowing
        }
    }

    //updates by id of the row data and updates a trip's active column
    static updateTripActiveById(id, active) {
        let table = 'Trip'
        try {
            this.checkTable(table);

            let data = this.DB.objects(table).filtered('id = ' + id)[0];
            count = 1;
            this.DB.write(() => {
                data['active'] = active
            });
            console.warn('Updated ' + table + ': ' + id);
        } catch (error) {
            console.warn('Failed to update ' + table + ': ' + id + '\n' + error);
            throw (error); //rethrowing
        }
    }
    //updates by id of the row data and updates a trip's active column
    static updateCompartmentFuel(id, amount) {
        let table = 'Compartment'
        try {
            this.checkTable(table);

            let data = this.DB.objects(table).filtered('id = ' + id)[0];
            count = 1;
            this.DB.write(() => {
                data['current_amount'] = parseInt(amount)
            });
            console.warn('Updated ' + table + ': ' + id);
        } catch (error) {
            console.warn('Failed to update ' + table + ': ' + id + '\n' + error);
            throw (error); //rethrowing
        }
    }
    //updates Driver by id of the row data and updates a TruckID's active column
    static updateTruckById(id, truck_number) {
        let table = 'Driver'
        try {
            this.checkTable(table);

            let data = this.DB.objects(table).filtered('id = ' + id)[0];
            data['truck_id'] = parseFloat(truck_number);
            this.DB.write(() => data);
            console.warn('Updated truck_id:' + truck_number);
        } catch (error) {
            console.warn('Failed to update ' + table + ': ' + truck_number+ '\n' + error);
            throw (error); //rethrowing
        }
    }

    //get by internal array id
    static get(table, id) {
        try {
            this.checkTable(table);
            let data = this.DB.objects(table)[id];
            return data;
        } catch (error) {
            console.warn('Failed to get ' + table + ': ' + id + '\n' + error);
            throw (error); //rethrowing
        }
    }

    //get by id of the row data
    static getById(table, id) {
        try {
            let data = this.DB.objects(table).filtered('id = ' + id)[0];
            return data;
        } catch (error) {
            console.warn('Failed to get ' + table + ': ' + id + '\n' + error);
            throw (error); //rethrowing
        }
    }

    //get all rows in the given table.
    static getAll(table) {
        try {
            this.checkTable(table);
            let data = this.DB.objects(table).sorted('id');
            return data;
        } catch (error) {
            console.warn('Failed to get all ' + table + '\n' + error);
            throw (error); //rethrowing
        }
    }

    static print(table, id) {
        let user = this.get(table, id);
        console.log(user);
    }

    static printAll(table) {
        let users = this.getAll(table);
        console.log(users);
    }

}