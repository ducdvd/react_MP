import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

//import Splash from './src/components/Splash/Splash'

/*
    Added React Native Library that allows the application
    to route to different views/components of the mobile
    application. Default first view is Login.js (within components/)
*/
import { App } from './config/router';

/**
 * All of the view inside of the application
 *
 * NOTE: Views must be added to config->route.js file
 * before displaying it (Using both <App /> or <{specific view} />)
 */

import Scanning from './components/WorkOrder/Scanning/Scanning';
import Manual from './components/WorkOrder/Manual/Manual';
import WorkOrder from './components/WorkOrder/WorkOrder';
import Settings from './components/Settings/Settings';
import Refresh from './components/Refresh/Refresh';
import About from './components/Settings/About/About';
import Truck from './components/Truck/Truck';
import Checklist from './components/Checklist/Checklist';
import AddingFuel from './components/Truck/Fueling/AddingFuel';
import Change from './components/Truck/Change/Change';
import Details from './components/WorkOrder/Details/Details';
import Signature from './components/WorkOrder/Signature/Signature';
import Note from './components/WorkOrder/Note/Note';
import Print from './components/Print/Print';

export default class FMSREACT extends Component {
    render() {
        return (
            /**
             * MUST: include all parent (view).js into the route.js
             * or the application will not work.
             * MUST: wrap all (view).js compents inside a <View></View>
             */

            <App />
            

            //------------------------------
            // Displays specific view.
            // Used or UI Design Development
            //------------------------------

            //<Print />
            //<Refresh />
            //<About />
            //<Truck />
            //<WorkOrder />
            //<Settings />
            //<AddingFuel />
            //<Scanning />
            //<Details />
            //<Change />
            //<Note />
            //<Checklist />
            //<Signature />

            //<Manual />


            //SAMPLE EXAMPLE:            
            // <View style={styles.container}>
            //   <Text style={styles.welcome}>
            //     Welcome to React Native!
            //   </Text>
            //   <Text style={styles.instructions}>
            //     To get started, edit index.ios.js
            //   </Text>
            //   <Text style={styles.instructions}>
            //     Press Cmd+R to reload,{'\n'}
            //     Cmd+D or shake for dev menu
            //   </Text>
            // </View>
        );
    }
}

//To be removed later in development
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});