import React, { Component}  from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, TouchableOpacity, Geolocation, Alert, TextInput, KeyboardAvoidingView, ScrollView, Keyboard } from 'react-native';

import ModalDropdown from 'react-native-modal-dropdown';
import { CheckboxField, Checkbox } from 'react-native-checkbox-field';
import renderIf from '../../config/renderif';
import DBHelper from '../../DBHelper';
import Header from './Header';


/**
 * Parent Component
 *
 * The Checklist component extends the react component and dynamically displays either the work order or
 * truck checklists specified within the checklist sqlite table.
 *
 * @author Duc Do
 */
export default class Checklist extends Component {

    /**
     * Defines the state and calls certain function before renderingthe view.
     *
     * @param {object} props - Contains navigation methods and information about previous component
     */
    constructor(props) {
        super(props)

        this.state = { //when component view is rendered use setState({})
            workorder_id: null, //is set if checklist table has it
            checklist_id: null, //is set if checklist table has it
            items: null, //REMOVE
            new_items: null, //all items within a checklist
            text: null, //RENAME: to note
        };
        this.getChecklist() //looks for checklist in database
        this.getItems() //looks for items of checklist in database
    }

    /**
     * Checks the navigation.state.params for either workorder or truck.
     * If it is successful it will store the information within the state.
     * It gets the information about the checklist via. Database (Realm)
     *
     * @throws - Failed to recieve checklist type params within state
     */
    getChecklist() {
        try {
            if (this.props.navigation.state.params != null) { //checks for params recieved
                //type parameter has either workorder or truck
                if (this.props.navigation.state.params.type == 'workorder') {
                    //Sent from previous View via navigation.state.params
                    this.state.workorder_id = this.props.navigation.state.params.workorder_id

                    //Gets a single checklist of type workorder (first one)
                    let checklist = DBHelper.getAll('Checklist').filtered('type = "workorder"')[0]
                    //Used to display checklist and get the checklist's items
                    this.state.checklist_id = checklist.id //save it

                    //Gets the note pertaining to the workorder that has already been set previoius (could be blank)
                    let note = DBHelper.getById('WorkOrder', this.props.navigation.state.params.workorder_id).note
                    //Used to display previous note
                    this.state.text = note
                } else {
                    //Gets a single checklist of type truck (first one)
                    let checklist = DBHelper.getAll('Checklist').filtered('type = "truck"')[0]
                    this.state.checklist_id = checklist.id //save it
                }
            } else {
                //should never be called unless someone goes extremely wrong!
                throw ('Failed to recieve checklist type params within state');
            }
        } catch (error) {
            this.props.navigation.navigate('ErrorHandler', { error: error, type: 'Internal', class: Checklist.name })
            
        }
    }

    /**
     * Gets an array of Items pertaining to state's checklist_id from via. Database
     * Creates a answer format example : [false, 'N'] ... should be changed
     *
     * NOTE: should be called after checklist_id state is set
     * @throws - Failed to get checklist_id within state
     */
    getItems() {
        try {
            if (this.state.checklist_id != null) {
                //Gets an array of items by checklist_id from via. database 
                //Sorted by the sequence so that you can have a higharcy
                let items = DBHelper.getAll('Checkitems').filtered('list_id =' + this.state.checklist_id).sorted('sequence')
                if (items != null) {
                    var data = []
                    for (let i = 0; i < items.length; i++) {
                        if (items[i].type == 'yesno') {
                            //yes or no answer structure
                            data.push([false, 'N'])
                        } else if (items[i].type == 'dr') {
                            //defected or repaired answer structure
                            data.push([false, 'D', false, 'R'])
                        } else if (items[i].type == 'text') {
                            //text answer structure
                            data.push([''])
                        }
                    }
                    this.state.new_items = data //saves answer format to state
                } else {
                    //should be called unless, fms has caused the error
                    throw('Didnt get any items?')
                }
            } else {
                //should never be called unless someone goes extremely wrong!
                throw('Failed to get checklist_id within state')
            }
        } catch (error) {
            this.props.navigation.navigate('ErrorHandler', {error: error, type: 'Internal', class: Checklist.name})
        }

        // let type = 'text'
        // if (type == 'yesno') {
        //     this.state.new_items = [[true, 'Y'], [false, 'N'], [true, 'Y']]
        // } else if (type == 'text') {
        //     this.state.new_items = [['stuff can go here'], [''], ['lots of stuff here\n tooooooooooooooo']]
        // } else {
        //     this.state.new_items = [[true, 'D', false, 'R'], [false, 'D', false, 'R'], [false, 'D', false, 'R']]
        // }

        //[{ type: 'yesno', data: [true, 'Y'] } , [false, 'N'], [true, 'Y'], ['stuff can go here'], [''], [true, 'D', false, 'R'], ]

    }

    //NEW dynamic data
    // {
    // 'checklist': {
    //     'id': 21,
    //     'type': 'workorder/truck'    
    //     'name' : 'location checklist',
    //     'items' : [
    //         {
    //             'id': 34234234,
    //             'description': 'Air Brake System',
    //             'type': 'selection', 'text', 'checks', 'dropdown'
    //                                 //'answers': 'defected'
    //             //'answers': 'yes'
    //             'values': ['D', 'R']
    //         },
    //     ]
    // }
    
    //     [{id: 23 /*workorder_id or truck_id*/, checklist_id: 1}]

    /**
     * Updates the answer structure stored within the state with the selected
     * button or inputted text
     *
     * @param {int} array_id //identifier within the answer state array (or database)
     * @param {string} who //only set for defected or repaired (which is select)
     * @param {string} text //only set for text inputt
     */
    updateItem(array_id, who, text) {
        //Array of item answer structures
        var items = this.state.new_items
        //Gets the actual type from the database
        let type = DBHelper.getAll('Checkitems').filtered('list_id =' + this.state.checklist_id).sorted('sequence')[array_id].type
        //Changes the answers and saves the new item answer structures
        if (type == 'yesno') {
            if (items[array_id][0]) {
                items[array_id][0] = false
                items[array_id][1] = 'N'
            } else {
                items[array_id][0] = true
                items[array_id][1] = 'Y'
            }
        } else if (type == 'text') {
            items[array_id][0] = text
        } else {
            if (items[array_id][2] && who == 'D') {
                items[array_id][0] = true
                items[array_id][2] = false
            } else if (items[array_id][0] && who == 'R'){
                items[array_id][0] = false
                items[array_id][2] = true
            } else {
                if (who == 'D') {
                    if (items[array_id][0]) {
                        items[array_id][0] = false
                    } else {
                        items[array_id][0] = true
                    }
                } else if (who == 'R') {
                    if (items[array_id][2]) {
                        items[array_id][2] = false
                    } else {
                        items[array_id][2] = true
                    }
                }
            }
        }
        this.setState({
            new_items: items
        })
    }

    /**
     * Submitting the array of item answer structures to the database.
     * Since both array.length is equaivalent you can match arrays and compare
     * The answers will be stored inside of the workorder or truck table column.
     */
    submitList() {
        var data = []
        let items = this.state.new_items
        let checklist = DBHelper.getAll('Checkitems').filtered('list_id =' + this.state.checklist_id)
        if (checklist.length > 0) {
            for (let i = 0; i < checklist.length; i++) {
                data.push({id: checklist[i].id, type: checklist[i].type, data: items[i]})
            }
        }

        //Determines if the it is a truck or workorder checklist and saves it to the corresponding table
        let type = this.props.navigation.state.params.type
        if (type == 'truck') {
            let truck_id = DBHelper.get('Driver', 0).truck_id

            data.push({ type: 'remarks', data: this.state.text })
            console.log('string array: ' + data.toString());

            DBHelper.updateElementAndById('Truck', truck_id, 'checklist', data.toString())
            this.props.navigation.navigate('Truck')
        } else {
            DBHelper.updateElementAndById('WorkOrder', this.state.workorder_id, 'note', this.state.text)
            DBHelper.updateElementAndById('WorkOrder', this.state.workorder_id, 'checklist', data.toString())
            //type == workorder
            this.props.navigation.navigate('WorkOrder')
        }
    }

    /**
     * Gets all of the items within the checklist which is displayed differently depending on the type
     * and is then stored within the array to be displayed
     *
     * @return {Array} - Checklist item's views to be displayed
     */
    getItemsView() {
        try {
            var item_array = []
            let items = DBHelper.getAll('Checkitems').filtered('list_id =' + this.state.checklist_id).sorted('sequence')
            if (items.length > 0) {
                for (let i = 0; i < items.length; i++) {
                    let type = items[i].type
                    if (type == 'yesno') {
                        item_array.push(
                            <View style={[styles.rowLayout, styles.rowStyles]} key={i}>
                                <View style={styles.left}>
                                    <Text>{items[i].description}</Text>
                                </View>
                                <View style={styles.right}>
                                    <Checkbox
                                        onSelect={() => this.updateItem(i, null, null)}
                                        disabled={false}
                                        disabledColor='#247fd2'
                                        selected={this.state.new_items[i][0]}
                                        defaultColor='white'
                                        selectedColor='#446CB3'
                                        checkboxStyle={styles.checkboxStyle}
                                    >
                                        <Text>{this.state.new_items[i][1]}</Text>
                                    </Checkbox>
                                </View>
                            </View>
                        )
                    } else if (type == 'text') {
                        item_array.push(
                            <View style={[styles.rowLayout, styles.rowStyles]} key={i} on={() => console.log('hello')}>
                                <View style={styles.left}>
                                    <Text>{items[i].description}</Text>
                                </View>
                                <View style={styles.right}>
                                    <View style={styles.textContainer}>
                                        <TextInput
                                            style={styles.textField}
                                            editable={true}
                                            multiline={true}
                                            onChangeText={(text) => this.updateItem(i, null, text)}
                                            placeholder='note here...'
                                            placeholderTextColor='#BFBFBF'
                                            value={this.state.new_items[i][0]}
                                        >
                                        </TextInput>
                                    </View>
                                </View>
                            </View>
                        )
                    } else {
                        item_array.push(
                            <View style={[styles.rowLayout, styles.rowStyles]} key={i}>
                                <View style={styles.left}>
                                    <Text>{items[i].description}</Text>
                                </View>
                                <View style={styles.right}>
                                    <View style={styles.rowLayout}>
                                        <View style={styles.left}>
                                            <Checkbox
                                                onSelect={() => this.updateItem(i, 'D', null)}
                                                disabled={false}
                                                disabledColor='#247fd2'
                                                selected={this.state.new_items[i][0]}
                                                defaultColor='white'
                                                selectedColor='#446CB3'
                                                checkboxStyle={styles.checkboxStyle}
                                            >
                                                <Text>{this.state.new_items[i][1]}</Text>
                                            </Checkbox>
                                        </View>
                                        <View style={styles.right}>
                                            <Checkbox
                                                onSelect={() => this.updateItem(i, 'R', null)}
                                                disabled={false}
                                                disabledColor='#247fd2'
                                                selected={this.state.new_items[i][2]}
                                                defaultColor='white'
                                                selectedColor='#446CB3'
                                                checkboxStyle={styles.checkboxStyle}
                                            >
                                                <Text>{this.state.new_items[i][3]}</Text>
                                            </Checkbox>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        )
                    }
                }
            }
            return item_array //Checklist items that will be display
        } catch (error) {
            this.props.navigation.navigate('ErrorHandler', { error: error, type: 'Internal', class: Checklist.name })
        }
    }

    render() {
        return (
            <View behavior="padding" style={styles.container}>
                <View style={styles.content}>
                    <ScrollView style={styles.scrollContainer}>
                        {/* Wrapped in a TouchableOpacity to close the keyboard */}
                        <TouchableOpacity activeOpacity={1} onPress={() => Keyboard.dismiss()}>  
                            <KeyboardAvoidingView behavior='padding'>
                                {/* All of the checklist get displayed here */}
                                <View style={styles.itemContainer}>
                                    {this.getItemsView()}
                                </View>
                                {/* Note to be left by the driver */}
                                <View style={styles.textContainer}>
                                    <Text>Remarks:</Text>
                                    <TextInput
                                        style={[styles.textField, {width: window.width - 30, height: 70}]}
                                        editable={true}
                                        multiline={true}
                                        onChangeText={(text) => this.setState({text: text})}
                                        placeholder='remarks here...'
                                        placeholderTextColor='#BFBFBF'
                                        value={this.state.text}
                                    >
                                    </TextInput>
                                </View>
                                {/* Confirmation Button */}
                                <View>
                                    <TouchableOpacity style={styles.startButtonContainer} onPress={() => this.submitList()}>
                                        <Text style={styles.startText}>Confirm Checklist</Text>
                                    </TouchableOpacity>
                                </View> 
                            </KeyboardAvoidingView>
                        </TouchableOpacity>
                    </ScrollView>
                </View >

                {/* Checklist header */}
                <Header nav={this.props.navigation} type={this.props.navigation.state.params.type}/>

            </View>
        );
    }
}

//Gets the size of the mobile device (in pixel's)
const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1, //like bootstraps xs-lg-12 but without dropping down
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        alignItems: 'center',
        width: window.width,
        top: 70
    },
    scrollContainer: {
        height: window.height - 70,
    },
    rowLayout: {
        flexDirection: 'row',
        paddingBottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    rowStyles: {
        borderBottomWidth: 1,
        borderColor: '#BDC3C7',
        margin: 8,
        marginBottom: 0,
        paddingBottom: 8,
    },

    left: {
        flex: 0.5,
        alignItems: 'center',
        paddingRight: 10,
        justifyContent: 'center',
    },
    right: {
        flex: 0.5,
        alignItems: 'center',
        paddingLeft: 10,
        
    },
  
    mainText: {
        color: 'white',
        fontSize: 16,
        fontWeight: '700',
    },
    secondaryText: {
        color: 'white',
        fontSize: 14,
        fontWeight: '600',
    },

    startButtonContainer: {
        backgroundColor: '#ffa726',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        width: window.width - 60,
        borderRadius: 5,
        margin: 20,
    },
    startText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: '700',
    },


    textContainer: {
        margin: 5,
        width: (window.width / 20) * 9,

    },
    textField: {
        padding: 2,
        backgroundColor: '#ECECEC',
        borderRadius: 5,
        height: 60,
        fontSize: 15,
    },

    checkboxStyle: {
        margin: 2,
        width: 26,
        height: 26,
        borderWidth: 2,
        borderColor: '#ddd',
        borderRadius: 5
    },

    itemContainer: {
        paddingTop: 10,
    }
});
