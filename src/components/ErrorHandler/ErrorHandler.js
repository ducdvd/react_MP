import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, NetInfo, ScrollView, Dimensions, TouchableOpacity} from 'react-native';

import DBHelper from '../../DBHelper';
import renderIf from '../../config/renderif';


/**
 * Parent Component
 *
 * Displays Errors in Debug or Release in a well formatted View
 */
export default class ErrorHandler extends Component {

    constructor(props) {
        super(props)
        this.state = {}
    }
    
    /**
     * Checks whether the error recieved is actually there within the parameter sent over
     * when the error was catch in another view
     *
     * @return - Returns View that will be displayed
     */
    recievedError() {
        var message = '';
        var type = 'Internal';
        try {
            if (this.props.navigation.state.params != undefined) {
                if (this.props.navigation.state.params.error != undefined) {
                    message = this.props.navigation.state.params.error;
                } else {
                    throw ('Recieved \'error\' parameter is undefined');
                }
                if (this.props.navigation.state.params.type != undefined) {
                    type = this.props.navigation.state.params.type;
                }
            } else {
                throw ('Recieved parameter is undefined')
            }
        } catch (error) {
            message = error;
            return this.displayError(message, type)
        }
        return this.displayError(message, type)
    }

    /**
     * Causes an error but the message will be to Restart
     * NOTE: only displays message in DEBUG mode not in RELEASE
     */
    throwRestart() {
        //give message
        throw('Restart Application')
    }

    /**
     * This should be implemented to push the error to Api (FMS)
     */
    submitError() {
        return
    }

    /**
     * Return the error message and type of error, in a well formatted
     * and styled view which will be displayed.
     *
     * @param {String} error - Error Message
     * @param {String} type - What Type of Error this is?
     * @return {Array} View that contains the error
     */
    displayError(error, type) {
        var title = type
        var message = error
        if (error == undefined || type == undefined) {
            title = 'Internal';
            message = 'Failed to display message';
        }
        var display = []
        display.push(
            <View style={styles.errorContainer} key={0}>
                <View style={styles.errorTitleContainer}>
                    <Text style={styles.errorTitleText}>{type}</Text>
                </View>
                <View style={styles.errorMessageContainer}>
                    <Text style={styles.errorMessageText}>{error}</Text>
                </View>
            </View>
        )
        return display
    }

    /**
     * Renders the View
     */
    render() {
        return (
            <View style={styles.container}>
                {/* Header */}
                <View style={styles.headerContainer}>
                    <Text style={styles.title}>Oops something went wrong!</Text>
                </View>

                {/* Body */}
                <View style={styles.content}>
                    <ScrollView style={styles.scrollContainer}>
                        {/* Send Error Button */}
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity style={styles.button} onPress={() => this.throwRestart() }>  
                                <Text style={styles.buttonText}>Send Report</Text>
                            </TouchableOpacity>
                        </View>

                         {/* Displays the Error message here */}
                        {this.recievedError()}    
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    content: {
        position: 'absolute',
        alignItems: 'center',
        backgroundColor: '#E4F1FE',
        width: window.width,
        top: 50,
    },

    scrollContainer: {
        backgroundColor: '#333333',
        height: window.height - 50,
    },
    errorContainer: {
        alignItems: 'center',
    },

    buttonContainer: {
        marginTop: 20,
        width: window.width,
        alignItems: 'center',
    },
    button: {
        backgroundColor: '#446CB3',
        borderRadius: 10,
        borderWidth: 2,
        borderColor: 'white',
        width: window.width / 3,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        fontWeight: '600',
        color: 'white'
    },

    errorTitleContainer: {
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20,
    },
    errorTitleText: {
        fontWeight: '800',
        fontSize: 18,
        color: 'white',
    },

    errorMessageContainer: {
        width: window.width - 20,
        backgroundColor: 'white',
        borderRadius: 2,
        borderColor: 'white',
        alignItems: 'center',
        padding: 5,
    },
    errorMessageText: {
        fontSize: 14,
    },

    headerContainer: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#D64541',
        justifyContent: 'center',
        alignItems: 'center',
        width: window.width,
        height: 50,

    },
    title: {
        marginTop: 10,
        color: 'white',
        fontWeight: '700',
    },
});