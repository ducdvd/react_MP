import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, KeyboardAvoidingView, Button, ScrollView, TouchableOpacity, AsyncStorage, Alert} from 'react-native';
import Timer from 'react-native-timer';
import DBHelper from '../../DBHelper';

/**
 * Child Component
 *
 * Provides a footer component that can be added into
 * any view. The footer has navigation buttoms to:
 * - Refresh
 * - Truck
 * - Map
 * - Work Orders
 * - Settings
 */
export default class Footer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            //URI to refresh image
            refreshImage: require('../../images/refresh-grey-256.png'),
        }
        //Check if a refresh recently happened
        this.getRefreshImage()
    }

    /**
     * From the scanning view it determines if there was any fuel
     * dispensed to that unit. If there was then alert the driver
     * to double check that he wishes to leave the View.
     *
     * @param {String} view - The view your heading to
     */
    leaveScanning(view) {
        if (this.props.compartment != null && this.props.task != null) {
            if (this.props.dispensed > 0) {
                Alert.alert(
                    'Finished with Unit',
                    'Are you sure your finished with this unit?',
                    [
                        { text: 'Yes', onPress: () => this.leaveView(view) },
                        { text: 'No', onPress: () => console.log() }
                    ],
                    { cancelable: false }
                )
                return
            }
        }
        this.props.nav.navigate(view)
    }

    /**
     * Saves the amount of dispensed fuel from Scanning View to the database
     * then returns the driver to the requested view (via. parameter view)
     * 
     * @param {String} view - The view your heading to
     */
    leaveView(view) {
        let compartment = this.props.compartment
        let task = this.props.task
        DBHelper.updateElementAndById('Compartment', compartment.id, 'current_amount', (compartment.current_amount - this.props.dispensed))
        DBHelper.updateElementAndById('Task', task.id, 'amount', (task.amount + this.props.dispensed))
        Timer.clearInterval('add')
        this.props.nav.navigate(view)
    }

    /**
     * Closes the timers (threads) that could be open during
     * the scanning view.
     */
    closeTimer() {
        Timer.clearInterval('sample');
        Timer.clearInterval('add');
    } 

    // ----------------------------------
    //           Map Functions
    // ----------------------------------

    /**
     * Check if your on this view
     *
     * @return {boolean} if you can go to this view
     */
    inMapView() {
        return false
        if (this.props.map) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check if your on this view and return a active or
     * deactive icon
     *
     * @return {require} string that holds the URI of Map Icon
     */
    activeMap() {
        if (this.props.map) {
            return require('../../images/petrologocircle-deactive.png');
        } else {
            return require('../../images/petrologocircle.png');
        }
    }
    
    // ----------------------------------
    //          Truck Functions
    // ----------------------------------   
    
    /**
     * Check if your on this view
     *
     * @return {boolean} if you can go to this view
     */
    inTruckView() {
        return false
        if (this.props.truck) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check if your on this view and return a active or
     * deactive icon
     *
     * @return {require} string that holds the URI of Truck Icon
     */
    activeTruck() {
        if (this.props.truck) {
            return require('../../images/truck-grey-256.png');
        } else {
            return require('../../images/truck-orange-256.png');
        }
    }

    // ----------------------------------
    //        WorkOrder Functions
    // ----------------------------------

    /**
     * Check if your on this view
     *
     * @return {boolean} if you can go to this view
     */
    inWorkOrderView() {
        return false
        if (this.props.workorder) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check if your on this view and return a active or
     * deactive icon
     *
     * @return {require} string that holds the URI of WorkOrder Icon
     */
    activeWorkOrder() {
        if (this.props.workorder) {
            return require('../../images/list-grey-256.png')
        } else {
            return require('../../images/list-orange-256.png')
        }
    }


    // ----------------------------------
    //         Settings Functions
    // ----------------------------------

    /**
     * Check if your on this view
     *
     * @return {boolean} if you can go to this view
     */
    inSettingsView() {
        return false
        if (this.props.settings) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check if your on this view and return a active or
     * deactive icon
     *
     * @return {require} string that holds the URI of Settings Icon
     */
    activeSettings() {
        if (this.props.settings) {
            return require('../../images/settings-grey-256.png')
        } else {
            return require('../../images/settings-orange-256.png')
        }
    }

    /**
     * If the driver doesn't have a active trip immediately displays
     * the Truck Selection View (Change)
     */
    getActiveTrip() {
        this.closeTimer()
        DB_active_trip = DBHelper.getAll('Trip').filtered('active == 1')
        if (DB_active_trip.length == 0) {
            this.leaveScanning('Change')
            //this.props.nav.navigate('Change');
            return;
        }
        this.leaveScanning('WorkOrder')
        //this.props.nav.navigate("WorkOrder")
        return;
    }

    /**
     * Closes the nessecary timers (threads) and
     * calls leaveScanning to check if it 'WAS' on Scanning View
     *
     * @param {String} screen - The view you wish to go to
     */
    onNavigate(screen) {
        this.closeTimer()
        this.leaveScanning(screen)
        //this.props.nav.navigate(screen); //replaced within leaveScanning
    }

    /**
     * NOTE: should be moved to work with onNavigate method
     */
    refresh() {
        this.closeTimer()
        this.leaveScanning('Refresh')
        //this.onNavigate("Refresh") //replaced within leaveScanning
    }

    /**
     * Checks the devices internal storage (NOT THE DATABASE) for
     * the @refreshed key item within the AsynStorage. If @refreshed
     * is set it means that a refresh recently happened.
     *
     * Sets the state of refresh icon to either green or grey icon URI
     */
    getRefreshImage() {
        try {
            var color = AsyncStorage.getItem('@refreshed', (err, result) => {
                //console.warn(result);
                if (result !== null) {
                    // We have data!!
                    //console.warn('result: ' + result)
                    this.setState({
                        refreshImage: require('FMSREACT/src/images/refresh-green-256.png')
                    })
                    // this.state.refreshImage = require('FMSREACT/src/images/refresh-green-256.png')
                    return;
                } else {
                    this.setState({
                        refreshImage: require('../../images/refresh-grey-256.png')
                    })
                    return;
                }
            })
        } catch (error) {
            this.state.refreshImage = require('../../images/refresh-grey-256.png')
        }
    }
    
    render() {
        return (
            // Displayed at the bottom of the screen
            <View style={styles.container}>
                {/* Navigation Items */}
                <TouchableOpacity
                    onPress={() => this.refresh()}
                    style={styles.buttons}
                >
                    {/*Refresh*/}
                    <Image
                        style={styles.iconImages}
                        source={this.state.refreshImage}
                    />   
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => this.onNavigate("Truck")}
                    style={styles.buttons}
                    disabled={this.inTruckView()}
                >
                    {/*Truck*/}
                    <Image
                        style={styles.iconImages}
                        source={this.activeTruck()}
                    />   
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => this.onNavigate("Map")}
                    style={styles.mainButton}
                    disabled={this.inMapView()}
                >
                    {/*Map*/}
                    <Image
                        style={styles.mapImage}
                        source={this.activeMap()}
                    />   
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => this.getActiveTrip()}
                    style={styles.buttons}
                    disabled={this.inWorkOrderView()}
                >
                    {/*WorkOrder*/}
                    <Image
                        style={styles.iconImages}
                        source={this.activeWorkOrder()}
                    />   
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => this.onNavigate("Settings")}
                    style={styles.buttons}
                    disabled={this.inSettingsView()}
                >
                    {/*Settings*/}
                    <Image
                        style={styles.iconImages}
                        source={this.activeSettings()}
                    />   
                </TouchableOpacity>
            </View>
        );
    }
}

const styles= StyleSheet.create({
    container: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: '#5f646d',
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        height: 45,
        },
    buttons: {
        /**
         * This the number of Navigation items on the bottom
         * To Add or Remove navigation items please change this
         * to the given number of % (percentage) that one item would take.
         *
         * Currently: 1/5
         * Equation: 1/{number of navigation items}
         * Test: (0.2 * 5) > 1
         */
        flex: 0.2,
        alignItems: 'center',
    },
    mainButton: {
        flex: 0.30,
        alignItems: 'center',
    },
    mapImage: {
        height: 90,
        width: 80,
        marginBottom: 35,
    },
    iconImages: {
        height: 25,
        width: 25,
        marginBottom: 0,
    }
});
