import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, KeyboardAvoidingView, Button, NetInfo , Geolocation} from 'react-native';

import LoginForm from './LoginForm';
import DBHelper from '../../DBHelper';
import renderIf from '../../config/renderif';

/**
 * Parent Component
 *
 * Displays a login forum also is the first view to be seen when starting
 * up the mobile application
 */
export default class Login extends Component {
    constructor(props) {
        super(props)
        //Binds the onNavigation method - used in LoginForm
        this.onNavigate = this.onNavigate.bind(this);

        this.state = {
            offline: 0, //True when there is no internet
        }

        //Determines if there is internet
        this.getInternet();
        //Gets the location of the driver
        //this.getLocation();
    }

    /**
     * Does not particular do anything with the geo location information
     * just logs is as a warning
     *
     * NOTE: this wont! be called if "this.getLocation()" is commented out in the
     * constructor
     *
     * NOTE: Updated way of getting geo location is in Scanning.js
     *
     */
    getLocation() {
        navigator.geolocation.watchPosition(function (position) {
            console.warn(position.coords.latitude + " : " + position.coords.longitude);         
        }, function (error) {
            console.log(error)
        }, {enableHighAccuracy: false, timeout: 1, maximumAge: 1});

        // navigator.geolocation.getCurrentPosition(function (position) {
        //     console.warn(position.coords.latitude + " : " + position.coords.longitude);         
        // });
    }

    /**
     * Called within the LoginForm (Child Component) and determines if there is an
     * active trip. If there is not a active trip it will immediately show
     * Truck Selection (Change)
     */
    onNavigate() {
        console.log("Parent Class") //Just to clariy where the method is being called from
        NetInfo.isConnected.removeEventListener('change', this._handleConnectionChange);
        if (DBHelper.getAll('Trip').filtered('active = 1').length == 1) {
            this.props.navigation.navigate('WorkOrder');
        } else {
            this.props.navigation.navigate('Change');
        }
    }
    
    /**
     * Adds the event listen to the View for detecting internet
     *
     * NOTE: Should be moved into the ComponentWillMount() method
     * ComponentWillMount() method is a predefined method that gets called during
     * execution. Just simply implement it
     */
    getInternet() {
        NetInfo.isConnected.addEventListener('change', this._handleConnectionChange);
        //NetInfo.isConnected.removeEventListener('change', this._handleConnectionChange);
    }

    /**
     * Updates the state of offline if they internet connection has changed
     */
    _handleConnectionChange = (isConnected) => {
        this.state.offline = isConnected
        if (isConnected) {
            console.log('Has internet? ' + isConnected);
        } else {
            console.log('Not connected');
        }
        this.forceUpdate();
    };
    
    render() {
        const { navigate } = this.props.navigation;
        return (
        //KeyboardAvoidingView prevents the keyboard pop up from interfering with the view
        <KeyboardAvoidingView behavior="padding" style={styles.container}>
            <View style={styles.logoContainer}>
                {/*Manage Petro Company Logo*/}
                <Image
                    style={styles.logo}
                    source={require('../../images/logo-with-text-white-low.png')}
                />
            </View>
            {/*Visually displays if the mobile application has internet*/}
            {renderIf(!this.state.offline)(
                <View style={styles.internetContainer}>
                    <Text style={styles.internet}>
                    Offline Mode
                    </Text>
                </View>
            )}
            <View style={styles.formContainer}>
            {/*External component added for logging in*/}
            <LoginForm nav={this.onNavigate} navigation={this.props.navigation}/>
            </View>
            <View style={styles.supportContainer}>
            <Text style={styles.support}>
                {/*Will be changed in later development*/}
                Support: +1-604-424-4024
            </Text>
            </View>
        </KeyboardAvoidingView>
        );
    }
}

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333'
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center',
        paddingBottom: 10
    },
    logo: {
        width: 200,
        height: 200
    },
    title: {
        color: '#FFF',
        marginTop: 10,
        width: 180,
        textAlign: 'center',
        opacity: 0.9
    },
    supportContainer: {
        alignItems: 'center',
    },
    support: {
        color: '#949494',
        paddingBottom: 10
    },
    internetContainer: {
        alignItems: 'center'
    },
    internet: {
        color: '#FFF',
    }
});
