import React, { Component} from 'react';
import { StyleSheet, View, TextInput, Button, TouchableOpacity, Text, StatusBar, Alert, AsyncStorage} from 'react-native';
import Login from './Login'

import DBHelper from '../../DBHelper';

/**
 * Child Component to: Login.js
 *
 * Displays a login form when added to parent.
 * requires:
 *      username and password
 *      internet connection
 *      successful api calls
 * 
 */
export default class LoginForm extends Component {
    onNav() {
        //console.log("Child Class")
        this.props.nav();
    };

    constructor(props) {
        super(props)
        this.state = {
            user: null,
            password: null,
            error: "",
            token: "",
            userid: ""
        }
    }

    /**
     * Calls the sample data hosted on the
     * dummy.managepetro.com website
     */    
    async loadSamples() {

        // SAMPLE: how to add information to the database
        //DBHelper.removeAll('Driver');
        //let array = [-1, 'teiweb', 'TEIWEB', '604-644-1334', 'hello@hello.ca', 'teiweb', 52];
        //let array2 = ['hello', 'hello', 'hello', 'hello', 'hello', 'hello', 22];
        // let stuff = DBHelper.generateDriver(array);
        // for (var key in stuff) {
        //     console.warn(stuff[key]);
        // }
        //DBHelper.add('Driver', array);
        //DBHelper.update('Driver', 1, array2); //can use updateElementById() instead of array
        //console.warn(DBHelper.getAll('Driver').toString());
        //DBHelper.printAll('Driver');

        //Basic URL to API
        let http = 'http://dummy.managepetro.com/';

        //Testing how AsyncStorage works (displayed in Settings -> About.js)
        try {
            AsyncStorage.setItem('@MySuperStore:key', 'I like to save it.');
        } catch (error) {
            this.props.navigation.navigate('ErrorHandler', {error: error, type: 'AsyncStorage', class: LoginForm.name})
            // Error saving data
        }



        /**
         * --------GENERAL NOTE:--------
         *
         * If you see:
         * //MULTIPLE: means that the JSON request contains an Array data
         * else:
         * //SINGLE: means that the JSON request contains only one piece of data
         */


        //LOAD LOCATIONS
        try {
            
           
            let table = 'Locations'
            //DBHelper.removeAll(table);
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'locations');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Locations--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

            }
        } catch (error) {
            console.warn("Failed to Load Locations Data \n" + error);
        }
        /**
         * --------GENERAL NOTE:--------
         *
         * If you see:
         * //MULTIPLE: means that the JSON request contains an Array data
         * else:
         * //SINGLE: means that the JSON request contains only one piece of data
         */


        //LOAD USERS
        try {
            //DBHelper.removeAll('Driver');
            let table = 'Driver'
            //DBHelper.removeAll(table);
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'driver');
                let json = await response.json();
                console.log("\n--json[Driver]--\n" + "\n\n");
                console.log(json);

                // //MULTIPLE
                // for (let i = 0; i < json.length; i++) {
                //
                //     let data = [];
                //
                //     // If the sample data comes in with no id, you must add a zero to the beginning and
                //     // Must! change the DBHelper "add" function to include the table that is missing id
                //     // So that the id is auto increment.
                //     //-----CODE:-------
                //     // data.push(0)
                //
                //     Parses the json into an array
                //     for (item in json[i]) {
                //         console.log(json[i][item]);
                //         data.push(json[i][item]);
                //     }
                //     console.log("\n\n" + data + "\n\n");
                //     DBHelper.add(table, data);
                // }

                //SINGULAR
                let data = [];

                // If the sample data comes in with no id, you must add a zero to the beginning and
                // Must! change the DBHelper "add" function to include the table that is missing id
                // So that the id is auto increment.
                //-----CODE:-------
                //data.push(0)

                //Parses the json into an array
                for (item in json) {

                    console.log("\n--Driver  username--:" + json[item].username + "\n\n");
                    DBHelper.addDriver(table, json[item]);
                    data.push(json[item]);
                }

                console.log("\n--Driver--\n" + data + "\n\n");
                //DBHelper.add(table, data);


            }
        } catch (error) {
            console.warn("Failed to Load Driver Data \n" + error);
        }

        //LOAD TRUCKS        
        try {
            let table = 'Truck'
            //DBHelper.removeAll(table);
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'truck');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {
                
                    let data = [];
                
                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)
                
                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Truck--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Truck Data \n" + error);
        }


        //LOAD VENDOR
        try {
            let table = 'Vendor'
            //DBHelper.removeAll(table);
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'vendor');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Vendor--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

            }
        } catch (error) {
            console.warn("Failed to Load Vendor Data \n" + error);
        }
        //LOAD Compartments       
        try {
            let table = 'Compartment'
            //DBHelper.removeAll(table);
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'compartment');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {
                
                    let data = [];
                
                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)
                
                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Compartment--\n" + data + "\n\n");
                    DBHelper.add(table, data);

                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Compartment Data \n" + error);
        }

        //LOAD Routes
        try {

            let table = 'Trip'
            //DBHelper.removeAll(table);
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'trip');
                let json = await response.json();
                console.log("\n--Trip--\n" + "\n\n");
                console.log(json);

                //Parses the json into an array
                for (item in json) {

                    console.log("\n--Trip  route--:" + json[item].route + "\n\n");
                    DBHelper.addTrip(table, json[item]);

                }


                //DBHelper.add(table, data);


            }
        } catch (error) {
            console.warn("Failed to Load Trip/Route Data \n" + error);
        }
        //LOAD WorkOrders
        try {

            let table = 'WorkOrder'

            //DBHelper.removeAll(table);
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'workorder');
                let json = await response.json();
                console.log("\n--WorkOrder--\n" + "\n\n");
                console.log(json);

                //Parses the json into an array
                for (item in json) {

                    console.log("\n--WorkOrder  order_id--:" + json[item].order_id + "\n\n");
                    DBHelper.addWorkOrder(table, json[item]);

                }


                //DBHelper.add(table, data);


            }
        } catch (error) {
            console.warn("Failed to Load WorkOrder Data \n" + error);
        }

        //LOAD Tasks
        try {
            let table = 'Task'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'task');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {
                
                    let data = [];
                
                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)
                
                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Task--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Task Data \n" + error);
        }

        //LOAD Units
        try {
            let table = 'Unit'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'units');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)

                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Unit--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Task Data \n" + error);
        }
        //LOAD Clients
        try {
            let table = 'Clients'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'clients');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {
                
                    let data = [];
                
                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)
                
                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Clients--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Clients Data \n" + error);
        }
        
        //LOAD Fuel
        try {
            let table = 'Fuel'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'fuels');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {
                
                    let data = [];
                
                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)
                
                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Fuel--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Fuel Data \n" + error);
        }



        //LOAD Log
        try {
            let table = 'Log'

            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'log');
                let json = await response.json();
                console.log("\n--json[Log]--\n" + "\n\n");
                console.log(json);

                // //MULTIPLE
                // for (let i = 0; i < json.length; i++) {
                //
                //     let data = [];
                //
                //     // If the sample data comes in with no id, you must add a zero to the beginning and
                //     // Must! change the DBHelper "add" function to include the table that is missing id
                //     // So that the id is auto increment.
                //     //-----CODE:-------
                //     // data.push(0)
                //
                //     Parses the json into an array
                //     for (item in json[i]) {
                //         console.log(json[i][item]);
                //         data.push(json[i][item]);
                //     }
                //     console.log("\n\n" + data + "\n\n");
                //     DBHelper.add(table, data);
                // }

                //SINGULAR
                let data = [];

                // If the sample data comes in with no id, you must add a zero to the beginning and
                // Must! change the DBHelper "add" function to include the table that is missing id
                // So that the id is auto increment.
                //-----CODE:-------
                //data.push(0)

                //Parses the json into an array
                for (item in json) {

                    console.log("\n--Log  message--:" + json[item].message + "\n\n");
                    DBHelper.addLog(table, json[item]);
                    data.push(json[item]);
                }

                console.log("\n--addLog--\n" + data + "\n\n");
                //DBHelper.add(table, data);


            }
        } catch (error) {
            console.warn("Failed to addLog Data \n" + error);
        }

        //LOAD Checklist
        try {
            let table = 'Checklist'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'checklist');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {
                
                    let data = [];
                
                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)
                
                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Checklist--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Checklist Data \n" + error);
        }

        //LOAD Checkitems
        try {
            let table = 'Checkitems'
            DBHelper.removeAll(table);
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'checkitems');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {
                //for (let i = 0; i < 10; i++) {
                
                    let data = [];
                
                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)
                
                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Checkitems--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Checkitems Data \n" + error);
        }
    }
    

    /**
     * Called when the user presses the login button
     *
     * Creates an api request to verify the username and password. The Api should
     * return a token
     * NOT IMPLEMENTED: and store it with asnycstorage
     */    
    async onLoginPressed() {
        
        //NOTE: uncomment this to have password varification
        // if (this.state.user == '' || this.state.user == null || this.state.password == null || this.state.password == '') {
        //     console.log(this.state.user + ' : ' + this.state.password)
        //     return;
        // }

        try {
            // fetch('https://mywebsite.com/endpoint/', {
            //   method: 'POST',
            //   headers: {
            //     'Accept': 'application/json',
            //     'Content-Type': 'application/json',
            //   },
            //   body: JSON.stringify({
            //     firstParam: 'yourValue',
            //     secondParam: 'yourOtherValue',
            //   })
            // })
            let response = await fetch('http://dummy.managepetro.com/login');
            //let responseJson = await response.json();
            let json = await response.json()
            let res = json['token']
            
            // let json = await response.text()
            // let res1 = JSON.parse(json)
            // let res = res1['token']
            
            if (response.status >= 200 && response.status < 300) {
                //Handle Success
                this.setState({ error: "" });
                //let accessToken = res;
                //console.log("res token:" + accessToken);
                
                this.loadSamples();

                //Displays what the token, username, and password is
                Alert.alert(
                    'Success',
                    'Token: ' + res + '\nUsername: ' + this.state.user + '\nPassword: ' + this.state.password,
                    [
                        //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                        //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                )
                
                //Moves to Map Screen 


                this.onNav();

            } else {
                //Handle error
                let error = res;
                throw error;
            }
        } catch (error) {
            this.setState({ error: error });
            console.log("error " + error);
            Alert.alert(
                'Invalid',
                '' + error,
                [
                    //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
            )
        }
        this.onNav();
    }
      
    render() {
        return (
            <View style={styles.container}>
                {/*StatusBar is never seen?*/}
                <StatusBar
                    barStyle="light-content"
                />
                {/*Username*/}
                <TextInput
                    style={styles.input}
                    onChangeText={(text) => this.setState({ user: text })}
                    placeholder="Username"
                    placeholderTextColor="#bababa"
                    returnKeyType="next"
                    onSubmitEditing={() => this.passwordInput.focus()}
                    //HELPFUL: keyboardType="email-address"
                />
                {/*Password*/}
                <TextInput
                    style={styles.input}
                    onChangeText={(text) => this.setState({password: text})}
                    placeholder="Password"
                    placeholderTextColor="#bababa"
                    returnKeyType="go"
                    secureTextEntry
                    ref={(input) => this.passwordInput = input}
                />
                {/*Login Button*/}
                <TouchableOpacity style={styles.buttonContainer} onPress={this.onLoginPressed.bind(this)}>
                    <Text style={styles.buttonText}>Log in</Text>
                </TouchableOpacity>
            </View>
        );
    }
}


const styles= StyleSheet.create({
    container: {
        padding: 10,
        paddingHorizontal: 50,
        paddingBottom: 150
    },
    input: {
        height: 40,
        backgroundColor: '#525252',
        marginBottom: 10,
        color: '#FFF',
        paddingHorizontal: 10,
        borderRadius: 5
    },
    buttonContainer: {
        backgroundColor: '#FF8126',
        paddingVertical: 10,
        borderRadius: 5
    },
    buttonText: {
        textAlign: 'center',
        color: '#FFF',
        fontWeight: '600',
        fontSize: 16
    }
});
