import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, Geolocation} from 'react-native';
import Footer from '../Footer/Footer';

import Timer from 'react-native-timer';
import renderIf from '../../config/renderif';
import DBHelper from '../../DBHelper';
import MapView from 'react-native-maps';

/**
 * Parent Component
 *
 * Displays a Map using either MapKit(IOS) or Google Maps (Android). If
 * there is an active trip available it will display the workorder locations
 * as markers inside of the Map
 */
export default class Map extends Component {

    constructor(props) {
        super(props)
        this.state = {
            coordinate: {
                latitude: 37.78825, //default latitude, San Francisco
                longitude: -122.4324, //default longitude, San Francisco
            },
        };
        //Determines the initial location of the driver
        this.getLocation()
        //this.openTimer();
    }

    /**
     * Gets the driver's location from Geolocation
     */
    getLocation() {
        try {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    console.log("cords: " + position.coords.longitude + ' : ' + position.coords.latitude);
                    this.setState({
                        coordinate: {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude,
                        }
                    })
                },
                function (error) {
                    console.log(error)
                },
                {
                    enableHighAccuracy: true //How accurate the geolocation will be (can be false to save battery)
                }
            );
        }catch (error) {
            throw (error)
        }

    }

    /**
     * If a trip is active it will get all of its workorder's addresses/geolocation
     * and create a MapView.Marker for each locations. Displays within the MapView
     *
     * @return {Array} Locations of workorders which are displayed as markers
     */
    getTripDetails() {
        try {


            var trip_id = DBHelper.getAll('Trip').filtered('active = 1')[0]
            if (trip_id != null) {
                trip_id = trip_id.id
                var markers = []
                let workorders = DBHelper.getAll('WorkOrder').filtered('trip_id =' + trip_id)
                for (let i = 0; i < workorders.length; i++) {
                    let wo_coords = workorders[i].coordinate.split(',')
                    console.log(wo_coords[1])
                    let coordinates = { latitude:  parseFloat(wo_coords[0]), longitude:  parseFloat(wo_coords[1]) }

                    let Clients;

                    if(workorders[i].vendor_id == ''){
                        Clients = DBHelper.getById("Locations", workorders[i].location_id);
                    }else{

                        Clients = DBHelper.getById("Vendor", workorders[i].vendor_id);
                    }


                    //let client = DBHelper.getById('Clients', workorders[i].client_id).name
                    let client = Clients.name
                    markers.push(
                        <MapView.Marker
                            key={i}
                            coordinate={coordinates}
                            description={Clients.address}
                            title={client}
                        />
                    )
                }
                return markers
            }
            return
        }catch (error) {
            throw (error)
        }


    }

    /**
     * Creates a timer (or thread) that will check for drivers
     * geo location every 2000ms
     */
    openTimer() {

        try {
            Timer.setInterval(
                'geo',
                () => this.getLocation(),
                2000
            )
        }catch (error) {
            throw (error)
        }


    }

    /**
     * @return renders The Map View
     */
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    {/* Map View */}
                    <MapView
                        ref="map"
                        style={styles.map}
                        showsUserLocation
                        showsMyLocationButton={true}
                        loadingEnabled={true}
                        region={{
                            latitude: this.state.coordinate.latitude,
                            longitude: this.state.coordinate.longitude,

                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                    >
                        {/* MarkView.Marker's */}
                        {this.getTripDetails()}    
                    </MapView>    
                </View>
                {/*Child component footer*/}
                <Footer nav={this.props.navigation} back={'Map'} truck={1} map={0} workorder={1} settings={1}/>
            </View>
        );
    }
}

//Current size of the screen on the device
const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        height: window.height - 45,
        width: window.width,
        bottom: 45,
    },
    text: {
        color: 'black',
    },
    map: {
        position: 'absolute',
        width: window.width,
        height: window.height - 45,
    },
});
