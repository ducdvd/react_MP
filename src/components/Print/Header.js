import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, TouchableOpacity } from 'react-native';

import Icon2 from 'react-native-vector-icons/Octicons';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

/**
 * Child Component to: Print.js
 *
 * Custom header suited to the Print View
 */
export default class Header extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <View style={styles.column}>
                        {/* Back Button */}
                        <TouchableOpacity onPress={() => this.props.nav()} style={styles.backButtonContainer}>
                            <Icon name={'arrow-left'} size={20} style={styles.icon}></Icon>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.bigColumn}>
                        <Text style={styles.text}>Print Preview</Text>
                    </View>
                    <View style={styles.column}>
                        {/* Print Button */}
                        {/* Current does exactly what the back button does */}
                        <TouchableOpacity style={styles.printButtonContainer} onPress={() => this.props.nav()}>
                            <Text style={styles.buttonText}>Print</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Text style={styles.bar}></Text>
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        position: 'absolute',
        backgroundColor: '#5F646D',
        alignItems: 'center',
        flex: 1,
        height: 70,
        bottom: window.height - 70,
        width: window.width,
        justifyContent: 'center',
    },
    content: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    text: {
        color: '#FFF',
        paddingTop: 10,
        fontWeight: '800',
        fontSize: 17,
    },
    bar: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#575C64',
        height: 10,
        width: window.width,
    },
    column: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bigColumn: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
    },

    printButtonContainer: {
        top: 5,
        width: 75,
        height: 20,
        backgroundColor: '#EF4836',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        color: 'white',
        fontWeight: '700'
    },

    backButtonContainer: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    icon: {
        paddingTop: 10,
        color: 'white',
        fontWeight: '700'
    },
});
