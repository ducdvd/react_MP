import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, ScrollView, TouchableOpacity, Switch } from 'react-native';

import Header from './Header';
import Footer from '../Footer/Footer';
import ModalDropdown from 'react-native-modal-dropdown';
import Icon2 from 'react-native-vector-icons/Octicons';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import renderIf from '../../config/renderif';
import DBHelper from '../../DBHelper';

/**
 * Parent Component
 *
 * Displays a list of:
 * - All Work Orders and there tasks
 * - Single completed Work Order and it's tasks
 */
export default class Print extends Component {

    constructor(props) {
        super(props)

        /**
         * NOTE: Realm = Database
         */
        this.state = {
            trip_id: props.navigation.state.params.trip_id,
            workorder_id: props.navigation.state.params.workorder_id,
            date: null, //Current Date
            time: null, //Current Time
            trip: null, //Trip Realm Object
            driver: null, //Driver Realm Object
            workorder: null, //WorkOrder Realm Object
            client: null, //Client Realm Object
        }
        //Determines the current Date and Time
        this.getDateTime()
        //Gets the needed Realm Objects based on workorder_id
        this.getInformation()
    }

    /**
     * Gets need Realm Objects from the Database (SQLite)
     * If the workorder_id is set:
     * Means that your getting a single Work Order
     * else
     * Means that your printing all Work Orders
     */
    getInformation() {
        if (this.state.trip_id != null) {
            this.state.trip = DBHelper.getById('Trip', this.state.trip_id)
        } else {
            this.state.trip = DBHelper.getAll('Trip').filtered('active =1')[0]
        }


        this.state.driver = DBHelper.get('Driver', 0)
        if (this.state.workorder_id != null) {
            this.state.workorder = DBHelper.getById('WorkOrder', this.state.workorder_id)
            //this.state.client = DBHelper.getById('Clients', this.state.workorder.client_id)
        } else {
            this.state.workorder = DBHelper.getById('WorkOrder', 1)
            //this.state.client = DBHelper.getById('Clients', 1)
        }

        let Clients;

        if(this.state.workorder.vendor_id == ''){
            Clients = DBHelper.getById("Locations", this.state.workorder.location_id);
        }else{

            Clients = DBHelper.getById("Vendor", this.state.workorder.vendor_id);
        }
        this.state.client = Clients


    }

    /**
     * Gets all of the Tasks that have been despensed too.
     *
     * @param {Int} id - workorder_id within the database
     * @return {Array} Array of tasks view's that have dispensed amount greater than zero
     */
    completedWorkData(id) {
        let tasks = DBHelper.getAll('Task').filtered('workorder_id =' + id + ' AND amount > 0')
        var data = []
        if (tasks.length > 0) {
            data.push(<Text key={-1}>Filled:</Text>)
        }
        for (let i = 0; i < tasks.length; i++) {
            data.push(
                <View key={i} style={{margin: 5, borderBottomWidth: 1}}>
                    <Text>{i + 1}. Unit #{tasks[i].unit_number}  Fuel Type: {DBHelper.getById('Fuel', tasks[i].fuel_id).name}</Text>
                    <Text>  Dispensed: {tasks[i].amount}L</Text>
                </View>
            )
        }
        return data
    }

    /**
     * Gets all of the WorkOrders that are within the Trip
     *
     * @return {Array} Array of Work Orders and its Array of Tasks
     * within each Work Order
     */
    allCompletedWorkData() {
        let workorders = DBHelper.getAll('WorkOrder').filtered('trip_id =' + this.state.trip.id)
        var data = []
        for (let i = 0; i < workorders.length; i++) {
            //let client = DBHelper.getById('Clients', workorders[i].client_id)

            let client;

            if(workorders[i].vendor_id == ''){
                client = DBHelper.getById("Locations", workorders[i].location_id);
            }else{

                client = DBHelper.getById("Vendor", workorders[i].vendor_id);
            }


            data.push(
                <View key={i} style={{borderBottomWidth: 2, borderTopWidth: 2, marginBottom: 5, backgroundColor: 'white'}}>
                    <Text>Order id: {workorders[i].order_number}</Text>
                    <Text>Status: {this.getStatus(workorders[i].status)}</Text>
                    <Text>Client: {client.name}</Text>
                    <Text>Address: {client.address}</Text>
                    <Text>Note: {workorders[i].workorder_note}</Text>

                    {this.completedWorkData(workorders[i].id)}
                </View>
            )
        }
        return data
    }

    /**
     * Determines if the Work Order your getting is compeleted or not.
     *
     * @param {Int} status The work order status
     * @return {String} If the Work Order is "Completed" or "Not Completed"
     */
    getStatus(status) {
        if (status == 1) {
            return 'Not Completed'
            //return [styles.routing, '#cde673', 'Routing', { fontWeight: '600', color: '#555555' }];
        } else if (status == 2) {
            return 'Not Completed'
            //return [styles.nextjob, '#ffa726', 'Next Job', { fontWeight: '600', color: '#262626' }];
        } else if (status == 3) {
            return 'Not Compeleted'
            //return [styles.waiting, '#FFFFFF', 'Waiting'];
        } else if (status == 4) {
            return 'Completed'
            //return [styles.completed, '#FFFFFF', 'Completed', { fontWeight: '600', color: '#b2d100' }];
        } else if (status == 5) {
            return 'Skipped'
            //return [styles.completed, '#FFFFFF', 'Completed', { fontWeight: '600', color: '#b2d100' }];
        } else if (status == -1) {
            return 'Not Completed'
            //return [styles.open, '#446CB3', 'Ticket Open', { fontWeight: '600', color: '#FFFFFF' }];
        } else {
            return 'Invalid Status!'
            //console.warn('invalid status type: ' + this.localData.status)
            //return [styles.invalid, 'red', 'Invalid'];
        }
    }

    /**
     * Returns you to the correct location your coming from:
     * - Details View: printing a single Work Order
     * - Settings View: printing all of the Work Orders
     */
    back() {
        if (this.state.workorder_id != null) {
            this.props.navigation.navigate('Details', {workorder_id: this.state.workorder_id})
        } else {
            this.props.navigation.navigate('Settings')
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={styles.content}>
                    {renderIf(this.state.workorder_id != null)(
                        /* Rendered if the drvier is printing single Work Order */
                        <View>
                            <Text>Date: {this.state.date} Time: {this.state.time}</Text>
                            <Text>Trip Name: {this.state.trip.route}</Text>
                            <Text>Trip id: {this.state.trip.id}</Text>
                            <Text>Truck: {this.state.driver.truck_id}</Text>
                            <View style={{ borderBottomWidth: 2, borderTopWidth: 2, marginBottom: 5, backgroundColor: 'white' }}>
                                <Text>Order id: {this.state.workorder.order_number}</Text>
                                <Text>Status: {this.getStatus(this.state.workorder.status)}</Text>
                                <Text>Client: {this.state.client.name}</Text>
                                <Text>Address: {this.state.client.address}</Text>
                                <Text>Work Order Note: {this.state.workorder.workorder_note}</Text>
                                <Text>Leaving Office Note: {this.state.workorder.before_note}</Text>
                                <Text>Start Note: {this.state.workorder.start_note}</Text>
                                <Text>End Note: {this.state.workorder.end_note}</Text>
                                <Text>Driver Note: {this.state.workorder.driver_note}</Text>


                                {this.completedWorkData(this.state.workorder_id)}
                            </View>
                        </View>
                    )}
                    {renderIf(this.state.workorder_id == null)(
                        /* Rendered if the driver is printing all of the Work Orders */
                        <View>
                            <Text>Date: {this.state.date} Time: {this.state.time}</Text>
                            <Text>Trip Name: {this.state.trip.route}</Text>
                            <Text>Trip id: {this.state.trip.id}</Text>
                            <Text>Truck: {this.state.driver.truck_id}</Text>
                            {this.allCompletedWorkData()}
                        </View>
                    )}
                </ScrollView>
                {/*Child component header*/}
                <Header nav={() => this.back()}/>
                {/*Child component footer*/}
                <Footer nav={this.props.navigation} truck={1} map={1} workorder={1} settings={1}/>
            </View>
        );
    }

    /**
     * Gets the current Date and Time.
     *
     * NOTE: This could possible return a different time if the
     * driver changes the internal clock on the device.
     */
    getDateTime() {
        let datetime = new Date(); //WARNING: Date() needs formating/configuration to show propper amounts
        let date = this.getMonth(datetime.getMonth()) + ' ' + datetime.getDate() + ', ' + datetime.getFullYear();
        let time = this.getActualTime(datetime.getHours()) + ':' + this.getMinutes(datetime.getMinutes()) + ' ' + this.getTimeOfDay(datetime.getHours());
        this.state.date = date;
        this.state.time = time;
    }

    /**
     * Correctly the display of time due to having no zero (0)
     * present if the current minutes less than zero (0)
     *
     * @param {Int} minutes - Within the Time
     * @return {String} Corrected minute display
     */
    getMinutes(minutes) {
        if (minutes < 10) {
            return '0' + minutes
        } else {
            return minutes
        }
    }

    /**
     * Converts the numerical value of month to string
     * @param {*String value of month} month
     */
    getMonth(month) {
        switch (month) {
            case 0:
                return "January";

            case 1:
                return "Febuary";

            case 2:
                return "March";

            case 3:
                return "April";

            case 4:
                return "May";

            case 5:
                return "June";

            case 6:
                return "July";

            case 7:
                return "Augest";

            case 8:
                return "September";

            case 9:
                return "October";

            case 10:
                return "November";

            case 11:
                return "December";

        }
    }

    /**
     * Returns am or pm based on a 24hour clock
     * @param {*Range from 0-24 hours in a day} hour
     */
    getTimeOfDay(hour) {
        if (hour < 12) {
            return 'AM';
        } else {
            console.log(hour)
            return 'PM';
        }
    }

    /**
     * Converts and Returns a 24 hour clock to normal
     * @param {*Range from 0-24 hours in a day} hour
     */
    getActualTime(hour) {
        if (hour > 12) {
            return hour - 12;
        } else if (hour == 0) {
            return 12;
        } else {
            return hour;
        }
    }
}

const window = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        flex: 1,
        flexDirection: 'column',
        position: 'absolute',
        backgroundColor: '#E2E2E3',
        height: window.height - (45 + 70),
        width: window.width,
        bottom: 45,
    },
});
