import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, ScrollView, TouchableOpacity, Switch, Animated, Easing, NetInfo, AsyncStorage} from 'react-native';

import Header from './Header';
import Footer from '../Footer/Footer';
import ModalDropdown from 'react-native-modal-dropdown';
import Timer from 'react-native-timer';


import Icon2 from 'react-native-vector-icons/Octicons';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import * as Progress from 'react-native-progress';

import DBHelper from '../../DBHelper';

/**
 * Parent Component
 * 
 * Refreshes the data within the database
 */
export default class Refresh extends Component {

    constructor(props) {
        super(props)
        this.state = {
            progress: 0, //Loading progress bar
            offline: 0, //Is there internet?
        }
        this.spinValue = new Animated.Value(0) //Animation for the refresh icon
        this.getInternet() //Determines if there is internet (adds event listener)
        this.loadSamples() //Pulls the data from the dummy-api
    }

    /**
     * When the view did load (or its components) calls the
     * spin() function to animate the refresh icon
     */
    componentDidMount () {
        this.spin()
    }
    
    /**
     * Animation that rotates the refresh icon
     * 360 degress continuously 
     */
    spin () {
        this.spinValue.setValue(0)
        Animated.timing(
            this.spinValue,
            {
            toValue: 1,
            duration: 4000,
            easing: Easing.linear
            }
        ).start(() => this.spin())
    }
    
    /**
     * Adds the event listener for detecting internet connection using
     * NetInfo from the device
     */
    getInternet() {
        NetInfo.isConnected.addEventListener('change', this._handleConnectionChange);
    }

    /**
     * Changes the state of offline when the internet connection changes
     */
    _handleConnectionChange = (isConnected) => {
        this.state.offline = isConnected
        if (isConnected) {
            console.log('Has internet? ' + isConnected);
        } else {
            console.log('Not connected');
        }
        //this.forceUpdate();
    };

    render() {
        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <View style={styles.row}>
                        <Image 
                            style={styles.logo}
                            source={require('../../images/logo-with-text-white-low.png')}
                        /> 
                    </View>
                    <View style={styles.bigRow}>
                        <Animated.Image
                            style={[styles.loading, { transform: [{ rotate: spin }] } ]}
                            source={require('../../images/refresh-grey-256.png')}
                        />

                        {/*Progress Bar*/}
                        <Progress.Bar
                            style={styles.progressBar}
                            width={window.width - 60}
                            progress={this.state.progress}
                            color={'#ff7043'}
                            unfilledColor={'white'}
                        />
                    </View>
                    <View style={styles.row}>
                        
                    </View>
                </View>
                {/*Child component header*/}
                <Header />
            </View>
        );
    }

    /**
     * Calls the sample data hosted on the
     * dummy.managepetro.com website
     */
    async loadSamples() {
        NetInfo.isConnected.removeEventListener('change', this._handleConnectionChange);
        if (this.state.offline) {
            //console.log('back: ' + this.props.navigation.state.params.back)
            //this.props.navigation.navigate(this.props.navigation.state.params.back)
            console.log('should be called: ' + this.state.offline)
            this.props.navigation.navigate('Map')
            return
        }

        //Used for the progress bar
        var progress = 0 //current progress of the refreshing (or loading)
        let numberOfProgresses = 29 //number of processes that happened after this line
        let increment = 1 / numberOfProgresses //how the progress bar should increment by
        
        DBHelper.removeAll('Driver')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Truck')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Compartment')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('WorkOrder') //PROBLEM WITH OPEN TICKET !!!!!!!! AND ONE THAT HAS BEEN CLOSED!
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Task')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Clients')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Contacts')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Fuel')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Refinery_Locations')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Log')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Trip')
        progress += increment
        DBHelper.removeAll('Unit')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Refinery')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Checklist')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Checkitems')
        progress += increment
        this.state.progress = progress
        DBHelper.removeAll('Documents')
        progress += increment
        this.state.progress = progress



        //DBHelper.removeAll('Driver');
        //let array = [-1, 'teiweb', 'TEIWEB', '604-644-1334', 'hello@hello.ca', 'teiweb', 52];
        //let array2 = ['hello', 'hello', 'hello', 'hello', 'hello', 'hello', 22];
        // let stuff = DBHelper.generateDriver(array);
        // for (var key in stuff) {
        //     console.warn(stuff[key]);
        // }
        //DBHelper.add('Driver', array);
        //DBHelper.update('Driver', 1, array2);

        //console.warn(DBHelper.getAll('Driver').toString());
        //DBHelper.printAll('Driver');

        // let test = {
        //     id: 1,
        //     username: 'test',
        //     password: 'test',
        //     phone: 'test',
        //     email: 'test',
        //     name: 'test',
        //     truck_id: 52,
        // }
        // for (var key in test) {
        //     console.warn(test[key]);
        // }


        //Basic URL to API
        let http = 'http://dummy.managepetro.com/';

        /**
         * --------GENERAL NOTE:--------
         *
         * If you see:
         * //MULTIPLE: means that the JSON request contains an Array data
         * else:
         * //SINGLE: means that the JSON request contains only one piece of data
         */

        //LOAD LOCATIONS
        try {
            let table = 'Locations'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'locations');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Locations--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

            }
        } catch (error) {
            console.warn("Failed to Load Locations Data \n" + error);
        }
        /**
         * --------GENERAL NOTE:--------
         *
         * If you see:
         * //MULTIPLE: means that the JSON request contains an Array data
         * else:
         * //SINGLE: means that the JSON request contains only one piece of data
         */


        //LOAD VENDOR
        try {
            let table = 'Vendor'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'vendor');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Vendor--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

            }
        } catch (error) {
            console.warn("Failed to Load Vendor Data \n" + error);
        }

        //LOAD USERS
        try {
            //DBHelper.removeAll('Driver');
            let table = 'Driver'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'driver');
                let json = await response.json();
                console.log("\n--json[Driver]--\n" + "\n\n");
                console.log(json);

                // //MULTIPLE
                // for (let i = 0; i < json.length; i++) {
                //
                //     let data = [];
                //
                //     // If the sample data comes in with no id, you must add a zero to the beginning and
                //     // Must! change the DBHelper "add" function to include the table that is missing id
                //     // So that the id is auto increment.
                //     //-----CODE:-------
                //     // data.push(0)
                //
                //     Parses the json into an array
                //     for (item in json[i]) {
                //         console.log(json[i][item]);
                //         data.push(json[i][item]);
                //     }
                //     console.log("\n\n" + data + "\n\n");
                //     DBHelper.add(table, data);
                // }

                //SINGULAR
                let data = [];

                // If the sample data comes in with no id, you must add a zero to the beginning and
                // Must! change the DBHelper "add" function to include the table that is missing id
                // So that the id is auto increment.
                //-----CODE:-------
                //data.push(0)

                //Parses the json into an array
                for (item in json) {

                    console.log("\n--Driver  username--:" + json[item].username + "\n\n");
                    DBHelper.addDriver(table, json[item]);
                    data.push(json[item]);
                }

                console.log("\n--Driver--\n" + data + "\n\n");
                //DBHelper.add(table, data);


            }
        } catch (error) {
            console.warn("Failed to Load Driver Data \n" + error);
        }

        //LOAD TRUCKS
        try {
            let table = 'Truck'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'truck');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)

                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Truck--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Truck Data \n" + error);
        }

        //LOAD Compartments
        try {
            let table = 'Compartment'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'compartment');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)

                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Compartment--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Compartment Data \n" + error);
        }

        //LOAD Routes
        try {

            let table = 'Trip'
            DBHelper.removeAll(table);
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'trip');
                let json = await response.json();
                console.log("\n--Trip--\n" + "\n\n");
                console.log(json);

                //Parses the json into an array
                for (item in json) {

                    console.log("\n--Trip  route--:" + json[item].route + "\n\n");
                    DBHelper.addTrip(table, json[item]);

                }


                //DBHelper.add(table, data);


            }
        } catch (error) {
            console.warn("Failed to Load Trip/Route Data \n" + error);
        }
        //LOAD WorkOrders
        try {

            let table = 'WorkOrder'

            DBHelper.removeAll(table);
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'workorder');
                let json = await response.json();
                console.log("\n--WorkOrder--\n" + "\n\n");
                console.log(json);

                //Parses the json into an array
                for (item in json) {

                    console.log("\n--WorkOrder  address--:" + json[item].address + "\n\n");
                    DBHelper.addWorkOrder(table, json[item]);

                }


                //DBHelper.add(table, data);


            }
        } catch (error) {
            console.warn("Failed to Load WorkOrder Data \n" + error);
        }

        //LOAD Units
        try {
            let table = 'Unit'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'units');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)

                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Unit--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Task Data \n" + error);
        }
        //LOAD Tasks
        try {
            let table = 'Task'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'task');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)

                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Task--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Task Data \n" + error);
        }

        //LOAD Clients
        try {
            let table = 'Clients'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'clients');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)

                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Clients--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Clients Data \n" + error);
        }

        //LOAD Fuel
        try {
            let table = 'Fuel'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'fuels');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)

                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Fuel--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Fuel Data \n" + error);
        }



        //LOAD Log
        try {
            let table = 'Log'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'log');
                let json = await response.json();
                console.log(json);

                // //MULTIPLE
                // for (let i = 0; i < json.length; i++) {

                //     let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // //-----CODE:-------
                // //data.push(0)

                //     //Parses the json into an array
                //     for (item in json[i]) {
                //         data.push(json[i][item]);
                //     }
                //     console.log("\n\n" + data + "\n\n");
                //     DBHelper.add(table, data);
                // }

                //SINGULAR
                let data = [];

                // If the sample data comes in with no id, you must add a zero to the beginning and
                // Must! change the DBHelper "add" function to include the table that is missing id
                // So that the id is auto increment.
                //-----CODE:-------
                data.push(0)

                //Parses the json into an array
                for (item in json) {
                    console.log(json[item]);
                    data.push(json[item]);
                }

                console.log("\n--Log--\n" + data + "\n\n");
                DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Log Data \n" + error);
        }

        //LOAD Checklist
        try {
            let table = 'Checklist'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'checklist');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)

                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Checklist--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Checklist Data \n" + error);
        }

        //LOAD Checkitems
        try {
            let table = 'Checkitems'
            if (!DBHelper.getAll(table).length) {
                let response = await fetch(http + 'checkitems');
                let json = await response.json();
                console.log(json);

                //MULTIPLE
                for (let i = 0; i < json.length; i++) {

                    let data = [];

                    // If the sample data comes in with no id, you must add a zero to the beginning and
                    // Must! change the DBHelper "add" function to include the table that is missing id
                    // So that the id is auto increment.
                    //-----CODE:-------
                    //data.push(0)

                    //Parses the json into an array
                    for (item in json[i]) {
                        data.push(json[i][item]);
                    }
                    console.log("\n--Checkitems--\n" + data + "\n\n");
                    DBHelper.add(table, data);
                }

                // //SINGULAR
                // let data = [];

                // // If the sample data comes in with no id, you must add a zero to the beginning and
                // // Must! change the DBHelper "add" function to include the table that is missing id
                // // So that the id is auto increment.
                // // -----CODE:-------
                // //data.push(0)

                // //Parses the json into an array
                // for (item in json) {
                //     console.log(json[item]);
                //     data.push(json[item]);
                // }

                //console.log("\n\n" + data + "\n\n");
                //DBHelper.add(table, data);
            }
        } catch (error) {
            console.warn("Failed to Load Checkitems Data \n" + error);
        }

        this.setState({
            progress: 1
        })

        if (this.state.progress == 1) {
            //console.log('back: ' + this.props.navigation.state.params.back)
            //this.props.navigation.navigate(this.props.navigation.state.params.back)
            
            try {
                AsyncStorage.setItem('@refreshed', 'green');
            } catch (error) {
                this.props.navigation.navigate('ErrorHandler', {error: error, type: 'AsyncStorage', class: Refresh.name})
                // Error saving data
            }

            Timer.setInterval(
                'refresh',
                () => this.refreshed(),
                20000
            )
            this.props.navigation.navigate('Change')
        }
    }

    refreshed() {
        console.log('Timer has been called')
        AsyncStorage.removeItem('@refreshed', (err) => {
            // keys k1 & k2 removed, if they existed
            // do most stuff after removal (if you want)
        })
        Timer.clearInterval('refresh')
    }
}

const window = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        flex: 1,
        flexDirection: 'column',
        position: 'absolute',
        backgroundColor: '#78909c',
        height: window.height - (70),
        width: window.width,
        justifyContent: 'center',
        alignItems: 'center',
        top: 70,
    },
    logo: {
        width: 100,
        height: 100,
    },
    loading: {
        width: 150,
        height: 150,
    },
    row: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        flex: 0.25
    },
    bigRow: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 0.50
    },
    progressBar: {
        margin: 30,
        marginBottom: 0,
    }
});
