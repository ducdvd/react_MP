import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, TouchableOpacity, Geolocation, AsyncStorage} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

import SignaturePad from 'react-native-signature-pad';
import Timer from 'react-native-timer';
import renderIf from '../../../config/renderif';
import DBHelper from '../../../DBHelper';
import Header from './Header';

/**
 * Parent Component
 *
 * Displays the information about ManagePetro
 */
export default class About extends Component {

    constructor(props) {
        super(props)
    }

    /**
     * Navigates back to the Settings View
     */
    back() {
        this.props.navigation.navigate('Settings')
    }

    /**
     * Testng AsynStorage
     */
    getMessage() {
        try {
            AsyncStorage.getItem('@MySuperStore:key', (err, result) => {
                console.warn(result);
                if (result !== null) {
                    // We have data!!
                    return '' + result
                }
            })

        } catch (error) {
        // Error retrieving data
        }
    }

    //Sample:
    // Managing your fuel delivery ordering and sales process can be a critical challenge that
    // includes many important tasks, such as daily work orders, invoicing, fuel forecasting,
    // managing costs & prices across different regions, monitoring your fleet, and the list goes on
    // and on. Here is where we help. Manage Petro’s fuel management solution (MP-FMS) was
    // developed with your best interests in mind. Our system offers automated and real-time
    // monitoring of your fuel delivery business so you have full control over your management and
    // operations. Here are just some of the features the system offers you:

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <Text style={styles.title}>What Manage Petro can do for your business</Text>
                    <Text style={styles.text}>
                        {this.getMessage()}

                    </Text>
                </View>
                <Header nav={() => this.back()}/>
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        backgroundColor: '#E2E2E3',
        alignItems: 'center',
        height: window.height - (70),
        width: window.width,
        top: 70,
    },
    rowLayout: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#575C64',
        height: 30,
    },
    title: {
        marginTop: 20,
        marginBottom: 10,
        fontWeight: '700',
        fontSize: 18,
        textAlign: 'center'
    },
    text: {
        textAlign: 'center'
    }
});
