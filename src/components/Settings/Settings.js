import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, ScrollView, TouchableOpacity, Switch } from 'react-native';

import Header from './Header';
import Footer from '../Footer/Footer';
import ModalDropdown from 'react-native-modal-dropdown';

import Icon2 from 'react-native-vector-icons/Octicons';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import renderIf from '../../config/renderif';
import DBHelper from '../../DBHelper';

/**
 * Parent Component
 *
 * Gives the driver options or customizations for the application
 * Also, provides information about Manage Petro
 */
export default class Settings extends Component {

    constructor(props) {
        super(props)
    }

    /**
     * Unused: List of options for dropdown
     */
    list = [
        'Show all completed Work Orders',
        'Show one completed Work Order',
        'Hide all completed Work Orders'
    ];

    /**
     * Called when the dropdown menu option is selected
     * within the dropdown.
     */
    getSelectedOption() {
        let selected = 0;
        switch (selected) {
            case 0:
                return [this.list[0], 0];
            case 1:
                return [this.list[1], 1];
            case 2:
                return [this.list[2], 2];
        }
    }

    /**
     * Logs out the driver from the application
     */
    logout() {
        //Removed the stored AsyncStorage information (API TOKEN/KEY)
        
        //do stuff here
        this.props.navigation.navigate('Login')
    }

    /**
     * Determines if there is any complete workorders to display
     * else it will alert (Display) that there are no completed Work Orders.
     */
    printCompleteWork() {
        // let trip = DBHelper.getAll('Trip').filtered('active = 1')
        // if (trip != null && trip.length != 0) {
        //     let completed = DBHelper.getAll('WorkOrder').filtered('status = 4 && trip_id =' + trip[0].id)
        //     if (completed != null && completed.length != 0) {
        //         this.props.navigation.navigate('Print', {workorder_id: null})
        //     } else {
        //         alert('No Work Orders are Complete')
        //     }
        // } else {
        //     alert('No Work Orders are Complete')
        // }
        this.props.navigation.navigate('Print', {workorder_id: null, trip_id: null})
    }

    resertDatabases(){
        this.props.navigation.navigate('ResertDB')

    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={styles.content}>
                    <View style={styles.setttingContainer}>
                        <View style={styles.settingLayout}>
                            
                            {/*Work Order Filters Options*/}
                            {/* <Text style={styles.settingText}>Work Order Filters</Text>
                            <View style={styles.dropdownContainer}>
                                <ModalDropdown
                                    style={styles.dropdown}
                                    textStyle={styles.dropdownText}
                                    dropdownStyle={styles.dropdownBox}
                                    dropdownTextStyle={styles.dropdownOption}
                                    dropdownTextHighlightStyle={styles.dropdownSelectedOption}
                                    options={this.list}
                                    defaultIndex={this.getSelectedOption()[1]}
                                    defaultValue={this.getSelectedOption()[0]}
                                />
                            </View> */}

                            {/* Button to display Work Orders */}
                            {renderIf(DBHelper.getAll('Trip').filtered('active =1').length > 0)(
                            <TouchableOpacity style={styles.settingButton} onPress={() => this.printCompleteWork()}>
                                <Text style={styles.settingButtonText} >Print Current Trip Work Orders</Text>
                            </TouchableOpacity>
                            )}
                            <TouchableOpacity style={styles.settingButton} onPress={() => this.props.navigation.navigate('Trip')}>
                                <Text style={styles.settingButtonText} >Completed Trips</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.settingButton} onPress={() => this.resertDatabases()}>
                                <Text style={styles.settingButtonText} >Reset Databases</Text>
                            </TouchableOpacity>
                        </View>
                        {/*Color blind mode switch*/}
                        {/* <View style={[styles.settingLayout, styles.rowLayout]}>
                            <Text style={styles.settingText}>Color Blind Mode</Text>
                            <Switch style={styles.switch}/>
                        </View> */}
                    </View>

                    <View style={styles.infoContainer}>
                        {/*About us button*/}
                        <View style={styles.infoLayout}>
                            <TouchableOpacity style={[styles.infoTouch, styles.rowLayout]} onPress={() => this.props.navigation.navigate('About')}>
                                <Text style={styles.infoText}>About Us</Text>
                                <Icon name={'arrow-right'} size={20} style={styles.icon}></Icon>
                                {/* <Text>X</Text> */}
                            </TouchableOpacity>
                        </View>
                        {/*Terms and Services button*/}
                        <View style={styles.infoLayoutMiddle}>
                            <TouchableOpacity style={[styles.infoTouch, styles.rowLayout]} onPress={() => this.props.navigation.navigate('Terms')}>
                                <Text style={styles.infoText}>Terms and Services</Text>
                                <Icon name={'arrow-right'} size={20} style={styles.icon}></Icon>
                            </TouchableOpacity>
                        </View>
                        {/*Terms and Services button*/}
                        <View style={styles.infoLayoutMiddle}>
                            <TouchableOpacity style={[styles.infoTouch, styles.rowLayout]} onPress={() => this.props.navigation.navigate('Privacy')}>
                                <Text style={styles.infoText}>Privacy</Text>
                                <Icon name={'arrow-right'} size={20} style={styles.icon}></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>
                    {/*Log out button*/}
                    <View style={styles.logoutContainer}>
                        <TouchableOpacity style={styles.logoutButton} onPress={() => this.logout()}>
                            <Text style={styles.logoutText}>Log Out</Text>
                        </TouchableOpacity>
                        {/*Button With Borders*/}
                    </View>
                </ScrollView>
                {/*Child component header*/}
                <Header />
                {/*Child component footer*/}
                <Footer nav={this.props.navigation} truck={1} map={1} workorder={1} settings={0}/>
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        flex: 1,
        flexDirection: 'column',
        position: 'absolute',
        backgroundColor: '#E2E2E3',
        height: window.height - (45 + 70),
        width: window.width,
        bottom: 45,
    },
    setttingContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        paddingTop: 40,
        paddingLeft: 40,
        paddingRight: 40,
        height: 250,
    },
    settingLayout: {
        flex: 1,
        justifyContent: 'center',
    },
    settingText: {
        color: '#555555',
        fontSize: 16,
        fontWeight: '800',
    },
    dropdownContainer: {
        backgroundColor: 'white',
        borderRadius: 5,
        borderColor: '#bbbbbb',
        borderWidth: 0.25,
        height: 30,
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 10,
        paddingRight: 10,
        paddingLeft: 10,
    },
    dropdown: {
        //Could add styles later
    },
    dropdownBox: {
        height: 114,
        borderRadius: 5,
    },
    dropdownText: {
        fontSize: 12,
        color: '#888888',
    },
    dropdownOption: {
        fontSize: 12,
    },
    dropdownSelectedOption: {
        color: 'black',
        fontWeight: '700',
        fontSize: 12,
    },
    infoContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        height: 100,
    },
    infoLayout: {
        flex: 1,
        borderColor: 'grey',
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        paddingTop: 5,
        paddingBottom: 5,
    },
    infoLayoutMiddle: {
        flex: 1,
        borderColor: 'grey',
        borderBottomWidth: 0.5,
        paddingTop: 5,
        paddingBottom: 5,
    },
    infoTouch: {
        flex: 1,
        paddingLeft: 40,
        paddingRight: 40,
    },
    infoText: {
        color: '#808080',
        fontWeight: '500',
    },
    switch: {
        borderRadius: 15,
        borderColor: '#bbbbbb',
        borderWidth: 1,
        backgroundColor: '#FFFFFF',
    },
    logoutContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        height: 200,
        paddingLeft: 40,
        paddingRight: 40,
    },
    logoutButton: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#78909c',
        borderRadius: 5,
        height: 40,
    },
    logoutText: {
        color: '#FFFFFF',
        fontWeight: '800',
    },
    rowLayout: {
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    icon: {
        color: '#78909c',
        fontWeight: '700'
    },
    settingButton: {
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffa726',
        borderRadius: 5,
        height: 40,
    },
    settingButtonText: {
        color: '#FFFFFF',
        fontWeight: '600',
    },
});
