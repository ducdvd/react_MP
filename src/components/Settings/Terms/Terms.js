import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, TouchableOpacity, ScrollView} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

import SignaturePad from 'react-native-signature-pad';
import Timer from 'react-native-timer';

import renderIf from '../../../config/renderif';
import DBHelper from '../../../DBHelper';

import Header from './Header';

export default class Terms extends Component {

    constructor(props) {
        super(props)
        this.state = {

        };
    }

    /**
     * Navigates back to the settings view
     */
    back() {
        this.props.navigation.navigate('Settings')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    {/* To Scroll through large amount of text */}
                     <ScrollView>  
                        <Text>
                            {/* SAMPLE: */}
                            Is a Terms and Conditions required?

                            A Terms and Conditions is not required and it’s not mandatory by law. Unlike Privacy Policies, there’s no law or regulation on Terms and Conditions.

                            But having a Terms and Conditions gives you the right to terminate the access of abusive users or to terminate the access to users who do not follow your rules and guidelines.

                            It’s extremely important to have this agreement if you operate a SaaS app.

                            Here are a few examples how this agreement can help you:

                            If users abuse your website or mobile app in any way, you can terminate their account. Your “Termination” clause can inform users that their accounts would be terminated if they abuse your service.
                            If users can post content on website or mobile app (create content and share it on your platform), you can remove any content they created if it infringes copyright.
                            Your Terms and Conditions will inform users that they can only create and/or share content they own rights to.

                            Similarly, if users can register for an account and choose an username, you can inform users that they are not allowed to choose usernames that may infringe trademarks, i.e. usernames like Google, Facebook, and so on.

                            If you sell products or services, you could cancel specific orders if a product price is incorrect. Your Terms and Conditions can include a clause to inform users that certain orders, at your sole discretion, can be canceled if the products ordered have incorrect prices due to various errors.
                            And many more examples.
                            While a Terms and Conditions isn’t required by law, other platforms (especially those that you can create an app for) will often ask you for this agreement.
                        </Text>
                    </ScrollView>
                </View>
                <Header nav={() => this.back()}/>
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        backgroundColor: '#E2E2E3',
        alignItems: 'center',
        height: window.height - (70),
        width: window.width,
        top: 70,
    },
    rowLayout: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#575C64',
        height: 30,
    },
    title: {
        marginTop: 20,
        marginBottom: 10,
        fontWeight: '700',
        fontSize: 18,
        textAlign: 'center'
    },
    text: {
        textAlign: 'center'
    }
});
