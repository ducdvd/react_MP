import React, { Component}  from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, TouchableOpacity, Geolocation, Alert, TextInput, KeyboardAvoidingView, ScrollView, Keyboard } from 'react-native';

import ModalDropdown from 'react-native-modal-dropdown';
import { CheckboxField, Checkbox } from 'react-native-checkbox-field';
import renderIf from '../../../config/renderif';
import DBHelper from '../../../DBHelper';
import Header from './Header';
import Footer from '../../Footer/Footer'



export default class Unit extends Component {

    /**
     * Defines the state and calls certain function before renderingthe view.
     *
     * @param {object} props - Contains navigation methods and information about previous component
     */
    constructor(props) {
        super(props)

        this.state = { //when component view is rendered use setState({})
            trip_id: props.navigation.state.params.trip_id, //from previous view
            
            //DB Object
            trip: null,

            name: '',
        }
        this.getTrip()
    }

    getTrip() {
        this.state.trip = DBHelper.getById('Trip', this.state.trip_id);
    }

    render() {
        return (
            <View behavior="padding" style={styles.container}>
                <View style={styles.content}>
                    <ScrollView style={styles.scrollContainer}>
                        {/* Wrapped in a TouchableOpacity to close the keyboard */}
                        <TouchableOpacity activeOpacity={1} onPress={() => Keyboard.dismiss()}>  
                            <KeyboardAvoidingView behavior='padding'>
                                <View style={styles.itemContainer}>
                                    <View style={styles.rowLayout}>
                                        <View style={styles.left}>
                                            <Text style={styles.label}>Delivery Trip:</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <Text>{this.state.trip.route}</Text>
                                        </View>
                                    </View>
                                </View>

                                {/* Confirmation Button */}
                                <View style={styles.confirmContainer}>
                                    <TouchableOpacity style={styles.confirmButtonContainer} onPress={() => this.props.navigation.navigate('Print', {workorder_id: null, trip_id: this.state.trip_id})}>
                                        <Text style={styles.confirmText}>Print Manifest</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.confirmContainer}>
                                    <TouchableOpacity style={styles.confirmButtonContainer} onPress={() => this.props.navigation.navigate('Print', {workorder_id: null, trip_id: this.state.trip_id})}>
                                        <Text style={styles.confirmText}>Print Work Orders</Text>
                                    </TouchableOpacity>
                                </View>
                            </KeyboardAvoidingView>
                        </TouchableOpacity>
                    </ScrollView>
                </View >
                <Footer nav={this.props.navigation} truck={1} map={1} workorder={1} settings={0} />
                <Header nav={this.props.navigation}/>
            </View>
        );
    }
}

//Gets the size of the mobile device (in pixel's)
const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1, //like bootstraps xs-lg-12 but without dropping down
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        alignItems: 'center',
        width: window.width,
        height: window.height - (45 + 70),
        bottom: 45,
    },
    scrollContainer: {
        height: window.height - 70,
    },
    rowLayout: {
        flexDirection: 'row',
        paddingBottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 10,
    },
    rowStyles: {
        borderBottomWidth: 1,
        borderColor: '#BDC3C7',
        margin: 8,
        marginBottom: 0,
        paddingBottom: 8,
    },

    left: {
        flex: 0.5,
        alignItems: 'center',
        paddingRight: 10,
        justifyContent: 'center',
    },
    right: {
        flex: 0.5,
        alignItems: 'center',
        paddingLeft: 10,
    },
  
    mainText: {
        color: 'white',
        fontSize: 16,
        fontWeight: '700',
    },
    secondaryText: {
        color: 'white',
        fontSize: 14,
        fontWeight: '600',
    },

    confirmButtonContainer: {
        backgroundColor: '#446CB3',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: window.width - 20,
        borderRadius: 5,
        margin: 20,
    },
    confirmText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: '700',
    },
    confirmContainer: {
        width: window.width,
        alignItems: 'center',
    },

    itemContainer: {
        paddingTop: 10,
        width: window.width,
        alignItems: 'center',
    },

    label: {
        fontSize: 16,
        fontWeight: '800',
    },
});
