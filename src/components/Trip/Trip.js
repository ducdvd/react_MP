import React, { Component} from 'react';
import { Animated, Platform, StyleSheet, View, Image, Text, Button, StatusBar, Dimensions, ScrollView, TouchableOpacity, Alert, TextInput, Linking, Geolocation} from 'react-native';

import renderIf from '../../config/renderif';
import DBHelper from '../../DBHelper';
import SortableList from 'react-native-sortable-list';
import Row from './Row';
import Icon2 from 'react-native-vector-icons/Octicons';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import Header from './Header';
import Footer from '../Footer/Footer';
import ImagePicker from 'react-native-image-picker';

/**
 * Parent Component
 *
 * 
 */
export default class Trip extends Component {

    constructor(props) {
        super(props)

        this.activeView = 1;
        this.state = {
            trip: null,
        }
        this.getTrips() //Gets all Task Realm Object
    }

    /**
     * Prints the current order of keys for row's
     * See: _renderRow() method
     *
     * @param {Array} array - of row's keys
     */
    printCurrentRowKeys(array) {
        //console.log(array);
    }

    /**
     * This defines the row component and how it will be rendered
     * Please see: Row.js
     * @return {Array} 
     */
    _renderRow = ({data, active}) => {
        return <Row data={data} active={active} navigation={this.props.navigation}/>
    }

    /**
     * Gets all of the Task Realm Objects from the database
     */
    getTrips() {
        //DB_Trip = DBHelper.getAll('Trip').filtered('active =' + 1)
        DB_Trip = DBHelper.getAll('Trip')
        this.state.trip = DB_Trip
    }
    
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <SortableList
                        style={styles.list}
                        contentContainerStyle={styles.listContainer}
                        data={this.state.trip}
                        renderRow={this._renderRow}
                        sortingEnabled={false}
                    />
                </View>
                <Header nav={this.props.navigation}/>
                <Footer nav={this.props.navigation} truck={1} map={1} workorder={1} settings={0} />
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        height: window.height - (45 + 70),
        width: window.width,
        bottom: 45,
    },
    buttonContainer: {
        flexDirection: 'row',
        height: 60,
    },
    buttonLeftContainer: {
        flex: 0.5,
        backgroundColor: '#79909B',
    },
    buttonLeft: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
    },
    buttonRightContainer: {
        flex: 0.5,
        backgroundColor: '#656F78',
    },
    buttonRight: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
    },
    bar: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#F98B24',
        height: 4,
        width: window.width/2,
    },
    list: {
        flex: 1,
    },
    listContainer: {
        width: window.width,

        ...Platform.select({
        ios: {
            paddingHorizontal: 5,
        },

        android: {
            paddingHorizontal: 0,
        }
        })
    },
    detailsContainer: {
        flex: 1,
        width: window.width,
        paddingTop: 10,
    },
    title: {
        color: 'black',
        fontSize: 18,
        fontWeight: '700',
    },
    address: {
        color: '#888888',
        fontSize: 13,
    },
    coordinates: {
        color: '#888888',
        fontSize: 12,
    },
    noteContainer: {
        backgroundColor: '#ffe88a',
        marginTop: 10,
        marginBottom: 10,
        margin: 20,
        padding: 10,
    },
    startButtonContainer: {
        marginTop: 0,
        margin: 10,
        backgroundColor: '#ffa726',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: window.width - 40,
        borderRadius: 5,
    },
    startText: {
        color: '#FFFFFF',
        fontSize: 18,
        fontWeight: '700',
    },
    directionsButtonContainer: {
        marginTop: 0,
        margin: 10,
        backgroundColor: '#4183D7',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: window.width - 40,
        borderRadius: 5,
    },
    directionsText: {
        color: '#FFFFFF',
        fontSize: 18,
        fontWeight: '700',
    },
    printButtonContainer: {
        marginTop: 0,
        margin: 10,
        backgroundColor: '#26A65B',
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
        width: window.width - 180,
        borderRadius: 5,
    },
    printText: {
        color: '#FFFFFF',
        fontSize: 18,
        fontWeight: '700',
    },
    attachmentLabels: {
        color: '#607d8b',
        fontSize: 15,
        fontWeight: '800',
    },
    attachmentContainer: {
        paddingLeft: 10,
        paddingRight: 10,
        width: window.width - 20,
        borderBottomWidth: 1,
        borderColor: '#dedede',
        marginBottom: 10,
    },
    attachmentLeft: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        flex: 0.5,
    },
    attachmentRight: {
        alignItems: 'flex-end',
        flex: 0.5,
        paddingRight: 20,
    },
    rowLayout: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    addButton: {
        color: '#607d8b',
        fontSize: 25,
        fontWeight: '900',
    },
    completeButton: {
        height: 60,
        width: window.width - 20,
        backgroundColor: '#ebebeb',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        marginBottom: 50,
    },
    compeleteText: {
        color: '#78909c',
        fontSize: 14,
    },
    noteTitleContainer: {
        height: 23,
        paddingRight: 5,
    },
    noteTitle: {
        color: '#ff5722',
        fontSize: 16,
        fontWeight: '700',
    },
    noteMoreButton: {
        alignItems: 'center',
        height: 22,
        width: 30,
    },
    noteBox: {
        paddingBottom: 5,
        flexWrap: 'wrap',
    },
    driverNoteContainer: {
        
    },
    editText: {
        color: '#607d8b',
        fontSize: 16,
        fontWeight: '700',
    },
    driverNoteText: {
        fontSize: 14,
    },
    docTitleContainer: {
        alignItems: 'center',
    },
    docTitle: {
        color: '#607d8b',
        fontSize: 12,
        fontWeight: '700',
    },
    imageContainer: {
        alignItems: 'center',
        margin: 5,
    },
    image: {
        width: window.width - 100,
        height: 400,
    },
    removeDocContainer: {
        backgroundColor: '#EF4836',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        width: 80,
        height: 20,
    },
    removeDocText: {
        fontSize: 14,
        color: 'white'
    },

});