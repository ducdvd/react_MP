import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, TextInput, Button, Dimensions, ScrollView, TouchableOpacity, Switch, KeyboardAvoidingView, Alert } from 'react-native';

import ModalDropdown from 'react-native-modal-dropdown';
import Prompt from 'react-native-prompt';
import DBHelper from '../../../DBHelper';
import Header from './Header';
import Footer from '../../Footer/Footer';
import Icon2 from 'react-native-vector-icons/Octicons';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

/**
 * Parent Component
 *
 * Adding Fuel view is only seen when a user clicks the
 * add fuel button on the truck.js view
 *
 * Change View Aka. Truck Selection allows the driver to change his/her
 * Trip and Truck that is currently "Active" in the system
 */
export default class Change extends Component {

    //Driver Default is the first one
    constructor(props) {
        super(props)
        this.state = {
            trips_color: null, //Array of active id's for trips
            trip_selected: null, //Current selected trip
            trip_id_default: null, //Default trip
            truck: null, //Truck Realm Object
            truckID: null, //Truck Realm Object
            empty_error: false
        }
        this.getColor() //Determines if a trip is active and gets a active color

        //Driver Default is the first one
        DBHelper.updateElementAndById('Trip', this.state.trip_id_default, 'active', 1)

        //let default_route = DBHelper.getAll('Driver')[0].route_id

        let default_route = DBHelper.getById('Driver',1).route_id


        this.state.trip_id_defaul = default_route
        console.warn("Driver Default:" + default_route + '=1');
        this.selectActiveTrip(parseInt(default_route) - 1);
        this.state.trip_selected = default_route
        this.state.trips_color[parseInt(default_route) - 1] = 1
        
    };

    /**
     * Get the active id of a trip and stores it within
     * the trisps_color state array.
     */
    getColor() {
        let DB_trips = DBHelper.getAll('Trip')
        let color = []
        for (let i = 0; i < DB_trips.length; i++) {
            if (DB_trips[i].active == 1) {
                this.state.trip_selected = DB_trips[i].id
            }
            color.push(DB_trips[i].active)
        }
        this.state.trips_color = color
    }

    /**
     * Returns the corresponding styles for a trip depending on its
     * active id.
     *
     * @param {Int} active - Selected or Active Trip 
     */
    getActiveTrip(active) {
        if (active == 1) {
            return [styles.selectedTrip, styles.selectedText]
        }
        return [styles.trip, styles.text]
    }

    /**
     * Gets the trip information and creates a view that displays its information.
     * The created view is stored within the trip_views array.
     *
     * @return {Array} Trip Views
     */
    getTrips() {
        let DB_trips = DBHelper.getAll('Trip')
        let trip_views = []
        for (let i = 0; i < DB_trips.length; i++) {
            console.log("Trip lenght:"+DB_trips.length )
            let workorders = DBHelper.getAll('WorkOrder').filtered('trip_id =' + DB_trips[i].id)
            if (i == (DB_trips.length - 1)) {
                
                trip_views.push(
                    <TouchableOpacity
                        key={DB_trips[i].id}
                        style={[styles.rowLayout, this.getActiveTrip(this.state.trips_color[i])[0], styles.lastTrip]}
                        onPress={() => this.selectActiveTrip(i)}
                    >
                        <View style={styles.section}>
                            <Text style={this.getActiveTrip(this.state.trips_color[i])[1]}> {DB_trips[i].route} </Text>
                        </View>
                        <View style={styles.section}>
                            <Text style={this.getActiveTrip(this.state.trips_color[i])[1]}> {workorders.length} </Text>
                        </View>
                        <View style={styles.section}>
                            <Text style={this.getActiveTrip(this.state.trips_color[i])[1]}> {DB_trips[i].status} </Text>
                        </View>
                    </TouchableOpacity>
                )
            } else {
                trip_views.push(
                    <TouchableOpacity
                        key={DB_trips[i].id}    
                        style={[styles.rowLayout, this.getActiveTrip(this.state.trips_color[i])[0]]}
                        onPress={() => this.selectActiveTrip(i)}
                    >
                        <View style={styles.section}>
                            <Text style={this.getActiveTrip(this.state.trips_color[i])[1]}> {DB_trips[i].route} </Text>
                        </View>
                        <View style={styles.section}>
                            <Text style={this.getActiveTrip(this.state.trips_color[i])[1]}> {workorders.length} </Text>
                        </View>
                        <View style={styles.section}>
                            <Text style={this.getActiveTrip(this.state.trips_color[i])[1]}> {DB_trips[i].status} </Text>
                        </View>
                    </TouchableOpacity>
                )
            }
        }
        return trip_views
    }

    /**
     * Called then a trip is selected. Updates the Trips state info
     *
     * @param {Int} index Position of the trip within the list 
     */
    selectActiveTrip(index) {
        var newColor = this.state.trips_color
        for (let i = 0; i < newColor.length; i++) {
            if (i == index) {
                this.setState({
                    trip_selected: DBHelper.get('Trip', index).id
                })
                newColor[i] = 1
            } else {
                newColor[i] = 0
            }
        }
        this.setState({
            trips_color: newColor
        })
    }

    /**
     * Determines if the trip selected is not already selected.
     *
     * @return {Boolean} Trip can be selected
     */
    getTripSelected() {
        let color = this.state.trips_color
        for (let i = 0; i < color.length; i++) {
            if (color[i] == 1) {
                return true
            }
        }
        return false;
    }

    /**
     * Returns the color of an active or deactive trip.
     *
     * @param {Boolean} selected - If the trip is selected 
     */
    getActiveConfirm(selected) {
        if (selected) {
            return '#ffa726'
        }
        return 'grey'
    }

    /**
     * Returns the default selected Truck for the dropdown list.
     *
     * @return {Array[String,Int]}
     */
    getCurrentTruck() {
        try {
            //this.render();

            if (DBHelper.getAll('Driver').length > 0) {
                
                let DB_truck_id = DBHelper.getAll('Driver')[0].truck_id
                let index = null;
                if (DB_truck_id != null) {
                    let DB_truck = DBHelper.getAll('Truck')
                    for (let i = 0; i < DB_truck.length; i++) {
                        if (DB_truck[i].id == DB_truck_id) {
                            index = i
                        }
                    }

                    

                    this.state.truck = DBHelper.getAll('Driver')[0].truck_id
                    return [index, '' + DBHelper.getAll('Driver')[0].truck_id]
                }
            } else {
                this.state.empty_error = true;
                //this.props.navigation.navigate('Login')
                return [null,'Select...']
            }


        } catch (error) {
            console.warn("Failed to getCurrentTruck \n" + error);

        }
    }

    /**
     * Gets all of the available trucks form the database
     *
     * @return {Array} Truck id's
     */
    getTrucks() {
        let DB_truck = DBHelper.getAll('Truck')
        let trucks = []
        for (let i = 0; i < DB_truck.length; i++) {
            trucks.push(DB_truck[i].id)
        }
        return trucks
    }

    /**
     * Called when a truck is selected from the drop down list
     *
     * Changes the state of the truck selected
     *
     * @param {Int} index 
     * @param {String} value 
     */
    selectTruck(index, value) {
        //API CALL HERE TO CHECK AVAILABLITY
        //IF
            this.state.truckID = value
            console.log("Selected Truck: " + index + " : " + this.state.truckID)

        /*
            if(DBHelper.getAll('Driver').filtered("truck_id =" + value).length > 0){
                let route_id = DBHelper.getAll('Driver').filtered("truck_id =" + value)[0].route_id;
                console.log("Selected Truck route/trip : " + route_id)
                this.selectActiveTrip(parseFloat(route_id) - 1);

            }
*/
        // ELSE 
            // ALERT!
            // GET NEW LIST (MAYBE)
            // setState

        //return false //stops the selection
    }

    /**
     * Validates that both a truck and trip have been selected.
     *
     * @return {Boolean} Prevents from confirming
     */
    checkSelection() {
        if (this.state.trip_selected == null) {
            return false
        } else if (this.state.truck == null) {
            return false
        }
        return true
    }

    /**
     * Updates Driver and Trip with the new selected Truck and Trip
     */
    confirmTrip() {
        if (this.getTripSelected()) {
            if (this.checkSelection()) {
                let DB_driver = DBHelper.getAll('Driver')[0]
                //DB_driver['truck_id'] = this.state.truck;
                let driver_data = []
                for (key in DB_driver) {
                    if (key == 'truck_id' && this.state.truckID != null) {
                        driver_data.push(parseFloat(this.state.truckID))
                        console.log("Selected Truck truck_id : " + this.state.truckID)
                    }  else {
                        driver_data.push(DB_driver[key])
                    }
                }

                DBHelper.update('Driver', 0, driver_data)
                //DBHelper.updateTruckById(0,this.state.truck);
                //this.state.trip_selected //trip_id to FMS
                //API call to update trip_id
                try {
                    let old_trip = DBHelper.getAll('Trip').filtered('active = 1')[0].id
                    DBHelper.updateTripActiveById(old_trip, 0);
                } catch (error) {
                    console.log('There is no old trip')
                }
                let new_trip = DBHelper.getById('Trip', this.state.trip_selected).id;
                DBHelper.updateTripActiveById(new_trip, 1);

                //Show Before Note
                //this.officeNotes(new_trip)


                //go to Note Confirmation
                let Note_lists = DBHelper.getAll('WorkOrder').filtered('trip_id = ' + new_trip)
                if (Note_lists.length > 0) {
                    this.props.navigation.navigate('NoteMain', {type: new_trip});
                    //this.props.navigation.navigate('Truck');
                } else {
                    this.props.navigation.navigate('Checklist', {type: 'truck'});
                }


                //go to CheckLists
                //let checklist = DBHelper.getAll('Checklist').filtered('type = "truck"')[0]
                //if (checklist != null) {
                //    this.props.navigation.navigate('Checklist', {type: 'truck'})
                //} else {
                //    this.props.navigation.navigate('Truck');
                //}
            }
            console.log('Failed to check')
        }
    }

    officeNotes(trip) {
        var notes = ''

        let workorders = DBHelper.getAll('WorkOrder')
//UNCOMMENT WHEN DUMMY API IS UPDATED!
        // for (let i = 0; i < workorders.length; i++) {
        //     if (workorders[i].trip_id == trip) {
        //         notes = notes + (workorders[0].before_note + "\n")
        //     }
        // }
//REMOVE WHEN DUMMY API IS UPDATED!
        notes = 'This is a notifcation which will pop up when ' 
            + 'the driver is about to leave the office or when he picks'
            + 'his truck and trip.'

        Alert.alert(
            'Before Leaving Note',
            notes,
            [
                { text: 'Ok', onPress: () => console.log() }
            ],
            { cancelable: false }
        )
    }

    render() {
        console.log("Info of Driver/WorkOrder:" + DBHelper.getAll('Driver').length + "/" + DBHelper.getAll('WorkOrder').length)
        if (DBHelper.getAll('Driver').length  == 0 || DBHelper.getAll('WorkOrder').length == 0){ // hack stupid databases
            console.log('Failed to empty_error')
            this.props.navigation.navigate('Truck')

        }
        return (
            <View style={styles.container}>
                <ScrollView style={styles.scrollView}>
                    <View style={styles.content}>
                        <View style={styles.rowLayout}>
                            <View style={{justifyContent: 'center', paddingRight: 10}}>
                                <Text style={styles.label}>Truck Number:</Text>
                            </View>
                            <View>
                                <View style={styles.dropdownContainer}>
                                    {/* Truck Drop Down */}
                                    <ModalDropdown
                                        style={[styles.dropdown, { width: 120 }]}
                                        textStyle={[styles.dropdownText, { textAlign: 'center' }]}
                                        dropdownStyle={[styles.dropdownBox, { width: 120, alignItems: 'center' }]}
                                        dropdownTextStyle={styles.dropdownOption}
                                        dropdownTextHighlightStyle={styles.dropdownSelectedOption}
                                        options={this.getTrucks()}
                                        onSelect={(index, value) => this.selectTruck(index,value)}
                                        defaultIndex={this.getCurrentTruck()[0]}
                                        defaultValue={this.getCurrentTruck()[1]}
                                    />
                                </View>    
                            </View>
                        </View>
                        <View>
                            <View style={styles.rowLayout}>
                                <View style={styles.section}>
                                    <Text style={styles.label}>Delivery Trip</Text>
                                </View>
                                <View style={styles.section}>
                                    <Text style={styles.label}>Work Orders #</Text>
                                </View>
                                <View style={styles.section}>
                                    <Text style={styles.label}>Status</Text>
                                </View>
                            </View>
                            {/* List of all of the trips */}
                            {this.getTrips()}
                        </View>
                        {/* Confirm Button */}
                        <TouchableOpacity style={[styles.confirmButton, { backgroundColor: this.getActiveConfirm(this.getTripSelected())}]} onPress={() => this.confirmTrip()} >
                            <Text style={styles.confirmText}>Confirm</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                {/*Child component header*/}
                <Header nav={() => this.props.navigation.navigate('Truck')}/>
                {/*Child component footer*/}
                <Footer nav={this.props.navigation} truck={1} map={1} workorder={1} settings={1} />
            </View>
        );
    }
}

const listContainerHeight = 40;

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollView: {
        position: 'absolute',
        height: window.height - (45 + 70),
        width: window.width - 60,
        bottom: 45,
    },
    content: {
        alignItems: 'center',
    },
    rowLayout: {
        flexDirection: 'row',
    },
    innerContent: {
        padding: 15,
        width: window.width - 40,
    },
    dropdownContainer: {
        backgroundColor: '#e6eef2',
        borderRadius: 5,
        height: 40,
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 10,
        paddingRight: 5,
        paddingLeft: 5,
    },
    dropdown: {
        //Could add styles later
    },
    dropdownBox: {
        //height: 125,
        borderRadius: 5,
    },
    dropdownText: {
        fontSize: 25,
        color: '#888888',
    },
    dropdownOption: {
        fontSize: 18,
        width: 110,
        textAlign: 'center',
    },
    dropdownSelectedOption: {
        color: 'black',
        fontWeight: '700',
        fontSize: 18,
    },
    confirmButton: {
        //backgroundColor: '#ffa726',
        borderRadius: 5,
        height: 60,
        width: window.width - 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
    confirmText: {
        color: 'white',
        fontSize: 18,
        fontWeight: '700',
    },
    label: {
        fontWeight: '700',
        color: '#607d8b',
        fontSize: 18,
    },
    section: {
        width: (window.width - 65)/3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    selectedTrip: {
        height: 50,
        borderRadius: 5,
        backgroundColor: '#78909c',
        marginTop: 5,
    },
    trip: {
        height: 50, 
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 5,
    },
    lastTrip: {
        marginBottom: 20,
    },
    selectedText: {
        fontSize: 16,
        fontWeight: '700',
        color: 'white',
    },
    text: {
        fontSize: 16,
        color: '#607d8b',
    },
});
