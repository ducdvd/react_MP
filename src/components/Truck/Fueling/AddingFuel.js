import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, TextInput, Button, Dimensions, ScrollView, TouchableOpacity, Switch, KeyboardAvoidingView, Alert } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import ModalDropdown from 'react-native-modal-dropdown';
import Prompt from 'react-native-prompt';

import DBHelper from '../../../DBHelper';

import Header from './Header';
import Footer from '../../Footer/Footer';

import Icon2 from 'react-native-vector-icons/Octicons';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import ImagePicker from 'react-native-image-picker';

/**
 * Parent Component to: Truck.js
 *
 * Displays a form that is needed to be completed to add fuel to
 * a compartment
 */
export default class AddingFuel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isDatePickerVisible: false, //Date Picker Display
            isTimePickerVIsible: false, //Time Picker Display
            promptVisible: false, //Entering the amount of fuel
            amount: null, //Amount that will be added to the compartment
            date: null, //Current Date or selected Date
            time: null, //Current Time or selected Time
            fuel: null, //Type of fuel within the take
            refinery: null, //Refinery of where he got the fuel
            location: null, //Location of the refinery
            location_array: ["Please select a Refinery"], //used for location selection
            compartment: DBHelper.getById('Compartment', props.navigation.state.params.cp_id), //Compartment that needs fuelign
            images: [], //Documents that are attached
        }
        this.getDateTime();
    };

    /**
     * Gets the current Date and Time.
     *
     * NOTE: This could possible return a different time if the
     * driver changes the internal clock on the device.
     */
    getDateTime() {
        let datetime = new Date(); //WARNING: Date() needs formating/configuration to show propper amounts
        let date = this.getMonth(datetime.getMonth()) + ' ' + datetime.getDate() + ', ' + datetime.getFullYear();
        let time = this.getActualTime(datetime.getHours()) + ':' + this.getMinutes(datetime.getMinutes()) + ' ' + this.getTimeOfDay(datetime.getHours());
        this.state.date = date;
        this.state.time = time;
    }

    /**
     * Correctly the display of time due to having no zero (0)
     * present if the current minutes less than zero (0)
     *
     * @param {Int} minutes - Within the Time
     * @return {String} Corrected minute display
     */
    getMinutes(minutes) {
        if (minutes < 10) {
            return '0' + minutes
        } else {
            return minutes
        }    
    }

    /**
     * Converts the numerical value of month to string
     * @param {*String value of month} month 
     */   
    getMonth(month) {
        switch (month) {
            case 0:
                return "January";
                
            case 1:
                return "February";
                
            case 2:
                return "March";
                
            case 3:
                return "April";
                
            case 4:
                return "May";
                
            case 5:
                 return "June";
                
            case 6:
                return "July";
                
            case 7:
                return "Augest";
                
            case 8:
                return "September";
                
            case 9:
                return "October";
                
            case 10:
                return "November";
                
            case 11:
                return "December";

        }
    }

    /**
     * Returns am or pm based on a 24hour clock
     * @param {*Range from 0-24 hours in a day} hour 
     */    
    getTimeOfDay(hour) {
        if (hour < 12) {
            return 'AM';
        } else {
            return 'PM';
        }
    }

    /**
     * Converts and Returns a 24 hour clock to normal
     * @param {*Range from 0-24 hours in a day} hour 
     */    
    getActualTime(hour) {
        if (hour > 12) {
            return hour - 12;
        } else if (hour == 0) {
            return 12;
        } else {
            return hour;
        }
    }

    /**
     * Display date picker
     */
    _showDatePicker = () => this.setState({ isDatePickerVisible: true });
    _hideDatePicker = () => this.setState({ isDatePickerVisible: false });
    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this.setState({ date: this.getMonth(date.getMonth()) + ' ' + date.getDate() + ', ' + date.getFullYear() });
        this._hideDatePicker();
    };

    /**
     * Displays time picker
     */
    _showTimePicker = () => this.setState({ isTimePickerVisible: true });
    _hideTimePicker = () => this.setState({ isTimePickerVisible: false });
    _handleTimePicked = (time) => {
        console.log('A time has been picked: ', time);
        this.setState({ time: this.getActualTime(time.getHours()) + ':' + this.getMinutes(time.getMinutes()) + ' ' + this.getTimeOfDay(time.getUTCHours()) });
        this._hideTimePicker();
    };

    /**
     * Gets a list of Refineries from the database
     *
     * @return {Array} Refinery's Name
     */
    getRefinerys() {
        let DB_refinery = DBHelper.getAll('Refinery')
        let refinerys = [];

        for (let i = 0; i < DB_refinery.length; i++) {
            refinerys.push(DB_refinery[i].name);
        }

        return refinerys;
    };
    
    /**
     * Called when a refinery is selected from the dropdown list.
     * Updates the current selected refinery state to the dropdown's
     *
     * @param {Int} index 
     * @param {String} value 
     */
    selectRefinery(index, value) {
        let DB_refinery = DBHelper.get('Refinery', index)
        console.log('Refinery selected:' + index + ' ' + value);

        console.log("id: " + DB_refinery.id)        
        let DB_refinery_locations = DBHelper.getAll('Refinery_Locations').filtered('refinery_id = ' + DB_refinery.id)
        var locations = [];

        for (let i = 0; i < DB_refinery_locations.length; i++) {
            locations.push(DB_refinery_locations[i].address)
        }

        this._dropdown_locations.select(-1)

        this.setState({
            refinery: DB_refinery,
            location: null,
            location_array: locations
        })
    };

    /**
     * Called when a refinery location is selected from the dropdown list.
     * Updates the current selected refinery location state to the dropdown's
     *
     * @param {Int} index 
     * @param {String} value 
     */
    selectLocation(index, value) {
        if (value != "Please select a Refinery") {
            let DB_location = DBHelper.getAll('Refinery_Locations').filtered('address = "' + value + '"')
            this.setState({
                location: DB_location[0],
            })
        }
        console.log('Refinery Location selected:' + index + ' ' + value);
    };

    /**
     * @return Maxmimum amount of fuel that can go into the compartment
     */
    getMaximumAmount() {
        return this.state.compartment.allowed_capacity - this.state.compartment.current_amount
    }

    /**
     * Determines whether the amount of fuel going into the tank is over its maximum
     * 
     * @param {Double} value - Amount of fuel being added to the compartment
     */
    submitAmount(value) {
        if (this.getMaximumAmount() >= value) {
            this.setState({
                promptVisible: false,
                amount: value
            })
        } else {
            Alert.alert('Incorrect Amount',
                'Amount Exceeds Maximum',
                [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
                { cancelable: true}
            )
        }
    }

    /**
     * Gets the number of compartments within the truck and stores them inside of a Array.
     * This array is used by the dropdown list for compartments.
     *
     * @return {Array[Array[Int],Int,String]} Default number of compartments, Initial Index, and Initial Value
     */
    getCompartments() {
        compartments = []

        for (var i = 1; i <= DBHelper.getAll('Compartment').filtered('truck_id =' + DBHelper.get('Driver', 0).truck_id ).length; i++) {
            compartments.push(i);
        }

        return [compartments, this.props.navigation.state.params.index, '' + (this.props.navigation.state.params.index + 1)]
    };

    /**
     * Called when a compartment is selected from compartment dropdown.
     * Updatest the selected compartment within the state.
     *
     * @param {Int} index 
     * @param {String} value 
     */
    selectCompartment(index, value) {
        let compartment = DBHelper.getById('Compartment', parseInt(value)) //done
        this.setState({
             compartment: compartment
        })
    };

    /**
     * Gets the current fuel type of that compartment selected
     * 
     * @return {Array[Int,String]} Index, and Value as Fuel Type name
     */
    getCurrentFuelType() {
        DB_fuel_id = this.state.compartment.fuel_id
        if (DB_fuel_id != null) {
            if (this.state.fuel == null) {
                console.log('this is called?')
                this.state.fuel = DBHelper.getById('Fuel', DB_fuel_id)
            }
            return [(DB_fuel_id - 1), DBHelper.getById('Fuel', DB_fuel_id).name]
        } else {
            return [null, null]
        }
    }

    /**
     * Gets all available fuel types for the compartment
     *
     * @return {Array[]} Fuel Type Values (Names)
     */
    getFuelType() {
        let DB_fuel = DBHelper.getAll('Fuel')
        var fueltypes = [];
        for (let i = 0; i < DB_fuel.length; i++) {
            fueltypes.push(DB_fuel[i].name);
        }
        return fueltypes;
    }

    /**
     * Called when a fuel type is selected from the dropdown.
     * Updates the selected fuel type within the state
     * 
     * @param {Int} index 
     * @param {String} value 
     */
    selectFuelType(index, value) {
        let DB_fuel = DBHelper.get('Fuel', index)
        this.state.fuel = DB_fuel
        console.log('Selected: ' + DB_fuel.name + " and " + DB_fuel.id)
    };

    /**
     * Checks whether the form is filled out or not.
     *
     * @return {Boolean} All values are good or not
     */
    checkValues() {
        try {
            var missing = '';
            if (this.state.date == null) {
                throw ("Date should never be null");
            }
            if (this.state.time == null) {
                throw ("Time should never be null");
            }
            if (this.state.fuel == null) {
                missing = missing + '\n Fuel'
            }
            
            if (this.state.refinery == null) {
                missing = missing + '\n Refinery'
            }
            if (this.state.location == null) {
                missing = missing + '\n Location'
            }
            if (this.state.amount == null) {
                missing = missing + '\n Amount'
            }
            if (missing != '') {
                missing = 'Values missing: ' + missing
                Alert.alert('Incorrect',
                    missing,
                    [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
                    { cancelable: true}
                )
                return false
            }
            return true
        } catch (error) {
            this.props.navigation.navigate('ErrorHandler', {error: error, type: 'Library', class: AddingFuel.name})
        }
    }

    /**
     * Add/Updates the database with the form information.
     * Also checks whether the form is completed.
     */
    confirmAdd() {
        if (this.checkValues()) {
            var data = []
            data.push(this.state.compartment.id)
            data.push(this.state.compartment.sequence)
            data.push(this.state.compartment.truck_id)
            data.push(this.state.fuel.id)
            data.push(this.state.compartment.allowed_capacity)
            let amount = (parseInt(this.state.amount) + this.state.compartment.current_amount) 
            data.push(amount)
            console.log("amount: " + amount)
            DBHelper.updateById('Compartment', this.state.compartment.id, data);

            let table_length = DBHelper.getAll('Documents').length
            console.log('table length: ' + table_length)

            let data = this.state.images
            for (let i = 0; i < data.length; i++) {
                let doc = [table_length + i, 0, this.state.compartment.id, 'photo', data[i], '']
                DBHelper.add('Documents', doc)
            }
            this.props.navigation.navigate('Truck');
        }
    }

    /**
     * Select Photo Option was selected and displays the ImagePicker.
     */
    selectPhotoTapped() {
        //The quality of the photo going to be returned.
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                //let source = { uri: response.uri };
                let source = response.uri;

                // You can also display the image using data:
                //let source = { uri: 'data:image/jpeg;base64,' + response.data };

                var data = this.state.images
                data.push(source)

                //VERSION for using states
                console.log(source)
                this.setState({
                    image: data
                });
            }
        });
    }
    
    /**
     * Removing a attached document alert (Warnning)
     *
     * @param {Int} doc_id 
     */
    removeImageAlert(doc_id) {
        Alert.alert(
            'Deleting Document',
            'Are you sure you wish to delete this document',
            [
                { text: 'Yes', onPress: () => this.removeImage(doc_id) },
                { text: 'No', onPress: () => console.log()},
            ],
            { cancelable: false }
        )
    }

    /**
     * Removes the image from the list
     *
     * @param {Int} doc_id 
     */
    removeImage(doc_id) {
        //find the url
        this.forceUpdate()
    }

    /**
     * Gets all of the photo attached to the adding of fuel and creates a display for them.
     *
     * @return {Array} Images that have been attached 
     */
    getImage() {
        let photos = this.state.images
        if (photos.length > 0 && photos != null) {
            var images = [];
            images.push(
                <View style={styles.docTitleContainer} key={-1}>
                    <Text style={styles.docTitle}>Number of images: {photos.length}</Text>
                </View>
            )
            for (let i = 0; i < photos.length; i++) {
                console.log('data image')
                console.log(photos[i])
                //let uri = encodeURI(photos[i].data)
                images.push(
                    <View style={styles.imageContainer} key={i}>
                        {/* <TouchableOpacity style={styles.removeDocContainer} onPress={() => this.removeImageAlert(photos[i].id)}>
                            <Text style={styles.removeDocText}>Remove</Text>
                        </TouchableOpacity> */}
                        {/* PUT A BUTTON THAT CAN REMOVE THE IMAGES FROM THE DATABASE */}
                        <Image
                            style={styles.image}
                            source={{ uri: photos[i] }}
                        >
                        </Image>
                    </View>
                )
            }
            return images
        }
        console.log('no image found')
        return
    }

    render() {
        return (
            <View style={styles.container}>    
                {/*Date Picker*/}
                {/*Does not show by default*/}
                <DateTimePicker
                    isVisible={this.state.isDatePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDatePicker}
                    mode={'date'}
                />    
                {/*Time Picker*/}
                {/*Does not show by default*/}
                <DateTimePicker
                    isVisible={this.state.isTimePickerVisible}
                    onConfirm={this._handleTimePicked}
                    onCancel={this._hideTimePicker}
                    mode={'time'}
                    titleIOS={'Pick a time'}
                />
                <ScrollView style={styles.scrollView}>    
                    <View style={styles.content}>
                        <View style={styles.innerContent}>  
                            <Text style={styles.subTitle}>Truck 56</Text>
                            
                            <View style={[styles.rowLayout, { marginTop: 20, marginBottom: 10 }]}>
                                {/*Date Picker Button*/}
                                <TouchableOpacity 
                                    style={[styles.datetimeContainer, {marginRight: 7.5}]} 
                                    onPress={this._showDatePicker}
                                >
                                    <View style={styles.datetimeLeftInnerContainer}>
                                        <Text style={styles.datetime}>{this.state.date}</Text>
                                    </View>
                                    <View style={styles.datetimeRightInnerContainer}>
                                        <Icon name={'note'} size={20} style={styles.icon}></Icon>
                                    </View>
                                </TouchableOpacity>
                                {/*Time Picker Button*/}
                                <TouchableOpacity 
                                    style={[styles.datetimeContainer, {marginLeft: 7.5}]} 
                                    onPress={this._showTimePicker}
                                >    
                                    <View style={styles.datetimeLeftInnerContainer}>
                                        <Text style={styles.datetime}>{this.state.time}</Text>
                                    </View>
                                    <View style={styles.datetimeRightInnerContainer}>
                                        <Icon name={'note'} size={20} style={styles.icon}></Icon>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            {/*Vender Dropdown list*/}
                            <Text style={styles.label}>Refinery:</Text>
                            <View style={styles.dropdownContainer}>
                                <ModalDropdown
                                    style={styles.dropdown}
                                    textStyle={styles.dropdownText}
                                    dropdownStyle={styles.dropdownBox}
                                    dropdownTextStyle={styles.dropdownOption}
                                    dropdownTextHighlightStyle={styles.dropdownSelectedOption}
                                    options={this.getRefinerys()}
                                    onSelect={(index, value) => this.selectRefinery(index, value)}
                                />
                            </View>
                            {/*Location Dropdown list*/}
                            <Text style={styles.label}>Location:</Text>
                            <View style={styles.dropdownContainer}>
                                <ModalDropdown 
                                    ref={el => this._dropdown_locations = el}        
                                    style={styles.dropdown}
                                    textStyle={styles.dropdownText}
                                    dropdownStyle={styles.dropdownBox}
                                    dropdownTextStyle={styles.dropdownOption}
                                    dropdownTextHighlightStyle={styles.dropdownSelectedOption}
                                    options={this.state.location_array}
                                    onSelect={(index, value) => this.selectLocation(index, value)}
                                />
                            </View>    
                            {/*Compartment Dropdown list*/}
                            <View style={styles.rowLayout}>
                                <View style={{justifyContent: 'center', paddingRight: 10}}>
                                    <Text style={styles.label}>Compartment:</Text>
                                </View>
                                <View style={styles.dropdownContainer}>    
                                    <ModalDropdown
                                        style={[styles.dropdown, { width: 30 }]}
                                        textStyle={[styles.dropdownText, { textAlign: 'center' }]}
                                        dropdownStyle={[styles.dropdownBox, { width: 30, alignItems: 'center' }]}
                                        dropdownTextStyle={styles.dropdownOption}
                                        dropdownTextHighlightStyle={styles.dropdownSelectedOption}
                                        options={this.getCompartments()[0]}
                                        defaultIndex={this.getCompartments()[1]}
                                        defaultValue={this.getCompartments()[2]}
                                        onSelect={(index, value) => this.selectCompartment(index, value)}
                                    />
                                </View>
                            </View>
                            
                            <View style={styles.rowLayout}>    
                                {/*Type of Fuel dropdown list*/}
                                <View style={{flex: 0.5, paddingRight: 5}}>
                                    <Text style={styles.label}>Fuel Type:</Text>
                                    <View style={styles.dropdownContainer}>
                                        <ModalDropdown
                                            style={styles.dropdown}
                                            textStyle={styles.dropdownText}
                                            dropdownStyle={styles.dropdownBox}
                                            dropdownTextStyle={styles.dropdownOption}
                                            dropdownTextHighlightStyle={styles.dropdownSelectedOption}
                                            options={this.getFuelType()}
                                            defaultIndex={this.getCurrentFuelType()[0]}
                                            defaultValue={this.getCurrentFuelType()[1]}
                                            onSelect={(index, value) => this.selectFuelType(index, value)}
                                        />
                                    </View>
                                </View>
                                {/*Amount of fuel field*/}
                                <View style={{flex: 0.5, paddingLeft: 5}}>
                                    <Text style={styles.label}>Amount:</Text>
                                    <View style={styles.dropdownContainer}>
                                        <Text
                                            onPress={() => this.setState({ promptVisible: true })}
                                            style={[styles.input]}

                                            /*
                                            onChangeText={(text) => this.setState({ amount: text })}
                                            placeholder="Amount Here"
                                            onSubmitEditing={() => this.submitAmount()}
                                            keyboardType="numeric"
                                            editable={true}*/
                                        >
                                            {this.state.amount}    
                                        </Text>
                                    </View>
                                    <Text style={styles.maximum}>Max {this.getMaximumAmount()}</Text>
                                </View>
                            </View>
                            {/*Attatch Documents*/}
                            <View style={[styles.rowLayout, {marginTop: 5, marginBottom: 5,}]}>    
                                <View style={[styles.docContainers, { paddingRight: 20 }]}>
                                    <Text style={styles.label}>Documents:</Text>
                                </View>
                                <View style={styles.docContainers}>
                                    <TouchableOpacity onPress={() => this.selectPhotoTapped()}>
                                        {/* <Text style={styles.addDoc}>+</Text> */}
                                        <Icon2  style={styles.plusIcon} name={'plus'} size={20}></Icon2>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.getImage()}
                        </View>
                        {/*Confirmation Button*/}
                        <TouchableOpacity style={styles.confirmButton} onPress={() => this.confirmAdd()} >
                            <Text style={styles.confirmText}>Confirm</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                {/*Child component header*/}
                <Header nav={() => this.props.navigation.navigate('Truck')} />
                {/*Child component footer*/}
                <Footer nav={this.props.navigation} truck={0} map={1} workorder={1} settings={1} />
                <Prompt
                    title="Amount of Fuel"
                    placeholder=""
                    defaultValue={this.state.amount}
                    visible={ this.state.promptVisible }
                    onCancel={ () => this.setState({
                        promptVisible: false,
                    }) }
                    onSubmit={
                        (value) => this.submitAmount(value)
                    }
                    textInputProps={{keyboardType: 'number-pad'}}
                />
            </View>
        );
    }
}

const listContainerHeight = 40;

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EBEBEB',
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollView: {
        position: 'absolute',
        backgroundColor: '#EBEBEB',
        height: window.height - (45 + 70),
        width: window.width - 40,
        bottom: 45,
    },
    content: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        width: window.width - 40,
    },
    rowLayout: {
        flexDirection: 'row',
    },
    innerContent: {
        padding: 15,
        width: window.width - 40,
    },
    subTitle: {
        color: '#888888',
        fontSize: 15,
        fontWeight: '700',
    },
    label: {
        color: '#607d8b',
        fontSize: 15,
    },
    datetime: {
        color: '#78909c',
    },
    datetimeContainer: {
        flex: 0.5,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#e6eef2',
        flexDirection: 'row',
        height: listContainerHeight,
    },
    datetimeLeftInnerContainer: {
        flex: 0.7,
        justifyContent: 'center',
        alignItems: 'center'
    },
    datetimeRightInnerContainer: {
        flex: 0.3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    confirmButton: {
        backgroundColor: '#ffa726',
        height: 100,
        width: window.width - 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 40,
        borderColor: '#EBEBEB'
    },
    confirmText: {
        color: 'white',
        fontSize: 18,
        fontWeight: '700',
    },
    dropdownContainer: {
        backgroundColor: '#e6eef2',
        borderRadius: 5,
        height: 40,
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 10,
        paddingRight: 5,
        paddingLeft: 5,
    },
    dropdown: {
        //Could add styles later
    },
    dropdownBox: {
        //height: 114,
        borderRadius: 5,
    },
    dropdownText: {
        paddingTop: 8,
        paddingBottom: 8,
        fontSize: 12,
        color: '#888888',
    },
    dropdownOption: {
        fontSize: 12,
    },
    dropdownSelectedOption: {
        color: 'black',
        fontWeight: '700',
        fontSize: 12,
    },
    maximum: {
        color: 'red',
        fontSize: 10,
    },
    input: {
        height: 20,
        fontSize: 12,
    },
    docContainers: {
        justifyContent: 'center',
        //backgroundColor: 'blue',
        height: 30,
    },
    addDoc: {
        fontSize: 40,
        fontWeight: '600',
        color: '#607d8b'
    },
    icon: {
        color: 'grey'
    },
    docTitleContainer: {
        alignItems: 'center',
    },
    docTitle: {
        color: '#607d8b',
        fontSize: 12,
        fontWeight: '700',
    },
    imageContainer: {
        alignItems: 'center',
        margin: 5,
    },
    image: {
        width: window.width - 100,
        height: 200,
    },
    removeDocContainer: {
        backgroundColor: '#EF4836',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        width: 80,
        height: 20,
    },
    removeDocText: {
        fontSize: 14,
        color: 'white'
    },
    plusIcon: {
        fontSize: 25,
        color: 'grey',
    }
});
