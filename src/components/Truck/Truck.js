import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    TextInput,
    Button,
    Dimensions,
    ScrollView,
    TouchableOpacity,
    Switch,
    Alert
} from "react-native";

import Header from "./Header";
import DBHelper from "../../DBHelper";
import Footer from "../Footer/Footer";

import Icon2 from "react-native-vector-icons/Octicons";
import Icon from "react-native-vector-icons/SimpleLineIcons";
import ImagePicker from "react-native-image-picker";

/**
 * Parent Component
 *
 * Dynamic view that allows a range from 1 to 5
 * compartments to be viewed on the screen.
 */
export default class Truck extends Component {
    constructor(props) {
        super(props);
        if(DBHelper.getAll("Trip").filtered("active =1").length == 0){
            this.props.navigation.navigate('Change');




        }else{

            let tripInfo;
            let Clients;
            tripInfo = DBHelper.getAll("Trip").filtered("active =1")[0];
            let WorkOrder = DBHelper.getAll("WorkOrder").filtered(
                "trip_id =" + tripInfo.id + ' AND type="ReFuel"'
            )[0];


            if(WorkOrder.vendor_id == ''){
                Clients = DBHelper.getById("Locations", WorkOrder.location_id);
            }else{

                Clients = DBHelper.getById("Vendor", WorkOrder.vendor_id);
            }




            this.state = {
                clientsName: Clients.name,
                clientsAddress: Clients.address,
                addFuelFlag: false,
                trip_id:tripInfo.id,
                arrarCompart: [],
                WorkOrderID: WorkOrder.id,
                VendorID: WorkOrder.vendor_id,
                images: [] //Documents that are attached
            };
            this.getCompartments();



        }


    }

    /**
        * Nagivates to the Truck Selection (Change) View
        */
    changeTruck() {
        console.log("Changing Truck");
        this.props.navigation.navigate("Change");
    }

    /**
    * Add Fuel Component
    *
    */
    addFuel() {

        var arr_tmp = this.state.arrarCompart;

        let tmp_id = 0
        let tmp_current= 0
        let count_id = 0
        for (i = 0; i < arr_tmp.length; i++) {
            let compartment = DBHelper.getById("Compartment", arr_tmp[i].id);
            let max_fuel = compartment.allowed_capacity;
            if(tmp_id != arr_tmp[i].id){
                tmp_current= compartment.current_amount
            }

            if (parseInt(arr_tmp[i].amount) <= parseInt(max_fuel)) {
                console.log("------addFuel-------")
                DBHelper.updateElementAndById(
                    "Compartment",
                    arr_tmp[i].id,
                    "current_amount",
                    parseInt(arr_tmp[i].amount)
                );

                if(tmp_id != arr_tmp[i].id){
                    let compartment2 = DBHelper.getById("Compartment", arr_tmp[i].id);
                    let fuel_add = parseInt(arr_tmp[i].amount) - tmp_current


                    console.log("------addFuel-------" + arr_tmp[i].amount + '/' + tmp_current)
                    let driver_info = DBHelper.getById('Driver',1)
                    count_id = DBHelper.getAll('Inventory').length + 1
                    let Inventory = [
                        parseFloat(count_id),
                        this.getTimeStamp(),
                        parseFloat(driver_info.id),
                        parseFloat(this.state.trip_id),
                        parseFloat(this.state.VendorID),
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        parseFloat(driver_info.truck_id),
                        parseFloat(arr_tmp[i].id),
                        0,
                        parseFloat(compartment.fuel_id),
                        'load',
                        parseInt(fuel_add) //
                    ]


                    console.log("------Inventory-------")
                    console.log(this.state.arrarCompart)
                    console.log(Inventory)
                    console.log("------end Inventory-------")
                    DBHelper.add('Inventory', Inventory)
                    tmp_id = arr_tmp[i].id
                }else{

                    DBHelper.updateElementAndById(
                        "Inventory",
                        parseFloat(count_id),
                        "quantity",
                        parseInt(parseInt(arr_tmp[i].amount) - tmp_current)
                    );
                }



            } else {
                Alert.alert(
                    "" + this.getType(arr_tmp[i].id),
                    "ERROR: Amount over Allowed Capacity\n",
                    [{ text: "Ok", onPress: () => console.log() }],
                    { cancelable: false }
                );
            }
        }




        this.setState({ arrarCompart: [] });
        let data_img = this.state.images;
        if (data_img.length > 0) {
            this.setState({ images: [] });

            let table_length = DBHelper.getAll("Documents").length;
            console.log("table length:>ducdvd>>>>>> " + table_length);

            let data_img = this.state.images;
            for (let i = 0; i < data_img.length; i++) {
                let doc = [
                    table_length + i,
                    this.state.WorkOrderID,
                    1,
                    "photo",
                    data_img[i],
                    "",this.state.trip_id
                ];
                DBHelper.add("Documents", doc);
            }
        }


        this.setState({ addFuelFlag: false });


    }
    /**
     * Get current time stamp
     *
     */
    getTimeStamp() {
        var now = new Date();
        return ((now.getMonth() + 1) + '/' +
            (now.getDate()) + '/' +
            now.getFullYear() + " " +
            now.getHours() + ':' +
            ((now.getMinutes() < 10)
                ? ("0" + now.getMinutes())
                : (now.getMinutes())) + ':' +
            ((now.getSeconds() < 10)
                ? ("0" + now.getSeconds())
                : (now.getSeconds())));
    }
    /**
       * Clear the form
       *
       */
    reSubmitFuel() {
        this.props.navigation.navigate('Truck')
        this.showVendorInfo();
        this.setState({ addFuelFlag: true })
    }
    /**
        * set compartment state
        *
        * @param {Int} compartment_id - Compartment Id
        * @param {Int} i - Index of the compartment (for dropdown lists)
        */
    setCompartState(compartment_id, Fuelvalue, CurrAmount) {

        if (parseInt(Fuelvalue) > 0) {
            Fuelvalue = parseInt(parseInt(Fuelvalue) + parseInt(CurrAmount));

            let compartment = DBHelper.getById("Compartment", compartment_id);
            let max_fuel = compartment.allowed_capacity;
            if (parseInt(Fuelvalue) <= parseInt(max_fuel)) {

                this.state.arrarCompart.push({ id: compartment_id, amount: Fuelvalue });
            } else {
                Alert.alert(
                    "" + this.getType(compartment_id),
                    "ERROR: Amount over Allowed Capacity\n",
                    [{ text: "Ok", onPress: () => this.reSubmitFuel() }],
                    { cancelable: false }
                );
            }
        } 
        if (isNaN(Fuelvalue)) {
            Alert.alert(
                "" + this.getType(compartment_id),
                "ERROR: Wrong Input\n",
                [{ text: "Ok"}],
                { cancelable: false }
            );
        }




    }
    /**
        * @return {String} Current driver's truck
        */
    getTruckNumber() {
        let truck_id = DBHelper.get("Driver", 0).truck_id;
        return "Truck " + truck_id;
    }
    /**
        * Gets the current amount of fuel within the compartment
        *
        * @param {Int} compartment_id - Compartment Id
        * @return {String} amount of fuel compared to its maximum
        */
    getCurrentPercent(compartment_id) {
        let compartment = DBHelper.getById("Compartment", compartment_id);

        if (compartment.allowed_capacity != 0 || compartment.current_amount != 0) {
            let percentage =
                compartment.current_amount / compartment.allowed_capacity * 100;
            return Math.round(percentage) + "%";
            //return compartment.current_amount + '\n/\n' + compartment.allowed_capacity
        } else {
            return "0%";
            //return '0/' + compartment.allowed_capacity
        }
        //console.log(compartment)
    }
    /**
        * Gets the current amount of fuel within the compartment
        *
        * @param {Int} compartment_id - Compartment Id
        * @return {String} amount of fuel compared to its maximum
        */
    getAmount(compartment_id) {
        let compartment = DBHelper.getById("Compartment", compartment_id);

        if (compartment.allowed_capacity != 0 || compartment.current_amount != 0) {
            let percentage =
                compartment.current_amount / compartment.allowed_capacity * 100;
            //return Math.round(percentage) + "%";
            return (
                compartment.current_amount +
                "\n/\n" +
                compartment.allowed_capacity +
                "\n\n\n" +
                Math.round(percentage) +
                "%"
            );
        } else {
            //return "0%";
            return "0/" + compartment.allowed_capacity;
        }
        //console.log(compartment)
    }
    /**
      * Gets the current amount of fuel within the compartment
      *
      * @param {Int} compartment_id - Compartment Id
      * @return {String} amount of fuel compared to its maximum
      */
    getCurrentAmount(compartment_id) {
        let compartment = DBHelper.getById("Compartment", compartment_id);

        if (compartment.allowed_capacity != 0 || compartment.current_amount != 0) {
            //let percentage = (compartment.current_amount / compartment.allowed_capacity) * 100
            //return Math.round(percentage) + '%'
            return compartment.current_amount;
        } else {
            //return '0%'
            return "0/";
        }
        //console.log(compartment)
    }
    /**
      * Gets the Fuel Capacity of fuel within the compartment
      *
      * @param {Int} compartment_id - Compartment Id
      * @return {String} amount of fuel compared to its maximum
      */
    getFuelCapacity(compartment_id) {
        let compartment = DBHelper.getById("Compartment", compartment_id);

        if (compartment.allowed_capacity > 0) {
            //let percentage = (compartment.current_amount / compartment.allowed_capacity) * 100
            //return Math.round(percentage) + '%'
            return compartment.allowed_capacity;
        } else {
            //return '0%'
            return 0;
        }
        //console.log(compartment)
    }
    /**
        * Gets the type of fuel within the compartment
        *
        * @param {Int} compartment_id - Compartment Id
        * @return {String} fuel type name
        */
    getType(compartment_id) {
        let compartment = DBHelper.getById("Compartment", compartment_id);
        let fuel = DBHelper.getById("Fuel", compartment.fuel_id);
        return fuel.name;
    }

    /**
        * Calculates the amount of fuel (in percentage) the amount
        * of fuel that can go inside of the tank
        *
        * @param {Int} compartment_id - Compartment Id
        * @return {Float} percentage of left over fuel
        */
    getLeftOver(compartment_id) {
        let percentage = this.getPercentage(compartment_id);
        let leftover = 1 - percentage;
        console.log("leftover: " + leftover);
        return parseFloat(leftover);
    }

    /**
        * Calculates the amount of fuel inside of the tank (in percentage)
        *
        * @param {Int} compartment_id - Compartment Id
        * @return {Float} percentage of fuel inside the tank
        */
    getPercentage(compartment_id) {
        let compartment = DBHelper.getById("Compartment", compartment_id);

        if (compartment.allowed_capacity != 0 || compartment.current_amount != 0) {
            let percentage =
                compartment.current_amount / compartment.allowed_capacity;
            //return Math.round(percentage) + '%'
            console.log("percentage: " + percentage);
            return parseFloat(percentage);
        } else {
            //return '0%'
            return 0;
        }
        //console.log(compartment)
    }

    /**
        * @return {String} Color of the type of fuel
        */
    getFuelColor(compartment_id) {
        let compartment = DBHelper.getById("Compartment", compartment_id);
        switch (compartment.fuel_id) {
            case 1: {
                return "#4183D7";
                break;
            }
            case 2: {
                return "aquamarine";
                break;
            }
            case 3: {
                return "crimson";
                break;
            }
            case 4: {
                return "darkorange";
                break;
            }
            case 5: {
                return "greenyellow";
                break;
            }
        }
    }
    /**
      * Gets the Fuel Capacity of fuel within the compartment
      *
      * @param {Int} capacity - Compartment Id
      * @return {String} amount of fuel compared to its maximum
      */
    getFuelEst(capacity) {
        return (capacity * (1 - 0.25)).toFixed(0);
    }
    /**
      * Gets the Fuel Recommendation of fuel within the compartment
      *
      * @param {Int} capacity - capacity fuel
      * @param {Int} current - current Id
      * @return {String} amount of fuel compared to its maximum
      */
    getFuelRec(capacity, current) {
        let rec = (capacity * (1 - 0.25) - current).toFixed(0);
        if (rec > 0) {
            return (capacity * (1 - 0.25) - current).toFixed(0);
        }
        return 0;
    }
    /**
        * Creates a custom dynamic display of the compartments within the truck.
        * It displays its current amount and type in a graphical way.
        *
        * NOTE: Why there is so many IF statements is because of handling:
        * - Only 2 compartments Left and Right
        * - Only 1 compartment
        * - Only 3 compartments Left, Right, and Custom Middle
        * - Greater than 3 compartments Left, Right, and Middle
        */
    getCompartments() {


        if (this.state.addFuelFlag) {


            let truck_id = DBHelper.get("Driver", 0).truck_id;
            let compartments = DBHelper.getAll("Compartment").filtered(
                "truck_id =" + truck_id
            );
            var cp_view = [];
            var truck_cp = 1;
            var truck_cpmax = DBHelper.getById("Truck", truck_id).compartments;
            for (let i = 0; i < compartments.length; i++) {

                // let stuff = DBHelper.get('Driver', 0);
                // throw (stuff.truck_id)
                if (truck_cpmax > 2) {
                    if (truck_cp == 1) {
                        //LEFT
                        cp_view.push(
                            <View
                                style={styles.compartmentContainer}
                                key={compartments[i].id}
                            >
                                <View style={{ position: "absolute", height: 120 }}>
                                    <View
                                        style={{
                                            flex: this.getLeftOver(compartments[i].id),
                                            width: window.width / 5 - 15,
                                            justifyContent: "center",
                                            alignContent: "center"
                                        }}
                                    />
                                    <View
                                        style={{
                                            flex: this.getPercentage(compartments[i].id),
                                            backgroundColor: this.getFuelColor(compartments[i].id),
                                            width: window.width / 5 - 15,
                                            alignItems: "center"
                                        }}
                                    />
                                </View>
                                <View style={[styles.rightTank]}>
                                    {/* height: 120, width: window.width/compartments - 50, */}
                                    <Text style={styles.amountText}>
                                        {this.getAmount(compartments[i].id)}
                                    </Text>
                                </View>
                                <View style={styles.overlay}>
                                    <View style={styles.aboutContainer}>
                                        <Text style={styles.aboutText}>
                                            {this.getType(compartments[i].id)}
                                        </Text>
                                        <Text style={styles.aboutText} />
                                        <Text style={styles.estText}>
                                            {this.getFuelEst(compartments[i].allowed_capacity)}
                                        </Text>

                                        <View>
                                            <Text style={styles.recText}>
                                                {this.getFuelRec(
                                                    compartments[i].allowed_capacity,
                                                    compartments[i].current_amount
                                                )}
                                            </Text>

                                            <TextInput
                                                name={compartments[i].id}
                                                style={{
                                                    height: 40,
                                                    borderColor: "gray",
                                                    borderWidth: 1
                                                }}
                                                editable={true}
                                                placeholderTextColor="#BFBFBF"
                                                placeholder="#"

                                                onChangeText={text =>
                                                    this.setCompartState(
                                                        compartments[i].id,
                                                        text,
                                                        this.getCurrentAmount(compartments[i].id)
                                                    )}
                                                keyboardType="numeric"
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.topOverLay}>
                                        <View style={styles.numberContainer}>
                                            <Text style={styles.numberText}>{truck_cp}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.bottomOverlay} />
                                </View>
                            </View>
                        );
                    } else if (truck_cp == truck_cpmax) {
                        //RIGHT
                        cp_view.push(
                            <View
                                style={styles.compartmentContainer}
                                key={compartments[i].id}
                            >
                                <View style={{ position: "absolute", height: 120 }}>
                                    <View
                                        style={{
                                            flex: this.getLeftOver(compartments[i].id),
                                            width: window.width / 5 - 15,
                                            justifyContent: "center",
                                            alignContent: "center"
                                        }}
                                    />
                                    <View
                                        style={{
                                            flex: this.getPercentage(compartments[i].id),
                                            backgroundColor: this.getFuelColor(compartments[i].id),
                                            width: window.width / 5 - 15,
                                            alignItems: "center"
                                        }}
                                    />
                                </View>
                                <View style={styles.leftTank}>
                                    <Text style={styles.amountText}>
                                        {this.getAmount(compartments[i].id)}
                                    </Text>
                                </View>
                                <View style={styles.overlay}>
                                    <View style={styles.aboutContainer}>
                                        <Text style={styles.aboutText}>
                                            {this.getType(compartments[i].id)}
                                        </Text>
                                        <Text style={styles.aboutText} />
                                        <Text style={styles.estText}>
                                            {this.getFuelEst(compartments[i].allowed_capacity)}
                                        </Text>
                                        <View>
                                            <Text style={styles.recText}>
                                                {this.getFuelRec(
                                                    compartments[i].allowed_capacity,
                                                    compartments[i].current_amount
                                                )}
                                            </Text>

                                            <TextInput
                                                name={compartments[i].id}
                                                style={{
                                                    height: 40,
                                                    borderColor: "gray",
                                                    borderWidth: 1
                                                }}
                                                editable={true}
                                                placeholderTextColor="#BFBFBF"
                                                placeholder="#"

                                                onChangeText={text =>
                                                    this.setCompartState(
                                                        compartments[i].id,
                                                        text,
                                                        this.getCurrentAmount(compartments[i].id)
                                                    )}
                                                keyboardType="numeric"
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.topOverLay}>
                                        <View style={styles.numberContainer}>
                                            <Text style={styles.numberText}>{truck_cp}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        );
                    } else {
                        //MIDDLE
                        if (truck_cpmax > 3) {
                            if (truck_cp == 2) {
                                //LEFT MIDDLE
                                cp_view.push(
                                    <View
                                        style={styles.compartmentContainer}
                                        key={compartments[i].id}
                                    >
                                        <View style={{ position: "absolute", height: 120 }}>
                                            <View
                                                style={{
                                                    flex: this.getLeftOver(compartments[i].id),
                                                    width: window.width / 5 - 15,
                                                    justifyContent: "center",
                                                    alignContent: "center"
                                                }}
                                            />
                                            <View
                                                style={{
                                                    flex: this.getPercentage(compartments[i].id),
                                                    backgroundColor: this.getFuelColor(
                                                        compartments[i].id
                                                    ),
                                                    width: window.width / 5 - 15,
                                                    alignItems: "center"
                                                }}
                                            />
                                        </View>
                                        <View style={styles.leftMiddleTank}>
                                            <Text style={styles.amountText}>
                                                {this.getAmount(compartments[i].id)}
                                            </Text>
                                        </View>
                                        <View style={styles.overlay}>
                                            <View style={styles.aboutContainer}>
                                                <Text style={styles.aboutText}>
                                                    {this.getType(compartments[i].id)}
                                                </Text>
                                                <Text style={styles.aboutText} />
                                                <Text style={styles.estText}>
                                                    {this.getFuelEst(compartments[i].allowed_capacity)}
                                                </Text>
                                                <View>
                                                    <Text style={styles.recText}>
                                                        {this.getFuelRec(
                                                            compartments[i].allowed_capacity,
                                                            compartments[i].current_amount
                                                        )}
                                                    </Text>

                                                    <TextInput
                                                        name={compartments[i].id}
                                                        style={{
                                                            height: 40,
                                                            borderColor: "gray",
                                                            borderWidth: 1
                                                        }}
                                                        editable={true}
                                                        placeholderTextColor="#BFBFBF"
                                                        placeholder="#"
                                                        onChangeText={text =>
                                                            this.setCompartState(
                                                                compartments[i].id,
                                                                text,
                                                                this.getCurrentAmount(compartments[i].id)
                                                            )}
                                                        keyboardType="numeric"
                                                    />
                                                </View>
                                            </View>
                                            <View style={styles.topOverLay}>
                                                <View style={styles.numberContainer}>
                                                    <Text style={styles.numberText}>{truck_cp}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                );
                            } else if (truck_cp == truck_cpmax - 1) {
                                //RIGHT MIDDLE
                                cp_view.push(
                                    <View
                                        style={styles.compartmentContainer}
                                        key={compartments[i].id}
                                    >
                                        <View style={{ position: "absolute", height: 120 }}>
                                            <View
                                                style={{
                                                    flex: this.getLeftOver(compartments[i].id),
                                                    width: window.width / 5 - 15,
                                                    justifyContent: "center",
                                                    alignContent: "center"
                                                }}
                                            />
                                            <View
                                                style={{
                                                    flex: this.getPercentage(compartments[i].id),
                                                    backgroundColor: this.getFuelColor(
                                                        compartments[i].id
                                                    ),
                                                    width: window.width / 5 - 15,
                                                    alignItems: "center"
                                                }}
                                            />
                                        </View>
                                        <View style={styles.rightMiddleTank}>
                                            <Text style={styles.amountText}>
                                                {this.getAmount(compartments[i].id)}
                                            </Text>
                                        </View>
                                        <View style={styles.overlay}>
                                            <View style={styles.aboutContainer}>
                                                <Text style={styles.aboutText}>
                                                    {this.getType(compartments[i].id)}
                                                </Text>
                                                <Text style={styles.aboutText} />
                                                <Text style={styles.estText}>
                                                    {this.getFuelEst(compartments[i].allowed_capacity)}
                                                </Text>
                                                <View>
                                                    <Text style={styles.recText}>
                                                        {this.getFuelRec(
                                                            compartments[i].allowed_capacity,
                                                            compartments[i].current_amount
                                                        )}
                                                    </Text>

                                                    <TextInput
                                                        name={compartments[i].id}
                                                        style={{
                                                            height: 40,
                                                            borderColor: "gray",
                                                            borderWidth: 1
                                                        }}
                                                        editable={true}
                                                        placeholderTextColor="#BFBFBF"
                                                        placeholder="#"
                                                        onChangeText={text =>
                                                            this.setCompartState(
                                                                compartments[i].id,
                                                                text,
                                                                this.getCurrentAmount(compartments[i].id)
                                                            )}
                                                        keyboardType="numeric"
                                                    />
                                                </View>
                                            </View>
                                            <View style={styles.topOverLay}>
                                                <View style={styles.numberContainer}>
                                                    <Text style={styles.numberText}>{truck_cp}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                );
                            } else {
                                cp_view.push(
                                    //MIDDLE
                                    <View
                                        style={styles.compartmentContainer}
                                        key={compartments[i].id}
                                    >
                                        <View style={{ position: "absolute", height: 120 }}>
                                            <View
                                                style={{
                                                    flex: this.getLeftOver(compartments[i].id),
                                                    width: window.width / 5 - 15,
                                                    justifyContent: "center",
                                                    alignContent: "center"
                                                }}
                                            />
                                            <View
                                                style={{
                                                    flex: this.getPercentage(compartments[i].id),
                                                    backgroundColor: this.getFuelColor(
                                                        compartments[i].id
                                                    ),
                                                    width: window.width / 5 - 15,
                                                    alignItems: "center"
                                                }}
                                            />
                                        </View>
                                        <View style={styles.middleTank}>
                                            <Text style={styles.amountText}>
                                                {this.getAmount(compartments[i].id)}
                                            </Text>
                                        </View>
                                        <View style={styles.overlay}>
                                            <View style={styles.aboutContainer}>
                                                <Text style={styles.aboutText}>
                                                    {this.getType(compartments[i].id)}
                                                </Text>
                                                <Text style={styles.aboutText} />
                                                <Text style={styles.estText}>
                                                    {this.getFuelEst(compartments[i].allowed_capacity)}
                                                </Text>
                                                <View>
                                                    <Text style={styles.recText}>
                                                        {this.getFuelRec(
                                                            compartments[i].allowed_capacity,
                                                            compartments[i].current_amount
                                                        )}
                                                    </Text>

                                                    <TextInput
                                                        name={compartments[i].id}
                                                        style={{
                                                            height: 40,
                                                            borderColor: "gray",
                                                            borderWidth: 1
                                                        }}
                                                        editable={true}
                                                        placeholderTextColor="#BFBFBF"
                                                        placeholder="#"
                                                        onChangeText={text =>
                                                            this.setCompartState(
                                                                compartments[i].id,
                                                                text,
                                                                this.getCurrentAmount(compartments[i].id)
                                                            )}
                                                        keyboardType="numeric"
                                                    />
                                                </View>
                                            </View>
                                            <View style={styles.topOverLay}>
                                                <View style={styles.numberContainer}>
                                                    <Text style={styles.numberText}>{truck_cp}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                );
                            }
                        } else {
                            //MIDDLE IF ONLY 3 COMPARTMENTS
                            cp_view.push(
                                <View
                                    style={styles.compartmentContainer}
                                    key={compartments[i].id}
                                >
                                    <View style={{ position: "absolute", height: 120 }}>
                                        <View
                                            style={{
                                                flex: this.getLeftOver(compartments[i].id),
                                                width: window.width / 5 - 15,
                                                justifyContent: "center",
                                                alignContent: "center"
                                            }}
                                        />
                                        <View
                                            style={{
                                                flex: this.getPercentage(compartments[i].id),
                                                backgroundColor: this.getFuelColor(compartments[i].id),
                                                width: window.width / 5 - 15,
                                                alignItems: "center"
                                            }}
                                        />
                                    </View>
                                    <View style={styles.middle3Tank}>
                                        <Text style={styles.amountText}>
                                            {this.getAmount(compartments[i].id)}
                                        </Text>
                                    </View>
                                    <View style={styles.overlay}>
                                        <View style={styles.aboutContainer}>
                                            <Text style={styles.aboutText}>
                                                {this.getType(compartments[i].id)}
                                            </Text>
                                            <Text style={styles.aboutText} />
                                            <Text style={styles.estText}>
                                                {this.getFuelEst(compartments[i].allowed_capacity)}
                                            </Text>
                                            <View>
                                                <Text style={styles.recText}>
                                                    {this.getFuelRec(
                                                        compartments[i].allowed_capacity,
                                                        compartments[i].current_amount
                                                    )}
                                                </Text>

                                                <TextInput
                                                    name={compartments[i].id}
                                                    style={{
                                                        height: 40,
                                                        borderColor: "gray",
                                                        borderWidth: 1
                                                    }}
                                                    editable={true}
                                                    placeholderTextColor="#BFBFBF"
                                                    placeholder="#"
                                                    onChangeText={text =>
                                                        this.setCompartState(
                                                            compartments[i].id,
                                                            text,
                                                            this.getCurrentAmount(compartments[i].id)
                                                        )}
                                                    keyboardType="numeric"
                                                />
                                            </View>
                                        </View>
                                        <View style={styles.topOverLay}>
                                            <View style={styles.numberContainer}>
                                                <Text style={styles.numberText}>{truck_cp}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            );
                        }
                    }
                } else {
                    if (truck_cpmax == 1) {
                        //SINGLE TANK
                        cp_view.push(
                            <View
                                style={styles.compartmentContainer}
                                key={compartments[i].id}
                            >
                                <View style={{ position: "absolute", height: 120 }}>
                                    <View
                                        style={{
                                            flex: this.getLeftOver(compartments[i].id),
                                            width: window.width / 5 - 15,
                                            justifyContent: "center",
                                            alignContent: "center"
                                        }}
                                    />
                                    <View
                                        style={{
                                            flex: this.getPercentage(compartments[i].id),
                                            backgroundColor: this.getFuelColor(compartments[i].id),
                                            width: window.width / 5 - 15,
                                            alignItems: "center"
                                        }}
                                    />
                                </View>
                                <View style={styles.singleTank}>
                                    <Text style={styles.amountText}>
                                        {this.getAmount(compartments[i].id)}
                                    </Text>
                                </View>
                                <View style={styles.overlay}>
                                    <View style={styles.aboutContainer}>
                                        <Text style={styles.aboutText}>
                                            {this.getType(compartments[i].id)}
                                        </Text>
                                        <Text style={styles.aboutText} />
                                        <Text style={styles.estText}>
                                            {this.getFuelEst(compartments[i].allowed_capacity)}
                                        </Text>
                                        <View>
                                            <Text style={styles.recText}>
                                                {this.getFuelRec(
                                                    compartments[i].allowed_capacity,
                                                    compartments[i].current_amount
                                                )}
                                            </Text>

                                            <TextInput
                                                name={compartments[i].id}
                                                style={{
                                                    height: 40,
                                                    borderColor: "gray",
                                                    borderWidth: 1
                                                }}
                                                editable={true}
                                                placeholderTextColor="#BFBFBF"
                                                placeholder="#"
                                                onChangeText={text =>
                                                    this.setCompartState(
                                                        compartments[i].id,
                                                        text,
                                                        this.getCurrentAmount(compartments[i].id)
                                                    )}
                                                keyboardType="numeric"
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.topOverLay}>
                                        <View style={styles.numberContainer}>
                                            <Text style={styles.numberText}>{truck_cp}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        );
                    } else {
                        if (truck_cp == 1) {
                            //LEFT 2 ONLY TANK
                            cp_view.push(
                                <View
                                    style={styles.compartmentContainer}
                                    key={compartments[i].id}
                                >
                                    <View style={{ position: "absolute", height: 120 }}>
                                        <View
                                            style={{
                                                flex: this.getLeftOver(compartments[i].id),
                                                width: window.width / 5 - 15,
                                                justifyContent: "center",
                                                alignContent: "center"
                                            }}
                                        />
                                        <View
                                            style={{
                                                flex: this.getPercentage(compartments[i].id),
                                                backgroundColor: this.getFuelColor(compartments[i].id),
                                                width: window.width / 5 - 15,
                                                alignItems: "center"
                                            }}
                                        />
                                    </View>
                                    <View style={styles.left2Tank}>
                                        <Text style={styles.amountText}>
                                            {this.getAmount(compartments[i].id)}
                                        </Text>
                                    </View>
                                    <View style={styles.overlay}>
                                        <View style={styles.aboutContainer}>
                                            <Text style={styles.aboutText}>
                                                {this.getType(compartments[i].id)}
                                            </Text>
                                            <Text style={styles.aboutText} />
                                            <Text style={styles.estText}>
                                                {this.getFuelEst(compartments[i].allowed_capacity)}
                                            </Text>
                                            <View>
                                                <Text style={styles.recText}>
                                                    {this.getFuelRec(
                                                        compartments[i].allowed_capacity,
                                                        compartments[i].current_amount
                                                    )}
                                                </Text>

                                                <TextInput
                                                    name={compartments[i].id}
                                                    style={{
                                                        height: 40,
                                                        borderColor: "gray",
                                                        borderWidth: 1
                                                    }}
                                                    editable={true}
                                                    placeholderTextColor="#BFBFBF"
                                                    placeholder="#"
                                                    onChangeText={text =>
                                                        this.setCompartState(
                                                            compartments[i].id,
                                                            text,
                                                            this.getCurrentAmount(compartments[i].id)
                                                        )}
                                                    keyboardType="numeric"
                                                />
                                            </View>
                                        </View>
                                        <View style={styles.topOverLay}>
                                            <View style={styles.numberContainer}>
                                                <Text style={styles.numberText}>{truck_cp}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            );
                        } else {
                            cp_view.push(
                                //RIGHT 2 ONLY TANK
                                <View
                                    style={styles.compartmentContainer}
                                    key={compartments[i].id}
                                >
                                    <View style={{ position: "absolute", height: 120 }}>
                                        <View
                                            style={{
                                                flex: this.getLeftOver(compartments[i].id),
                                                width: window.width / 5 - 15,
                                                justifyContent: "center",
                                                alignContent: "center"
                                            }}
                                        />
                                        <View
                                            style={{
                                                flex: this.getPercentage(compartments[i].id),
                                                backgroundColor: this.getFuelColor(compartments[i].id),
                                                width: window.width / 5 - 15,
                                                alignItems: "center"
                                            }}
                                        />
                                    </View>
                                    <View style={styles.right2Tank}>
                                        <Text style={styles.amountText}>
                                            {this.getAmount(compartments[i].id)}
                                        </Text>
                                    </View>
                                    <View style={styles.overlay}>
                                        <View style={styles.aboutContainer}>
                                            <Text style={styles.aboutText}>
                                                {this.getType(compartments[i].id)}
                                            </Text>
                                            <Text style={styles.aboutText} />
                                            <Text style={styles.estText}>
                                                {this.getFuelEst(compartments[i].allowed_capacity)}
                                            </Text>
                                            <View>
                                                <Text style={styles.recText}>
                                                    {this.getFuelRec(
                                                        compartments[i].allowed_capacity,
                                                        compartments[i].current_amount
                                                    )}
                                                </Text>

                                                <TextInput
                                                    name={compartments[i].id}
                                                    style={{
                                                        height: 40,
                                                        borderColor: "gray",
                                                        borderWidth: 1
                                                    }}
                                                    editable={true}
                                                    placeholderTextColor="#BFBFBF"
                                                    placeholder="#"
                                                    onChangeText={text =>
                                                        this.setCompartState(
                                                            compartments[i].id,
                                                            text,
                                                            this.getCurrentAmount(compartments[i].id)
                                                        )}
                                                    keyboardType="numeric"
                                                />
                                            </View>
                                        </View>
                                        <View style={styles.topOverLay}>
                                            <View style={styles.numberContainer}>
                                                <Text style={styles.numberText}>{truck_cp}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            );
                        }
                    }
                }
                truck_cp += 1;
            }
        } else {
            console.log(">>>>>>>>>>>>>>>>>>=======addFuelFlag False======");

            let truck_id = DBHelper.get("Driver", 0).truck_id;
            let compartments = DBHelper.getAll("Compartment").filtered(
                "truck_id =" + truck_id
            );
            var cp_view = [];
            var truck_cp = 1;
            var truck_cpmax = DBHelper.getById("Truck", truck_id).compartments;
            for (let i = 0; i < compartments.length; i++) {
                // let stuff = DBHelper.get('Driver', 0);
                // throw (stuff.truck_id)
                if (truck_cpmax > 2) {
                    if (truck_cp == 1) {
                        //LEFT
                        cp_view.push(
                            <View
                                style={styles.compartmentContainer}
                                key={compartments[i].id}
                            >
                                <View style={{ position: "absolute", height: 120 }}>
                                    <View
                                        style={{
                                            flex: this.getLeftOver(compartments[i].id),
                                            width: window.width / 5 - 15,
                                            justifyContent: "center",
                                            alignContent: "center"
                                        }}
                                    />
                                    <View
                                        style={{
                                            flex: this.getPercentage(compartments[i].id),
                                            backgroundColor: this.getFuelColor(compartments[i].id),
                                            width: window.width / 5 - 15,
                                            alignItems: "center"
                                        }}
                                    />
                                </View>
                                <View style={[styles.rightTank]}>
                                    {/* height: 120, width: window.width/compartments - 50, */}
                                    <Text style={styles.amountText}>
                                        {this.getAmount(compartments[i].id)}
                                    </Text>
                                </View>
                                <View style={styles.overlay}>
                                    <View style={styles.aboutContainer}>
                                        <Text style={styles.aboutText}>
                                            {this.getType(compartments[i].id)}
                                        </Text>
                                        <Text style={styles.aboutText} />
                                        <Text style={styles.estText}>
                                            {this.getFuelEst(compartments[i].allowed_capacity)}
                                        </Text>
                                    </View>
                                    <View style={styles.topOverLay}>
                                        <View style={styles.numberContainer}>
                                            <Text style={styles.numberText}>{truck_cp}</Text>
                                        </View>
                                    </View>

                                    <View style={styles.bottomOverlay} />
                                </View>
                            </View>
                        );
                    } else if (truck_cp == truck_cpmax) {
                        //RIGHT
                        cp_view.push(
                            <View
                                style={styles.compartmentContainer}
                                key={compartments[i].id}
                            >
                                <View style={{ position: "absolute", height: 120 }}>
                                    <View
                                        style={{
                                            flex: this.getLeftOver(compartments[i].id),
                                            width: window.width / 5 - 15,
                                            justifyContent: "center",
                                            alignContent: "center"
                                        }}
                                    />
                                    <View
                                        style={{
                                            flex: this.getPercentage(compartments[i].id),
                                            backgroundColor: this.getFuelColor(compartments[i].id),
                                            width: window.width / 5 - 15,
                                            alignItems: "center"
                                        }}
                                    />
                                </View>
                                <View style={styles.leftTank}>
                                    <Text style={styles.amountText}>
                                        {this.getAmount(compartments[i].id)}
                                    </Text>
                                </View>
                                <View style={styles.overlay}>
                                    <View style={styles.aboutContainer}>
                                        <Text style={styles.aboutText}>
                                            {this.getType(compartments[i].id)}
                                        </Text>
                                        <Text style={styles.aboutText} />
                                        <Text style={styles.estText}>
                                            {this.getFuelEst(compartments[i].allowed_capacity)}
                                        </Text>
                                    </View>
                                    <View style={styles.topOverLay}>
                                        <View style={styles.numberContainer}>
                                            <Text style={styles.numberText}>{truck_cp}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        );
                    } else {
                        //MIDDLE
                        if (truck_cpmax > 3) {
                            if (truck_cp == 2) {
                                //LEFT MIDDLE
                                cp_view.push(
                                    <View
                                        style={styles.compartmentContainer}
                                        key={compartments[i].id}
                                    >
                                        <View style={{ position: "absolute", height: 120 }}>
                                            <View
                                                style={{
                                                    flex: this.getLeftOver(compartments[i].id),
                                                    width: window.width / 5 - 15,
                                                    justifyContent: "center",
                                                    alignContent: "center"
                                                }}
                                            />
                                            <View
                                                style={{
                                                    flex: this.getPercentage(compartments[i].id),
                                                    backgroundColor: this.getFuelColor(
                                                        compartments[i].id
                                                    ),
                                                    width: window.width / 5 - 15,
                                                    alignItems: "center"
                                                }}
                                            />
                                        </View>
                                        <View style={styles.leftMiddleTank}>
                                            <Text style={styles.amountText}>
                                                {this.getAmount(compartments[i].id)}
                                            </Text>
                                        </View>
                                        <View style={styles.overlay}>
                                            <View style={styles.aboutContainer}>
                                                <Text style={styles.aboutText}>
                                                    {this.getType(compartments[i].id)}
                                                </Text>
                                                <Text style={styles.aboutText} />
                                                <Text style={styles.estText}>
                                                    {this.getFuelEst(compartments[i].allowed_capacity)}
                                                </Text>
                                            </View>
                                            <View style={styles.topOverLay}>
                                                <View style={styles.numberContainer}>
                                                    <Text style={styles.numberText}>{truck_cp}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                );
                            } else if (truck_cp == truck_cpmax - 1) {
                                //RIGHT MIDDLE
                                cp_view.push(
                                    <View
                                        style={styles.compartmentContainer}
                                        key={compartments[i].id}
                                    >
                                        <View style={{ position: "absolute", height: 120 }}>
                                            <View
                                                style={{
                                                    flex: this.getLeftOver(compartments[i].id),
                                                    width: window.width / 5 - 15,
                                                    justifyContent: "center",
                                                    alignContent: "center"
                                                }}
                                            />
                                            <View
                                                style={{
                                                    flex: this.getPercentage(compartments[i].id),
                                                    backgroundColor: this.getFuelColor(
                                                        compartments[i].id
                                                    ),
                                                    width: window.width / 5 - 15,
                                                    alignItems: "center"
                                                }}
                                            />
                                        </View>
                                        <View style={styles.rightMiddleTank}>
                                            <Text style={styles.amountText}>
                                                {this.getAmount(compartments[i].id)}
                                            </Text>
                                        </View>
                                        <View style={styles.overlay}>
                                            <View style={styles.aboutContainer}>
                                                <Text style={styles.aboutText}>
                                                    {this.getType(compartments[i].id)}
                                                </Text>
                                                <Text style={styles.aboutText} />
                                                <Text style={styles.estText}>
                                                    {this.getFuelEst(compartments[i].allowed_capacity)}
                                                </Text>
                                            </View>
                                            <View style={styles.topOverLay}>
                                                <View style={styles.numberContainer}>
                                                    <Text style={styles.numberText}>{truck_cp}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                );
                            } else {
                                cp_view.push(
                                    //MIDDLE
                                    <View
                                        style={styles.compartmentContainer}
                                        key={compartments[i].id}
                                    >
                                        <View style={{ position: "absolute", height: 120 }}>
                                            <View
                                                style={{
                                                    flex: this.getLeftOver(compartments[i].id),
                                                    width: window.width / 5 - 15,
                                                    justifyContent: "center",
                                                    alignContent: "center"
                                                }}
                                            />
                                            <View
                                                style={{
                                                    flex: this.getPercentage(compartments[i].id),
                                                    backgroundColor: this.getFuelColor(
                                                        compartments[i].id
                                                    ),
                                                    width: window.width / 5 - 15,
                                                    alignItems: "center"
                                                }}
                                            />
                                        </View>
                                        <View style={styles.middleTank}>
                                            <Text style={styles.amountText}>
                                                {this.getAmount(compartments[i].id)}
                                            </Text>
                                        </View>
                                        <View style={styles.overlay}>
                                            <View style={styles.aboutContainer}>
                                                <Text style={styles.aboutText}>
                                                    {this.getType(compartments[i].id)}
                                                </Text>
                                                <Text style={styles.aboutText} />
                                                <Text style={styles.estText}>
                                                    {this.getFuelEst(compartments[i].allowed_capacity)}
                                                </Text>
                                            </View>
                                            <View style={styles.topOverLay}>
                                                <View style={styles.numberContainer}>
                                                    <Text style={styles.numberText}>{truck_cp}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                );
                            }
                        } else {
                            //MIDDLE IF ONLY 3 COMPARTMENTS
                            cp_view.push(
                                <View
                                    style={styles.compartmentContainer}
                                    key={compartments[i].id}
                                >
                                    <View style={{ position: "absolute", height: 120 }}>
                                        <View
                                            style={{
                                                flex: this.getLeftOver(compartments[i].id),
                                                width: window.width / 5 - 15,
                                                justifyContent: "center",
                                                alignContent: "center"
                                            }}
                                        />
                                        <View
                                            style={{
                                                flex: this.getPercentage(compartments[i].id),
                                                backgroundColor: this.getFuelColor(compartments[i].id),
                                                width: window.width / 5 - 15,
                                                alignItems: "center"
                                            }}
                                        />
                                    </View>
                                    <View style={styles.middle3Tank}>
                                        <Text style={styles.amountText}>
                                            {this.getAmount(compartments[i].id)}
                                        </Text>
                                    </View>
                                    <View style={styles.overlay}>
                                        <View style={styles.aboutContainer}>
                                            <Text style={styles.aboutText}>
                                                {this.getType(compartments[i].id)}
                                            </Text>
                                            <Text style={styles.aboutText} />
                                            <Text style={styles.estText}>
                                                {this.getFuelEst(compartments[i].allowed_capacity)}
                                            </Text>
                                        </View>
                                        <View style={styles.topOverLay}>
                                            <View style={styles.numberContainer}>
                                                <Text style={styles.numberText}>{truck_cp}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            );
                        }
                    }
                } else {
                    if (truck_cpmax == 1) {
                        //SINGLE TANK
                        cp_view.push(
                            <View
                                style={styles.compartmentContainer}
                                key={compartments[i].id}
                            >
                                <View style={{ position: "absolute", height: 120 }}>
                                    <View
                                        style={{
                                            flex: this.getLeftOver(compartments[i].id),
                                            width: window.width / 5 - 15,
                                            justifyContent: "center",
                                            alignContent: "center"
                                        }}
                                    />
                                    <View
                                        style={{
                                            flex: this.getPercentage(compartments[i].id),
                                            backgroundColor: this.getFuelColor(compartments[i].id),
                                            width: window.width / 5 - 15,
                                            alignItems: "center"
                                        }}
                                    />
                                </View>
                                <View style={styles.singleTank}>
                                    <Text style={styles.amountText}>
                                        {this.getAmount(compartments[i].id)}
                                    </Text>
                                </View>
                                <View style={styles.overlay}>
                                    <View style={styles.aboutContainer}>
                                        <Text style={styles.aboutText}>
                                            {this.getType(compartments[i].id)}
                                        </Text>
                                        <Text style={styles.aboutText} />
                                        <Text style={styles.estText}>
                                            {this.getFuelEst(compartments[i].allowed_capacity)}
                                        </Text>
                                    </View>
                                    <View style={styles.topOverLay}>
                                        <View style={styles.numberContainer}>
                                            <Text style={styles.numberText}>{truck_cp}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        );
                    } else {
                        if (truck_cp == 1) {
                            //LEFT 2 ONLY TANK
                            cp_view.push(
                                <View
                                    style={styles.compartmentContainer}
                                    key={compartments[i].id}
                                >
                                    <View style={{ position: "absolute", height: 120 }}>
                                        <View
                                            style={{
                                                flex: this.getLeftOver(compartments[i].id),
                                                width: window.width / 5 - 15,
                                                justifyContent: "center",
                                                alignContent: "center"
                                            }}
                                        />
                                        <View
                                            style={{
                                                flex: this.getPercentage(compartments[i].id),
                                                backgroundColor: this.getFuelColor(compartments[i].id),
                                                width: window.width / 5 - 15,
                                                alignItems: "center"
                                            }}
                                        />
                                    </View>
                                    <View style={styles.left2Tank}>
                                        <Text style={styles.amountText}>
                                            {this.getAmount(compartments[i].id)}
                                        </Text>
                                    </View>
                                    <View style={styles.overlay}>
                                        <View style={styles.aboutContainer}>
                                            <Text style={styles.aboutText}>
                                                {this.getType(compartments[i].id)}
                                            </Text>
                                            <Text style={styles.aboutText} />
                                            <Text style={styles.estText}>
                                                {this.getFuelEst(compartments[i].allowed_capacity)}
                                            </Text>
                                        </View>
                                        <View style={styles.topOverLay}>
                                            <View style={styles.numberContainer}>
                                                <Text style={styles.numberText}>{truck_cp}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            );
                        } else {
                            cp_view.push(
                                //RIGHT 2 ONLY TANK
                                <View
                                    style={styles.compartmentContainer}
                                    key={compartments[i].id}
                                >
                                    <View style={{ position: "absolute", height: 120 }}>
                                        <View
                                            style={{
                                                flex: this.getLeftOver(compartments[i].id),
                                                width: window.width / 5 - 15,
                                                justifyContent: "center",
                                                alignContent: "center"
                                            }}
                                        />
                                        <View
                                            style={{
                                                flex: this.getPercentage(compartments[i].id),
                                                backgroundColor: this.getFuelColor(compartments[i].id),
                                                width: window.width / 5 - 15,
                                                alignItems: "center"
                                            }}
                                        />
                                    </View>
                                    <View style={styles.right2Tank}>
                                        <Text style={styles.amountText}>
                                            {this.getAmount(compartments[i].id)}
                                        </Text>
                                    </View>
                                    <View style={styles.overlay}>
                                        <View style={styles.aboutContainer}>
                                            <Text style={styles.aboutText}>
                                                {this.getType(compartments[i].id)}
                                            </Text>
                                            <Text style={styles.aboutText} />
                                            <Text style={styles.estText}>
                                                {this.getFuelEst(compartments[i].allowed_capacity)}
                                            </Text>
                                        </View>
                                        <View style={styles.topOverLay}>
                                            <View style={styles.numberContainer}>
                                                <Text style={styles.numberText}>{truck_cp}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            );
                        }
                    }
                }
                truck_cp += 1;
            }
        }

        return cp_view;
    }
    /**
     * show Submit Button
     *
     * @return {String} view of button
     */
    showSubmitButton() {
        if (this.state.addFuelFlag) {
            return (
                <TouchableOpacity
                    style={styles.submitButtonContainer}
                    onPress={() => this.addFuel()}
                >
                    <Text style={styles.startText}>Submit</Text>
                </TouchableOpacity>
            );
        }
    }
    /**
      * show Add Fuel Button within the compartment
      *
      * @return {String} Add Fuel Button view
      */
    showAddFuelButton() {
        if (this.state.addFuelFlag) {

        } else {
            return (
                <View style={{ alignItems: "center" }}>
                    <TouchableOpacity
                        style={styles.startButtonContainer}
                        onPress={() => this.setState({ addFuelFlag: true })}
                    >
                        <Text style={styles.startText}>Add Fuel</Text>
                    </TouchableOpacity>
                </View>
            );
        }
    }
    /**
      * show Estimate and Recommendation within the compartment
      *
      * @return {String} text of Rec And Est
      */
    showEstRec() {
        if (this.state.addFuelFlag) {
            return (
                <View style={styles.compartmentContainer}>
                    <View style={styles.aboutContainerEstRecTop} />
                    <View style={styles.aboutContainerEstRec}>
                        <Text style={styles.aboutText} />
                        <Text style={styles.aboutText} />
                        <Text style={styles.estText}>Est: </Text>

                        <Text style={styles.recText}>Rec: </Text>
                    </View>
                </View>
            );
        } else {
            return (
                <View style={styles.compartmentContainer}>
                    <View style={styles.aboutContainerEstRecTop} />
                    <View style={styles.aboutContainerEstRec}>
                        <Text style={styles.aboutText} />
                        <Text style={styles.aboutText} />
                        <Text style={styles.estText}>Est: </Text>
                    </View>
                </View>
            );
        }
    }
    /**
       * show Clients Info within the compartment
       *
       * @return {String} amount of fuel compared to its maximum
       */
    showClientsInfo() {
        console.log("showClientsInfo=========");
        if (this.state.addFuelFlag) {
            let WorkOrder = DBHelper.getById("WorkOrder", 1);

            let Clients = DBHelper.getById("Clients", 1);


            return (
                <View style={{ alignItems: "center" }}>
                </View>
            );
        }
    }
    /**
         * Gets all of the photo attached to the adding of fuel and creates a display for them.
         *
         * @return {Array} Images that have been attached 
         */
    getTotalImage() {
        let photos = this.state.images;
        if (photos.length > 0 && photos != null) {
            return (
                <View style={styles.docTitleContainer}>
                    <Text style={styles.docTitle}>Number of images: {photos.length}</Text>
                </View>
            );
        }
        console.log("no image found");
        return;
    }
    /**
        * Gets all of the photo attached to the adding of fuel and creates a display for them.
        *
        * @return {Array} Images that have been attached 
        */
    getImage() {
        let photos = this.state.images;
        if (photos.length > 0 && photos != null) {
            var images = [];

            for (let i = 0; i < photos.length; i++) {
                console.log("data image");
                console.log(photos[i]);
                //let uri = encodeURI(photos[i].data)

                images.push(
                    <View style={styles.imageContainer} key={i}>
                        {/* <TouchableOpacity style={styles.removeDocContainer} onPress={() => this.removeImageAlert(photos[i].id)}>
                          <Text style={styles.removeDocText}>Remove</Text>
                      </TouchableOpacity> */}
                        {/* PUT A BUTTON THAT CAN REMOVE THE IMAGES FROM THE DATABASE */}
                        <Image style={styles.image} source={{ uri: photos[i] }} />
                    </View>
                );
            }

            return images;
        } else {
            console.log(">>no image found");
        }

        return;
    }

    /**
       * Select Photo Option was selected and displays the ImagePicker.
       */
    selectPhotoTapped() {
        //The quality of the photo going to be returned.
        const options = {
            quality: 1.0,
            maxWidth: 200,
            maxHeight: 200,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, response => {
            console.log("Response = ", response);

            if (response.didCancel) {
                console.log("User cancelled photo picker");
            } else if (response.error) {
                console.log("ImagePicker Error: ", response.error);
            } else if (response.customButton) {
                console.log("User tapped custom button: ", response.customButton);
            } else {
                //let source = { uri: response.uri };
                let source = response.uri;

                // You can also display the image using data:
                //let source = { uri: 'data:image/jpeg;base64,' + response.data };

                var data = this.state.images;
                data.push(source);

                //VERSION for using states
                console.log(source);
                this.setState({
                    image: data
                });
            }
        });
    }
    /**
     * show Add Image Layout within the compartment
     *
     * @return {String} Image view row layout
     */
    showAddImageLayout() {
        if (this.state.addFuelFlag) {
            return (
                <View style={{ flex: 1, flexDirection: "row" }}>{this.getImage()}</View>
            );
        } else {
            return <View style={{ flex: 1, flexDirection: "row" }} />;
        }
    }
    /**
     * show Plus Button within the compartment
     * @return {String} button view
     */
    showPlusButton() {
        if (this.state.addFuelFlag) {
            return (
                <View style={[styles.rowLayout, { marginTop: 5, marginBottom: 5 }]}>
                    <View style={[styles.docContainers, { paddingRight: 5 }]}>
                        <Text style={styles.contentTitle}>Documents:</Text>
                    </View>
                    <View style={styles.docContainers}>
                        <TouchableOpacity onPress={() => this.selectPhotoTapped()}>
                            {/* <Text style={styles.addDoc}>+</Text> */}
                            <Icon2 style={styles.plusIcon} name={"plus"} size={20} />
                        </TouchableOpacity>
                    </View>
                </View>
            );
        } else {
            return (
                <View style={[styles.rowLayout, { marginTop: 5, marginBottom: 5 }]} />
            );
        }
    }
    /**
     * show Vendor Info within the compartment
     *
     * @return {String} vendor information
     */
    showVendorInfo() {
        if (this.state.addFuelFlag) {
            return (
                <View style={{ position: "absolute", alignItems: "center", marginTop: 450 }}>
                    <View style={{ alignItems: "center" }}>
                        <Text style={styles.contentTitle}>Vendor</Text>





                        <Text style={styles.title}>{this.state.clientsName}</Text>
                        <Text style={styles.address}>{this.state.clientsAddress}</Text>
                        <Text style={styles.contentTitle}></Text>
                    </View>

                    <View style={[styles.docContainers, { paddingRight: 20 }]}>
                        <Text style={styles.contentTitle}>Documents:</Text>
                    </View>
                    <View style={styles.docContainers}>
                        <TouchableOpacity onPress={() => this.selectPhotoTapped()}>
                            {/* <Text style={styles.addDoc}>+</Text> */}
                            <Icon2 style={styles.plusIcon} name={"plus"} size={20} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        {this.getImage()}
                    </View>
                    <Text style={styles.address}></Text>



                    <TouchableOpacity
                        style={styles.submitButtonContainer}
                        onPress={() => this.addFuel()}
                    >
                        <Text style={styles.startText}>Submit</Text>
                    </TouchableOpacity>

                </View>
            );
        }
    }
    render() {
        if(DBHelper.getAll("Trip").filtered("active =1").length == 0){
            return (<View style={styles.container}>
                <View style={styles.content}>


                    {this.changeTruck()}



                </View>

            </View>);



        }else{
            return (
                <View style={styles.container}>
                    <View style={styles.content}>
                        {/*Banner that is below the header*/}
                        <View style={styles.bannerContainer}>
                            <View style={styles.bannerTitleContainer}>
                                <Text style={styles.bannerTitle}>{this.getTruckNumber()}</Text>
                            </View>
                            <TouchableOpacity
                                style={styles.bannerButtonContainer}
                                onPress={() => this.changeTruck()}
                            >
                                <View style={styles.bannerAlignButton}>
                                    <Text style={styles.bannerButtonText}>Change</Text>
                                </View>
                                <View style={styles.bannerAlignButton}>
                                    <Icon name={"pencil"} size={20} style={styles.icon} />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.contentContainer}>
                            <Text style={styles.contentTitle}>Compartments Level:</Text>
                            <View style={styles.rowLayout}>
                                {this.showEstRec()}
                                {this.getCompartments()}
                            </View>
                            <View style={{ alignItems: "center" }}>
                                {this.showAddFuelButton()}

                            </View>

                        </View>

                        {this.showVendorInfo()}



                    </View>
                    {/*Child component header*/}
                    <Header />
                    {/*Child component footer*/}
                    <Footer
                        nav={this.props.navigation}
                        truck={0}
                        map={1}
                        workorder={1}
                        settings={1}
                    />
                </View>
            );
        }

    }
}

const window = Dimensions.get("window");

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#333333",
        alignItems: "center",
        justifyContent: "center"
    },
    startButtonContainer: {
        backgroundColor: "#ffa726",
        justifyContent: "center",
        alignItems: "center",
        height: 40,
        width: window.width - 20,
        borderRadius: 5,
        margin: 20
    },
    submitButtonContainer: {
        backgroundColor: "#00cc00",
        justifyContent: "center",
        alignItems: "center",
        height: 40,
        width: window.width - 20,
        borderRadius: 5
    },
    startText: {
        color: "#FFFFFF",
        fontSize: 16,
        fontWeight: "700"
    },
    content: {
        position: "absolute",
        backgroundColor: "#FFFFFF",
        alignItems: "center",
        height: window.height - (45 + 70),
        width: window.width,
        bottom: 45
    },
    bannerContainer: {
        flexDirection: "row",
        height: 70,
        width: window.width,
        justifyContent: "flex-end",
        alignItems: "center",
        backgroundColor: "#e8e8e8"
    },
    clientsContainer: {
        flexDirection: "row",
        height: 70,
        width: window.width,

        alignItems: "center",
        backgroundColor: "#e8e8e8"
    },
    bannerTitleContainer: {
        position: "absolute",
        width: window.width,
        height: 70,
        alignItems: "center",
        justifyContent: "center"
    },
    bannerTitle: {
        fontWeight: "700",
        fontSize: 20,
        color: "#607d8b"
    },
    bannerButtonContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignContent: "center",
        height: 70
    },
    bannerButton: {
        width: 30,
        height: 30
    },
    bannerButtonText: {
        color: "#555555"
    },
    bannerAlignButton: {
        height: 70,
        justifyContent: "center",
        alignContent: "center",
        paddingRight: 10
    },
    contentContainer: {
        flex: 1,
        width: window.width,
        height: window.height - (45 + 70 + 70),
        paddingLeft: 40,
        paddingRight: 40
    },
    contentTitle: {
        fontSize: 16,
        fontWeight: "700",
        color: "#808080",
        paddingTop: 10,
        paddingBottom: 10
    },
    rowLayout: {
        flexDirection: "row",
        justifyContent: "center",
        height: window.height - 800
    },
    es: {
        //borderTopLeftRadius: 40,
        //borderBottomLeftRadius: 40,
        borderColor: "#5F646D",
        borderWidth: 3,
        height: 120,
        width: window.width / 5 - 15,
        justifyContent: "center",
        alignItems: "center"
    },
    rightTank: {
        //borderTopLeftRadius: 40,
        //borderBottomLeftRadius: 40,
        borderColor: "#5F646D",
        borderWidth: 3,
        //borderTopWidth: 3,
        //borderLeftWidth: 3,
        //borderBottomWidth: 3,
        height: 120,
        width: window.width / 5 - 15,
        justifyContent: "center",
        alignItems: "center"
    },
    rightMiddleTank: {
        //marginRight: 0.5,
        //marginLeft: 0.5,
        borderColor: "#5F646D",
        //borderTopWidth: 3,
        //borderBottomWidth: 3,
        borderWidth: 3,
        //borderRightWidth: 6,
        height: 120,
        width: window.width / 5 - 15,
        justifyContent: "center",
        alignItems: "center"
    },
    middleTank: {
        //marginRight: 0.5,
        //marginLeft: 0.5,
        borderColor: "#5F646D",
        //borderTopWidth: 3,
        //borderBottomWidth: 3,
        //borderLeftWidth: 3,
        borderWidth: 3,
        height: 120,
        width: window.width / 5 - 15,
        justifyContent: "center",
        alignItems: "center"
    },
    middle3Tank: {
        //marginRight: 0.5,
        //marginLeft: 0.5,
        borderColor: "#5F646D",
        //borderTopWidth: 3,
        //borderBottomWidth: 3,
        borderWidth: 3,
        //borderLeftWidth: 6,
        //borderRightWidth: 6,
        height: 120,
        width: window.width / 5 - 15,
        justifyContent: "center",
        alignItems: "center"
    },
    leftMiddleTank: {
        //marginRight: 0.5,
        //marginLeft: 0.5,
        borderColor: "#5F646D",
        //borderTopWidth: 3,
        //borderBottomWidth: 3,
        borderWidth: 3,
        //borderLeftWidth: 6,
        height: 120,
        width: window.width / 5 - 15,
        justifyContent: "center",
        alignItems: "center"
    },
    leftTank: {
        //borderTopRightRadius: 40,
        //borderBottomRightRadius: 40,
        borderColor: "#5F646D",
        borderWidth: 3,
        //borderTopWidth: 3,
        //borderBottomWidth: 3,
        //borderRightWidth: 3,
        height: 120,
        width: window.width / 5 - 15,
        justifyContent: "center",
        alignItems: "center"
    },
    singleTank: {
        borderRadius: 40,
        borderColor: "#5F646D",
        //borderWidth: 3,
        borderWidth: 3,
        height: 120,
        width: window.width / 5 - 15,
        justifyContent: "center",
        alignItems: "center"
    },
    right2Tank: {
        //borderTopLeftRadius: 40,
        //borderBottomLeftRadius: 40,
        borderColor: "#5F646D",
        borderWidth: 3,
        height: 120,
        width: window.width / 5 - 15,
        justifyContent: "center",
        alignItems: "center"
    },
    left2Tank: {
        //borderTopRightRadius: 40,
        //borderBottomRightRadius: 40,
        borderColor: "#5F646D",
        borderWidth: 3,
        height: 120,
        width: window.width / 5 - 15,
        justifyContent: "center",
        alignItems: "center"
    },
    tankText: {
        fontSize: 17
    },
    compartmentContainer: {
        alignItems: "center",
        paddingLeft: 0.5,
        paddingRight: 0.5
    },
    numberContainer: {
        width: 60,
        height: 60,
        borderRadius: 30,
        backgroundColor: "#f5f5f5",
        justifyContent: "center",
        alignItems: "center"
    },
    numberText: {
        color: "#ff5722",
        fontSize: 20,
        fontWeight: "600"
    },
    aboutContainer: {
        position: "absolute",
        backgroundColor: "#e6e6e6",
        height: window.height / 5 - 130,
        width: window.width / 5 - 15,
        paddingTop: 20,
        marginTop: 45
    },
    aboutContainerEstRec: {
        position: "relative",

        height: window.height / 5
    },
    aboutContainerEstRecTop: {
        position: "relative",

        height: window.height / 5 - 17
    },
    aboutText: {
        color: "#808080",
        fontWeight: "600",
        flexWrap: "wrap",
        textAlign: "center"
    },
    estText: {
        color: "#00cc33",
        fontWeight: "600",
        flexWrap: "wrap",
        textAlign: "right",
        paddingRight: 10,
    },
    recText: {
        color: "#0033cc",
        fontWeight: "600",
        flexWrap: "wrap",
        textAlign: "right",
        paddingRight: 10,
    },
    addButtonContainer: {
        width: 60,
        height: 60,
        borderRadius: 30,
        backgroundColor: "#78909c",
        justifyContent: "center",
        alignItems: "center"
    },
    addButton: {
        color: "white",
        fontSize: 40,
        fontWeight: "600",
        paddingBottom: 6
    },
    overlay: {
        alignItems: "center",
        paddingTop: 5
    },
    topOverLay: {
        alignContent: "flex-start"
    },
    bottomOverlay: {
        alignContent: "flex-end",
        marginTop: window.height / 5 - 40
    },
    bottomOverlayAddFuel: {
        alignItems: "center",
        alignContent: "flex-end",
        marginTop: window.height / 5 - 40
    },
    amountText: {
        textAlign: "center",
        backgroundColor: "transparent"
    },
    icon: {
        color: "#555555"
    },
    docContainers: {
        justifyContent: "center",
        //backgroundColor: 'blue',
        height: 30
    },
    addDoc: {
        fontSize: 40,
        fontWeight: "600",
        color: "#607d8b"
    },
    icon: {
        color: "grey"
    },
    docTitleContainer: {
        alignItems: "center"
    },
    docTitle: {
        alignItems: "center",
        color: "#607d8b",
        fontSize: 12,
        fontWeight: "700"
    },
    imageContainer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        margin: 5
    },
    image: {
        width: 150,
        height: 150
    },
    removeDocContainer: {
        backgroundColor: "#EF4836",
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center",
        margin: 5,
        width: 80,
        height: 20
    },
    removeDocText: {
        fontSize: 14,
        color: "white"
    },
    plusIcon: {
        fontSize: 25,
        color: "grey"
    }
});
