import React, { Component} from 'react';
import { Animated, Platform, StyleSheet, View, Image, Text, Button, StatusBar, Dimensions, ScrollView, TouchableOpacity, Alert, TextInput, Linking, Geolocation} from 'react-native';

import renderIf from '../../../config/renderif';
import DBHelper from '../../../DBHelper';
import SortableList from 'react-native-sortable-list';
import Row from './Row';
//import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/Octicons';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import Header from './Header';
import Footer from '../../Footer/Footer';
import ImagePicker from 'react-native-image-picker';

/**
 * Parent Component
 *
 *
 */
export default class Details extends Component {

    constructor(props) {
        super(props)



        this.state = {
            workorder_id: props.navigation.state.params.workorder_id, //parameter passed from previous view
            // workorder_id: arr[0], //parameter passed from previous view
            workorder: null, //WorkOrder Realm Object
            client: null, //Client Realm Object
            tasks: null, //Tasks Realm Object
            note: '',
            coordinate: {
                latitude: 0.0,
                longitude: 0.0,
            },
            image: null,
            WO_NOT_STARTED:0,
            WO_ROUTING:1,
            WO_NEXT:2,
            WO_IN_PROGRESS:-1,
            WO_COMPLETED:4,
            WO_SKIPPED:5
        }
        this.activeView = 1
        let status = DBHelper.getById('WorkOrder', props.navigation.state.params.workorder_id).status
        //if(status ==  -1 || status ==  1) {
        //    this.activeView = 2
        //}
        console.log("Details status=" + status + "/ view")
        console.log("Details status=" + DBHelper.getAll('WorkOrder'))

        //if(DBHelper.getAll('Task').filtered('workorder_id =' + this.state.workorder_id).length < 1){
        //    this.activeView = 1
        //}
        this.getLocation() //Gets the drivers current location
        this.getWorkOrder() //Gets all of the Work Order Realm Objects
        this.getClient() //Gets a Client Realm Object
        this.getTasks() //Gets all Task Realm Object
        this.getNote() //Gets the note from Work Order



    }

    /**
     * Return the left tab bar color which changes if
     * the tab is active.
     *
     * @return {String}
     */
    getLeftColor() {
        if (this.activeView == 1) {
            return '#FFFFFF';
        } else {
            return '#CFCFCF';
        }
    }

    /**
     * Disables the left tab bar if active
     *
     * @return {Boolean}
     */
    getLeftClickable() {
        if (this.activeView == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return the right tab bar color which changes if
     * the tab is active.
     *
     * @return {String}
     */
    getRightColor() {
        if (this.activeView == 2) {
            return '#FFFFFF';
        } else {
            return '#CFCFCF';
        }
    }

    /**
     * Disables the right tab bar if active
     *
     * @return {Boolean}
     */
    getRightClickable() {
        if (this.state.tasks.length != 0) {
            if (this.activeView == 2) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Prints the current order of keys for row's
     * See: _renderRow() method
     *
     * @param {Array} array - of row's keys
     */
    printCurrentRowKeys(array) {
        //console.log(array);
    }

    /**
     * This defines the row component and how it will be rendered
     * Please see: Row.js
     * @return {Array}
     */
    _renderRow = ({data, active}) => {
        return <Row data={data} active={active} navigation={this.props.navigation} wo_id={this.state.workorder_id}/>
    }

    /**
     * Returns true if there was a note found in the database
     */
    checkNote() {
        let note = DBHelper.getById('WorkOrder', this.state.workorder_id).note
        if (note != false && note != null && note != '') {
            return true
        }
        return false
    }

    /**
     * Gets the note from the database
     */
    getNote() {
        let note = DBHelper.getById('WorkOrder', this.state.workorder_id).note
        console.log('NOTE: ' + note)
        if (note != false && note != null && note != '') {
            this.state.note = note
        }
    }

    /**
     * Navigates to the "Edit" note view, which allows the driver to edit the note.
     */
    editNote() {
        this.props.navigation.navigate('Note', {wo_id: this.state.workorder_id, editing: true})
    }

// CHANGE OR COPY THIS TO WORK WITH THE SEARCH BAR!
    /**
     * Gets all of the Task Realm Objects from the database
     */
    getTasks() {
        DB_tasks = DBHelper.getAll('Task').filtered('workorder_id =' + this.state.workorder_id)
        this.state.tasks = DB_tasks
    }

    /**
     * Gets all of the Work Order Realm Objects from the database
     */
    getWorkOrder() {
        let workorder = DBHelper.getById('WorkOrder', this.state.workorder_id)
        this.state.workorder = workorder
        console.log(workorder)
    }

    /**
     * Gets a single Client from the database and return the Client Realm Object
     */
    getClient() {
        console.log(this.state.workorder)
        let Clients;

        if(this.state.workorder.vendor_id == ''){
            Clients = DBHelper.getById("Locations", this.state.workorder.location_id);
        }else{

            Clients = DBHelper.getById("Vendor", this.state.workorder.vendor_id);
        }
        this.state.client = Clients
    }

    /**
     * Switches the active tab bar to the selected tab.
     * This results in the whole display changing.
     */
    changeTabs() {
        console.log("Function can result in performance issues");
        if (this.activeView == 1) {
            this.activeView = 2;
            console.log('\nSwitching to Details Tab');
        } else {
            this.activeView = 1;
            console.log('\nSwitching to Task Tab');
        }
        this.forceUpdate();
    }

    /**
     * Returns true if the work order status is routing or open ticket and that
     * the workorder type is not of ReFuel
     */
    checkStatus() {
        //1 = Routing
        //-1 = Open Ticket
        if ((this.state.workorder.status == 1 || this.state.workorder.status == this.state.WO_IN_PROGRESS)
            && this.state.workorder.type != "ReFuel") {
            return true
        }
        return false
    }

    /**
     * Determines if the amount of fuel within each compartment is zero: stops the driver from continuing
     * else the driver will continue to the scanning page.
     */
    scan() {
        //figure out if the total amount of fuel in compartments is 0 (ZERO)
        let driver = DBHelper.get('Driver', 0)
        if (driver != null) {
            let compartments = DBHelper.getAll('Compartment').filtered('truck_id =' + driver.truck_id)
            if (compartments != null) {
                var fuel = 0
                for (let i = 0; i < compartments.length; i++) {
                    if (compartments[i].current_amount > 0) {
                        fuel += compartments[i].current_amount
                    }
                }
                if (fuel == 0) {
                    Alert.alert(
                        'Cannot Open Ticket',
                        'Amount of fuel inside of the compartments are all 0 (Zero).\n Please Add Fuel',
                        [
                            { text: 'Ok', onPress: () => console.log() }
                        ],
                        { cancelable: false }
                    )
                } else {
                    DBHelper.updateElementAndById('WorkOrder', this.state.workorder_id, 'status', -1)
//CHANGE BOOLEAN!
                    Alert.alert(
                        'Start Note',
                        this.state.workorder.start_note,
                        [
                            { text: 'Ok!', onPress: () => console.log() }
                        ],
                        { cancelable: false }
                    )
                    if (true) {
                        this.changeTabs()
                    } else {
                        //this.props.navigation.navigate('Scanning', { wo_id: this.state.workorder_id})
                    }
                }
            } else {
                this.props.navigation.navigate('ErrorHandler', {error: 'Truck has no compartments', type: 'Internal', class: Details.name})
            }
        } else {
            this.props.navigation.navigate('ErrorHandler', {error: 'Trying to scan without a driver', type: 'Internal', class: Details.name})
        }

    }

    /**
     * Determines if the amount of fuel within each compartment is zero: stops the driver from continuing
     * else the driver will continue to the scanning page.
     */
    scanNote() {
        //figure out if the total amount of fuel in compartments is 0 (ZERO)
        let driver = DBHelper.get('Driver', 0)
        if (driver != null) {
            let compartments = DBHelper.getAll('Compartment').filtered('truck_id =' + driver.truck_id)
            if (compartments != null) {
                var fuel = 0
                for (let i = 0; i < compartments.length; i++) {
                    if (compartments[i].current_amount > 0) {
                        fuel += compartments[i].current_amount
                    }
                }
                if (fuel == 0) {
                    Alert.alert(
                        'Cannot Open Ticket',
                        'Amount of fuel inside of the compartments are all 0 (Zero).\n Please Add Fuel',
                        [
                            { text: 'Ok', onPress: () => console.log() }
                        ],
                        { cancelable: false }
                    )
                } else {
                    this.props.navigation.navigate('NoteOpenTK', {wID: this.state.workorder_id})
                }
            } else {
                this.props.navigation.navigate('ErrorHandler', {error: 'Truck has no compartments', type: 'Internal', class: Details.name})
            }
        } else {
            this.props.navigation.navigate('ErrorHandler', {error: 'Trying to scan without a driver', type: 'Internal', class: Details.name})
        }

    }
    /**
     * Provides an alert when clicking the Open Ticket or Continue Scanning button.
     * The alert asks if you like to open the ticket if the work order status is Routing.
     */
    startScanning() {
        if (this.state.workorder.status == 1) {
            // Alert.alert(
            //     'Opening Ticket',
            //     'Wish to open a ticket?',
            //     [
            //         { text: 'Yes', onPress: () => },
            //         { text: 'No', onPress: () => console.log()},
            //     ],
            //     { cancelable: false }
            // )
            //this.scan()
            //this.scanNote()
            this.props.navigation.navigate('NoteOpenTK', {wID: this.state.workorder_id})
        } else if (this.state.workorder.status == this.state.WO_IN_PROGRESS) {
//CHANGE BOOLEAN!
            if (true) {
                this.changeTabs()
            } else {
                //this.props.navigation.navigate('Scanning', { wo_id: this.state.workorder_id})
            }
        } else {

            Alert.alert(
                'Incorrect Status',
                'Work order must moved up into a\n "routing" \nstatus before opening the ticket',
                [
                    { text: 'Ok', onPress: () => console.log()},
                ],
                { cancelable: false }
            )
        }
    }

    /**
     * If the workorder status is either "Routing" (1) or "Open Ticket" (-1) this will be called.
     * This algorithm takes the next workorder after the one you are completing and changes it to Routing,
     * the next one after that is changes to "Next Job".
     *
     * NOTE: all other workorders will be switched to "Waiting" Status (Execpt: Open Ticket)
     * NOTE: this cannot be called for Status such as:
     * - Next Job
     * - Waiting
     */
    completeChangeStatus() {
        let DB_workorder = DBHelper.getById('WorkOrder', this.state.workorder_id)
//END NOTE
        //Alert.alert(
        //    'End Note',
        //    DB_workorder.end_note,
        //    [
        //        { text: 'Ok', onPress: () => console.log() }
        //    ],
        //    { cancelable: false }
        //)
        console.log(DB_workorder.trip_id)

        let data = DBHelper.getAll('WorkOrder').filtered('status != "4" AND trip_id ="' + DB_workorder.trip_id + '"').sorted('sequence')
        if (data != null) {
            var id_array = []
            for (let i = 1; i < data.length; i++) {
                id_array.push(data[i].id)
            }

            var new_sequence = 1;
            for (let i = 0; i < id_array.length; i++) {
                if (this.state.workorder_id != id_array[i]) {
                    if (new_sequence == 1) {
                        DBHelper.updateElementAndById('WorkOrder', id_array[i], 'status', 1)
                    } else if (new_sequence == 2) {
                        DBHelper.updateElementAndById('WorkOrder', id_array[i], 'status', 2)
                    } else {
                        DBHelper.updateElementAndById('WorkOrder', id_array[i], 'status', 3)
                    }
                    //console.log('before sequence: ' + id_array[i] + new_sequence)
                    DBHelper.updateElementAndById('WorkOrder', id_array[i], 'sequence', new_sequence)
                    //console.log('after sequence: ' + id_array[i] + new_sequence)
                    new_sequence += 1;
                }
            }
            if (this.state.workorder.status == this.state.WO_IN_PROGRESS || this.state.workorder.type == 'ReFuel') {
                this.props.navigation.navigate('NoteCloseTK', {wID: this.state.workorder_id})
                //this.completeLeaveCheckList()

            } else {
                this.completeLeaveNote()
            }
        }

    }

    /**
     * If the workorder status is either "Routing" (1) or "Open Ticket" (-1) this will be called.
     * This algorithm takes the next workorder after the one you are completing and changes it to Routing,
     * the next one after that is changes to "Next Job".
     *
     * NOTE: all other workorders will be switched to "Waiting" Status (Execpt: Open Ticket)
     * NOTE: this cannot be called for Status such as:
     * - Next Job
     * - Waiting
     */
    completeChangeStatus_BK() {
        let DB_workorder = DBHelper.getById('WorkOrder', this.state.workorder_id)
//END NOTE
        //Alert.alert(
        //    'End Note',
        //    DB_workorder.end_note,
        //    [
        //        { text: 'Ok', onPress: () => console.log() }
        //    ],
        //    { cancelable: false }
        //)
        console.log(DB_workorder.trip_id)

        let data = DBHelper.getAll('WorkOrder').filtered('status != "4" AND trip_id ="' + DB_workorder.trip_id + '"').sorted('sequence')
        if (data != null) {
            var id_array = []
            for (let i = 1; i < data.length; i++) {
                id_array.push(data[i].id)
            }

            var new_sequence = 1;
            for (let i = 0; i < id_array.length; i++) {
                if (this.state.workorder_id != id_array[i]) {
                    if (new_sequence == 1) {
                        DBHelper.updateElementAndById('WorkOrder', id_array[i], 'status', 1)
                    } else if (new_sequence == 2) {
                        DBHelper.updateElementAndById('WorkOrder', id_array[i], 'status', 2)
                    } else {
                        DBHelper.updateElementAndById('WorkOrder', id_array[i], 'status', 3)
                    }
                    //console.log('before sequence: ' + id_array[i] + new_sequence)
                    DBHelper.updateElementAndById('WorkOrder', id_array[i], 'sequence', new_sequence)
                    //console.log('after sequence: ' + id_array[i] + new_sequence)
                    new_sequence += 1;
                }
            }
            if (this.state.workorder.status == this.state.WO_IN_PROGRESS || this.state.workorder.type == 'ReFuel') {
                //this.completeLeaveCheckList()
                DBHelper.updateElementAndById('WorkOrder', this.state.workorder_id, 'status', 4);
                this.props.navigation.navigate('WorkOrder')
            } else {
                this.completeLeaveNote()
            }
        }
        this.props.navigation.navigate('NoteCloseTK', {wID: this.state.workorder_id})
    }
    /**
     * If the workorder status is "Next Job" this will be called.
     * This algorithm does nothing with first workorder (within the list),
     * the selected work order is changed to "Completed" and the third workorder is switched to "Next Job".
     *
     * NOTE: all other workorders will be switched to "Waiting" Status (Execpt: Open Ticket)
     * NOTE: this can only be used for workorders with the status of "Next Job" ONLY!
     */
    completeNextJob() {
        let DB_workorder = DBHelper.getById('WorkOrder', this.state.workorder_id)
        console.log(DB_workorder.id)

        let data = DBHelper.getAll('WorkOrder').filtered('status != "4" AND trip_id ="' + DB_workorder.trip_id + '"').sorted('sequence')
        if (data != null) {
            var id_array = []
            for (let i = 1; i < data.length; i++) {
                console.log('id: ' + data[i].id + ' status: ' + data[i].status)
                id_array.push(data[i].id)
            }
            console.log(id_array)

            var new_sequence = 1;
            for (let i = 0; i < id_array.length; i++) {
                if (id_array[i].id != this.state.workorder_id) {
                    if (i == 1) {
                        DBHelper.updateElementAndById('WorkOrder', id_array[i], 'status', 2)
                    } else {
                        DBHelper.updateElementAndById('WorkOrder', id_array[i], 'status', 3)
                    }
                    DBHelper.updateElementAndById('WorkOrder', id_array[i], 'sequence', new_sequence)
                    new_sequence += 1;
                }
            }
            //This is to compelete the selected workorder and asks the driver to leave a note when closing.
            this.completeLeaveNote()
        }
    }

    /**
     * Completes the selected workorder and asks the driver to leave a note behind.
     */
    completeLeaveNote() {
        DBHelper.updateElementAndById('WorkOrder', this.state.workorder_id, 'status', 4);
        this.props.navigation.navigate('Note', {wo_id: this.state.workorder_id, editing: false})
    }

    /**
     * Completes the selected workorder and displays the checklist needed to be completed.
     */
    completeLeaveCheckList() {
        DBHelper.updateElementAndById('WorkOrder', this.state.workorder_id, 'status', 4);
        this.props.navigation.navigate('Checklist', {type: 'workorder', workorder_id: this.state.workorder_id})
    }

    completeWork() {
        if (this.state.workorder.status == this.state.WO_IN_PROGRESS) {
            Alert.alert(
                'Confirm',
                'Are you sure you wish to complete the work order?',
                [
                    { text: 'Yes', onPress: () => this.completeChangeStatus()},
                    { text: 'No', onPress: () => console.log()},
                ],
                { cancelable: false }
            )
        } else {
            if (this.state.workorder.status == 1) {
                Alert.alert(
                    'No ticket was opened',
                    'Are you sure you wish to complete the work order?',
                    [
                        { text: 'Yes', onPress: () => this.completeChangeStatus() },
                        { text: 'No', onPress: () => console.log()},
                    ],
                    { cancelable: false }
                )
            } else if (this.state.workorder.status == 2) {
                Alert.alert(
                    'Haven\'t Routed',
                    'Are you sure you wish to complete the work order?',
                    [
                        { text: 'Yes', onPress: () => this.completeNextJob() },
                        { text: 'No', onPress: () => console.log()},
                    ],
                    { cancelable: false }
                )
            } else {
                Alert.alert(
                    'Haven\'t Routed',
                    'Are you sure you wish to complete the work order?',
                    [
                        { text: 'Yes', onPress: () => this.completeLeaveNote() },
                        { text: 'No', onPress: () => console.log()},
                    ],
                    { cancelable: false }
                )
            }
        }
    }

    /**
     * @returns the title for the button which you press to start scanning.
     */
    continueScanning() {
        if (this.state.workorder.status == this.state.WO_IN_PROGRESS) {
            return 'Continue Filling' //WAS CONTINUE SCANNING
        } else {
            return 'Open Ticket'
        }
    }

    /**
     * @return {Linking Object} this will navigates to the MapKit built into the IOS device
     *
     * NOTE: THIS ONLY WORKS FOR IOS NOT ANDROID!
     */
    handleNavigation() {
        let wo_coords = this.state.workorder.coordinate.split(',')
        let coordinates = { latitude: parseFloat(wo_coords[0]), longitude: parseFloat(wo_coords[1]) }
        // const rla = this.region.latitude;
        // const rlo = this.region.longitude;
        const la = parseFloat(wo_coords[0])
        const lo = parseFloat(wo_coords[1])
        //const la = 37.78825
        //const lo = -122.4324

        const rla = this.state.coordinate.latitude
        const rlo = this.state.coordinate.longitude

        const url = `http://maps.apple.com/?saddr=${rla},${rlo}&daddr=${la},${lo}&dirflg=d`;
        return Linking.openURL(url);
    }

    /**
     * Gets the current location of the driver and stores it within the state.
     */
    getLocation() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log("cords: " + position.coords.longitude + ' : ' + position.coords.latitude);


                this.setState({
                    coordinate: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    }
                })
            },
            function (error) {
                console.log(error)
            },
            {
                enableHighAccuracy: true
            }
        );
    }

    /**
     * Displays the image picker to the driver. Allow the driver to
     * either select a already taken photo or take a photo now.
     */
    selectPhotoTapped() {
        //defines the quality of the photo selected or taken
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        //Image picker
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                //let source = { uri: response.uri };
                let source = response.uri;

                // You can also display the image using data:
                //let source = { uri: 'data:image/jpeg;base64,' + response.data };

                //let array = [-1, 'teiweb', 'TEIWEB', '604-644-1334', 'hello@hello.ca', 'teiweb', 52];

                let table_length = DBHelper.getAll('Documents').length
                console.log('table length: ' + table_length)

                let doc = [table_length, this.state.workorder_id, 0, 'photo', source, '', 1]
                DBHelper.add('Documents', doc)
                this.forceUpdate()
                console.log('Added Document')
                //VERSION for using states
                // console.log(source)
                // this.setState({
                //     //avatarSource: source
                //     image: source
                // });
            }
        });
    }

    /**
     * Alerts the driver that he is deleting the document that he has added/selected.
     *
     * @param {Int} doc_id Document Id
     */
    removeImageAlert(doc_id) {
        Alert.alert(
            'Deleting Document',
            'Are you sure you wish to delete this document',
            [
                { text: 'Yes', onPress: () => this.removeImage(doc_id) },
                { text: 'No', onPress: () => console.log()},
            ],
            { cancelable: false }
        )

    }

    /**
     * Removes the document form the database.
     *
     * @param {Int} doc_id Document Id
     */
    removeImage(doc_id) {
        DBHelper.removeById('Documents', doc_id)
        this.forceUpdate()
    }


    /**
     * Gets all of the documents (photos) from the database pretaining to the
     * selected workorder and creates a view for each one.
     *
     * @return {Array[Array]} Array of Image View's
     */
    getImage() {
        let photos = DBHelper.getAll('Documents').filtered('workorder_id =' + this.state.workorder_id + ' AND type="photo"')
        if (photos.length > 0 && photos != null) {
            console.log('image is being called?')
            var images = [];
            images.push(
                <View style={styles.docTitleContainer} key={-1}>
                    <Text style={styles.docTitle}>Number of images: {photos.length}</Text>
                </View>
            )
            for (let i = 0; i < photos.length; i++) {
                if (photos[i].data != null && photos[i].data != '') {
                    console.log('data image')
                    console.log(photos[i].data)
                    //let uri = encodeURI(photos[i].data)
                    images.push(
                        <View style={styles.imageContainer} key={i}>
                            {/* REMOVE IMAGE BUTTON CAN GO HERE */}
                            <TouchableOpacity style={styles.removeDocContainer} onPress={() => this.removeImageAlert(photos[i].id)}>
                                <Text style={styles.removeDocText}>Remove</Text>
                            </TouchableOpacity>
                            <Image
                                style={styles.image}
                                source={{ uri: photos[i].data }}
                            >
                            </Image>
                        </View>
                    )
                } else if (photos[i].url != null && photos[i].url != '') {
                    console.log('url image')
                    //for web urls
                    images.push(
                        <View style={styles.imageContainer} key={i}>
                            <Image
                                style={styles.image}
                                source={{ uri: photos[i].data }}
                            >
                            </Image>
                        </View>
                    )
                } else {
                    continue
                }
            }
            return images
        }

        console.log('no image found')
        return
    }

    /**
     * @returns true if there has been a signature provided before.
     */
    checkSignature() {
        let signatures = DBHelper.getAll('Documents').filtered('workorder_id =' + this.state.workorder_id + ' AND type="signature"')
        if (signatures.length > 0 && signatures != null) {
            return true
        } else {
            return false
        }
    }

    /**
     * Navigates to the signature view which allows the driver to recieve a signature.
     * If the driver recieved a signature earlier it will pull that document id and
     * replace the current signature with the new one.
     */
    startSignature() {
        let signatures = DBHelper.getAll('Documents').filtered('workorder_id =' + this.state.workorder_id + ' AND type="signature"')
        if (signatures.length > 0 && signatures != null) {
            this.props.navigation.navigate('Signature', {workorder_id: this.state.workorder_id, id: signatures[0].id})
        } else {
            this.props.navigation.navigate('Signature', {workorder_id: this.state.workorder_id, id: null})
        }
    }

    /**
     * If a the driver recieved a signature earlier it will be displayed.
     *
     * @returns Image View that contains the signature.
     */
    getSignature() {
        let signatures = DBHelper.getAll('Documents').filtered('workorder_id =' + this.state.workorder_id + ' AND type="signature"')
        if (signatures.length > 0 && signatures != null) {
            return (
                <View style={styles.imageContainer}>
                    <Image
                        style={styles.image}
                        source={{ uri: signatures[0].data}}
                    >
                    </Image>
                </View>
            )
        } else {
            return
        }
    }

    /**
     * Navigates to the print view which displays the current work done to the driver.
     */
    printWork() {
        this.props.navigation.navigate('Print', {workorder_id: this.state.workorder_id, trip_id: null})
    }

    /**
     * If there was no fuel dispensed to any of the compartments the "Print" button
     * will not be displayed on the screen.
     */
    canPrintWork() {
        if (this.state.tasks != null && this.state.tasks.length != 0) {
            for (let i = 0; i < this.state.tasks.length; i++) {
                if (this.state.tasks[i].amount > 0) {
                    return true
                }
            }
        }

        return false
    }

    /**
     * @returns {String} this will be attached to the already exsisting title.
     */
    isRefinery() {
        if (this.state.workorder.type == 'ReFuel') {
            return '(Refinery)'
        } else {
            return
        }
    }

    getButtonText() {
        if (this.state.workorder.status != this.state.WO_IN_PROGRESS && this.state.workorder.type != 'ReFuel') {
            return 'Skip Work Order'
        } else {
            return 'Complete Work Order'
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <View style={styles.buttonContainer}>
                        {/* TAB BAR */}
                        <TouchableOpacity style={styles.buttonLeftContainer} disabled={this.getLeftClickable()} onPress={() => this.changeTabs()}>
                            <View style={styles.buttonLeft}>
                                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                    <View style={{justifyContent: 'center'}}>
                                        <Text style={{ color: this.getLeftColor(), fontWeight: '800', fontSize: 15 }}>
                                            Details
                                        </Text>
                                    </View>
                                </View>
                                {renderIf(this.activeView == 1)(
                                    <Text style={styles.bar}></Text>
                                )}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonRightContainer} disabled={this.getRightClickable()} onPress={() => this.changeTabs()}>
                            <View style={styles.buttonRight}>
                                {renderIf(this.state.workorder.type != "ReFuel")(
                                    <Text style={{ color: this.getRightColor(), fontWeight: '800', fontSize: 15 }}>
                                        Tasks
                                    </Text>
                                )}
                                {renderIf(this.activeView == 2)(
                                    <Text style={styles.bar}></Text>
                                )}
                            </View>
                        </TouchableOpacity>
                    </View>
                    {/* Will not render if this is not the active view */}
                    {renderIf(this.activeView == 1)(
                        <ScrollView style={styles.detailsContainer}>
                            <View style={{alignItems: 'center',}}>
                                <Text style={styles.title}>{this.isRefinery()} {this.state.client.name}</Text>
                                <Text style={styles.address}>{this.state.client.address}</Text>
                                <Text style={styles.coordinates}>{this.state.workorder.coordinate}</Text>
                                <View style={styles.noteContainer}>
                                    <View>
                                        <View style={styles.rowLayout}>
                                            <View style={styles.noteTitleContainer}>
                                                <Text style={styles.noteTitle}>Work Order Note:</Text>
                                            </View>
                                            <TouchableOpacity
                                                style={styles.noteMoreButton}
                                                //NEEDS TO BE CHANGED TO WORK ORDER NOTE NOT START NOTE
                                                onPress={() => alert(this.state.workorder.start_note)}
                                            >
                                                <Icon name={'options'} size={20}></Icon>
                                            </TouchableOpacity>
                                        </View>
                                        {/* //NEEDS TO BE CHANGED TO WORK ORDER NOTE NOT START NOTE */}
                                        <Text style={styles.noteBox}>{this.state.workorder.start_note}</Text>
                                    </View>
                                    <View>
                                        <View style={styles.rowLayout}>
                                            <View style={styles.noteTitleContainer}>
                                                <Text style={styles.noteTitle}>Leaving Office Notes:</Text>
                                            </View>
                                            <TouchableOpacity
                                                style={styles.noteMoreButton}
                                                onPress={() => alert(this.state.workorder.start_note)}
                                            >
                                                <Icon name={'options'} size={20}></Icon>
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={styles.noteBox}>{this.state.workorder.start_note}</Text>
                                    </View>
                                    <View>
                                        <View style={styles.rowLayout}>
                                            <View style={styles.noteTitleContainer}>
                                                <Text style={styles.noteTitle}>Start Notes:</Text>
                                            </View>
                                            <TouchableOpacity
                                                style={styles.noteMoreButton}
                                                onPress={() => alert(this.state.workorder.start_note)}
                                            >
                                                <Icon name={'options'} size={20}></Icon>
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={styles.noteBox}>{this.state.workorder.start_note}</Text>
                                    </View>
                                    <View>
                                        <View style={styles.rowLayout}>
                                            <View style={styles.noteTitleContainer}>
                                                <Text style={styles.noteTitle}>End Notes:</Text>
                                            </View>
                                            <TouchableOpacity
                                                style={styles.noteMoreButton}
                                                onPress={() => alert(this.state.workorder.end_note)}
                                            >
                                                <Icon name={'options'} size={20}></Icon>
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={styles.noteBox}>{this.state.workorder.end_note}</Text>
                                    </View>
                                </View>
                                {/* PRINT BUTTON */}
                                {renderIf(this.canPrintWork())(
                                    <View>
                                        <TouchableOpacity style={styles.printButtonContainer} onPress={() => this.printWork()}>
                                            <Text style={styles.printText}>Print Work</Text>
                                        </TouchableOpacity>
                                    </View>
                                )}
                                {/* GET DIRECTION BUTTON */}
                                <View>
                                    <TouchableOpacity style={styles.directionsButtonContainer} onPress={() => this.handleNavigation()}>
                                        <Text style={styles.directionsText}>Get Direction</Text>
                                    </TouchableOpacity>
                                </View>


                                {/* SCANNING BUTTON */}
                                {renderIf(this.checkStatus())(
                                    <View>
                                        <TouchableOpacity style={styles.startButtonContainer} onPress={() => this.startScanning()}>
                                            <Text style={styles.startText}>{this.continueScanning()}</Text>
                                        </TouchableOpacity>
                                    </View>
                                )}
                                <View>
                                    {/* ATTACH DOCUMENTS */}
                                    <View style={styles.attachmentContainer}>
                                        <View style={styles.rowLayout}>
                                            <View style={styles.attachmentLeft}>
                                                <Text style={styles.attachmentLabels}>Documents</Text>
                                            </View>
                                            <View style={styles.attachmentRight}>
                                                <TouchableOpacity onPress={() => this.selectPhotoTapped()}>
                                                    <Icon2  style={styles.addButton} name={'plus'} size={20}></Icon2>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                    {this.getImage()}
                                    {/* SIGNATURES */}
                                    <View style={styles.attachmentContainer}>
                                        <View style={styles.rowLayout}>
                                            <View style={styles.attachmentLeft}>
                                                <Text style={styles.attachmentLabels}>Signature</Text>
                                            </View>
                                            <View style={styles.attachmentRight}>
                                                {renderIf(this.checkSignature())(
                                                    <TouchableOpacity onPress={() => this.startSignature()}>
                                                        <Text style={styles.editText}>Edit</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {renderIf(!this.checkSignature())(
                                                    <TouchableOpacity onPress={() => this.startSignature()}>
                                                        <Icon2  style={styles.addButton} name={'plus'} size={20}></Icon2>
                                                    </TouchableOpacity>
                                                )}

                                            </View>
                                        </View>
                                    </View>
                                    {this.getSignature()}
                                    {/* NOTES */}
                                    <View style={styles.attachmentContainer}>
                                        <View style={styles.rowLayout}>
                                            <View style={styles.attachmentLeft}>
                                                <Text style={styles.attachmentLabels}>Notes</Text>
                                            </View>
                                            <View style={styles.attachmentRight}>
                                                {renderIf(this.checkNote())(
                                                    <TouchableOpacity onPress={() => this.editNote()}>
                                                        <Text style={styles.editText}>Edit</Text>
                                                    </TouchableOpacity>
                                                )}
                                                {renderIf(!this.checkNote())(
                                                    <TouchableOpacity onPress={() => this.editNote()}>
                                                        <Icon2  style={styles.addButton} name={'plus'} size={20}></Icon2>
                                                    </TouchableOpacity>
                                                )}
                                            </View>
                                        </View>
                                        {renderIf(this.checkNote())(
                                            <View>
                                                <TextInput
                                                    editable={false}
                                                    multiline={true}
                                                    value={this.state.note}
                                                    style={styles.driverNoteText}
                                                >
                                                </TextInput>
                                            </View>
                                        )}
                                    </View>
                                </View>
                                {/* COMPLETE WORKORDER BUTTON */}
                                {renderIf(this.state.workorder.status != 4)(
                                    <TouchableOpacity style={styles.completeButton} onPress={() => this.completeWork()}>
                                        <Text style={styles.compeleteText}>{this.getButtonText()}</Text>
                                    </TouchableOpacity>
                                )}
                            </View>
                        </ScrollView>
                    )}
                    {/* TASK LIST VIEW */}
                    {renderIf(this.activeView == 2)(
                        <SortableList
                            style={styles.list}
                            contentContainerStyle={styles.listContainer}
                            data={this.state.tasks}
                            renderRow={this._renderRow}
                            sortingEnabled={false}
                        />
                    )}
                </View>
                <Header nav={this.props.navigation} wo_id={this.state.workorder_id}/>
                <Footer nav={this.props.navigation} truck={1} map={1} workorder={1} settings={1} />
            </View>
        );

    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        height: window.height - (45 + 70),
        width: window.width,
        bottom: 45,
    },
    buttonContainer: {
        flexDirection: 'row',
        height: 60,
    },
    buttonLeftContainer: {
        flex: 0.5,
        backgroundColor: '#79909B',
    },
    buttonLeft: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
    },
    buttonRightContainer: {
        flex: 0.5,
        backgroundColor: '#656F78',
    },
    buttonRight: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
    },
    bar: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#F98B24',
        height: 4,
        width: window.width/2,
    },
    list: {
        flex: 1,
    },
    listContainer: {
        width: window.width,

        ...Platform.select({
            ios: {
                paddingHorizontal: 5,
            },

            android: {
                paddingHorizontal: 0,
            }
        })
    },
    detailsContainer: {
        flex: 1,
        width: window.width,
        paddingTop: 10,
    },
    title: {
        color: 'black',
        fontSize: 18,
        fontWeight: '700',
    },
    address: {
        color: '#888888',
        fontSize: 13,
    },
    coordinates: {
        color: '#888888',
        fontSize: 12,
    },
    noteContainer: {
        backgroundColor: '#ffe88a',
        marginTop: 10,
        marginBottom: 10,
        margin: 20,
        padding: 10,
    },
    startButtonContainer: {
        marginTop: 0,
        margin: 10,
        backgroundColor: '#ffa726',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: window.width - 40,
        borderRadius: 5,
    },
    startText: {
        color: '#FFFFFF',
        fontSize: 18,
        fontWeight: '700',
    },
    directionsButtonContainer: {
        marginTop: 0,
        margin: 10,
        backgroundColor: '#4183D7',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: window.width - 40,
        borderRadius: 5,
    },
    directionsText: {
        color: '#FFFFFF',
        fontSize: 18,
        fontWeight: '700',
    },
    printButtonContainer: {
        marginTop: 0,
        margin: 10,
        backgroundColor: '#26A65B',
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
        width: window.width - 180,
        borderRadius: 5,
    },
    printText: {
        color: '#FFFFFF',
        fontSize: 18,
        fontWeight: '700',
    },
    attachmentLabels: {
        color: '#607d8b',
        fontSize: 15,
        fontWeight: '800',
    },
    attachmentContainer: {
        paddingLeft: 10,
        paddingRight: 10,
        width: window.width - 20,
        borderBottomWidth: 1,
        borderColor: '#dedede',
        marginBottom: 10,
    },
    attachmentLeft: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        flex: 0.5,
    },
    attachmentRight: {
        alignItems: 'flex-end',
        flex: 0.5,
        paddingRight: 20,
    },
    rowLayout: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    addButton: {
        color: '#607d8b',
        fontSize: 25,
        fontWeight: '900',
    },
    completeButton: {
        height: 60,
        width: window.width - 20,
        backgroundColor: '#ebebeb',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        marginBottom: 50,
    },
    compeleteText: {
        color: '#78909c',
        fontSize: 14,
    },
    noteTitleContainer: {
        height: 23,
        paddingRight: 5,
    },
    noteTitle: {
        color: '#ff5722',
        fontSize: 16,
        fontWeight: '700',
    },
    noteMoreButton: {
        alignItems: 'center',
        height: 22,
        width: 30,
    },
    noteBox: {
        paddingBottom: 5,
        flexWrap: 'wrap',
    },
    driverNoteContainer: {

    },
    editText: {
        color: '#607d8b',
        fontSize: 16,
        fontWeight: '700',
    },
    driverNoteText: {
        fontSize: 14,
    },
    docTitleContainer: {
        alignItems: 'center',
    },
    docTitle: {
        color: '#607d8b',
        fontSize: 12,
        fontWeight: '700',
    },
    imageContainer: {
        alignItems: 'center',
        margin: 5,
    },
    image: {
        width: window.width - 100,
        height: 400,
    },
    removeDocContainer: {
        backgroundColor: '#EF4836',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        width: 80,
        height: 20,
    },
    removeDocText: {
        fontSize: 14,
        color: 'white'
    },

});