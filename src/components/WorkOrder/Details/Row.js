import React, { Component } from 'react';
import {
  Animated,
  Easing,
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
  Dimensions,
  Platform,
  Button,
  TouchableOpacity
} from 'react-native';
import SortableList from 'react-native-sortable-list';
import * as Progress from 'react-native-progress';
import DBHelper from '../../../DBHelper';
import renderIf from '../../../config/renderif';

export default class Row extends Component {

    constructor(props) {
        super(props);

        this._active = new Animated.Value(0);

        // Style specific to the type of device
        this._style = {
            ...Platform.select({
                ios: {
                    transform: [{
                        scale: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [1, 1.1],
                        }),
                    }],
                    shadowRadius: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [2, 10],
                    }),
                },

                android: {
                    transform: [{
                        scale: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [1, 1.07],
                        }),
                    }],
                    elevation: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [2, 6],
                    }),
                },
            })
        };

        this.localData = [];
        this.progress = 0;
    }

    //PART OF THE LIBRARY
    componentWillReceiveProps(nextProps) {
        if (this.props.active !== nextProps.active) {
        Animated.timing(this._active, {
            duration: 300,
            easing: Easing.bounce,
            toValue: Number(nextProps.active),
        }).start();
        }
    }
    
    /**
     * Determines the progress of the task
     *
     * @param {Array} data - A single task within the Array of task passed into
     * SortableList view.
     */
    setLocalData(data) {
        this.localData = data;
        if (data.fill != -1) {
            this.progress = data.amount / data.fill;
        }
    }

    /**
     *Gets the tasks type of fuel (unit)
     */
    getFuelType(fuel_id) {
        try {
            let fuel = DBHelper.getById('Fuel', fuel_id)
            if (fuel == undefined) {
                throw ('Failed to get name of fuel type with id: ' + fuel_id + ' and type: ' + fuel);
            }
            return fuel.name
        } catch (error) {
            this.props.navigation.navigate('ErrorHandler', {error: error, type: 'FuelType', class: Row.name})
        }
    }

    completedWorkOrder() {
        let wo = DBHelper.getById('WorkOrder', this.props.wo_id)
        if (wo.status != -1) {
            return true
        } else {
            return false
        }
    }

    render() {
        const { data, active, navigation, wo_id} = this.props;
        this.setLocalData(data);
        return (
            <Animated.View 
            style={[
                styles.row,
                this._style,
                
            ]}>
                <TouchableOpacity
                    disabled={this.completedWorkOrder()}
                    onPress={() => navigation.navigate('Manual', { wo_id: wo_id, task_id: data.id})}
                >
                    <View style={{width: window.width - 5 * 2, alignItems: 'center',}}>
                        <View>
                            <Text style={styles.label}>Unit {data.unit_number}</Text>
                        </View>
                        <View style={styles.rowLayout}>
                            <View style={styles.rowLayoutContainers}>
                                <Text style={styles.typeAmountLabel}>{this.getFuelType(data.fuel_id)}</Text>
                            </View>
                            <View style={styles.rowLayoutContainers}>
                                {renderIf(data.fill != -1)(
                                    <Text style={styles.typeAmountLabel}>{data.amount} / {data.fill} L</Text>
                                )}
                                {renderIf(data.fill == -1)(
                                    <Text style={styles.typeAmountLabel}>Fill Unit</Text>
                                )}
                            </View>
                        </View>
                        {renderIf(data.fill != -1)(
                            <View>
                                {/*Progress Bar*/}
                                <Progress.Bar
                                    style={styles.amountBar}
                                    width={window.width - 20}
                                    progress={this.progress}
                                    color={'#ffa726'}
                                    unfilledColor={'#78909c'}
                                />
                            </View>
                        )}
                        {renderIf(data.fill == -1)(
                            <View>
                                <Text style={[styles.typeAmountLabel, {fontWeight: '600', fontSize: 15}]}>Dispensed: {data.amount}</Text>
                            </View>
                        )}
                    </View>
                </TouchableOpacity>
            </Animated.View>
        );
    }
}

const window = Dimensions.get('window');

const styles = StyleSheet.create({
    row: {
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#F0F0F2',
        padding: 10,
        paddingLeft: 2,
        paddingRight: 2,
        //height: 60,
        flex: 1,
        marginTop: 2,
        marginBottom: 0,
        borderRadius: 4,
        

        ...Platform.select({
            ios: {
                width: window.width - 5 * 2,
                shadowColor: 'rgba(0,0,0,0.2)',
                shadowOpacity: 1,
                shadowOffset: {height: 2, width: 2},
                shadowRadius: 2,
            },

            android: {
                width: window.width - 5 * 2,
                elevation: 0,
                marginHorizontal: 30,
            },
        })
    },
    rowLayout: {
        flexDirection: 'row',
        //paddingTop: 0, unknown added padding from bottom of Unit Label
        paddingBottom: 5,
    },
    rowLayoutContainers: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    amountBar: {
        //to add styles to bar container
    },
    label: {
        color: '#607d8b',
        fontSize: 16,
        fontWeight: '800',
    },
    typeAmountLabel: {
        color: '#607d8b',
        fontSize: 14,
    }
});