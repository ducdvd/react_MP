import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, TouchableOpacity, Geolocation, Alert  } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

import { CheckboxField, Checkbox } from 'react-native-checkbox-field';

import renderIf from '../../../config/renderif';
import DBHelper from '../../../DBHelper';

import Header from './Header';

/**
 *
 * THIS COMPONENT IS NOT USED AT ALL WITHIN THE APPLICATION!
 *
 */
export default class Dispensed extends Component {

    constructor(props) {
        super(props)
        this.state = {
            status: 'scan',
            workorder_id: props.navigation.state.params.wo_id, //Parameter passed from a previous view
            task_id: props.navigation.state.params.task_id, //Parameter passed from a previous view
            //workorder: null,
            //task: null,
            dispensed: props.navigation.state.params.dispensed,
            compartments: null,
        };
    }
    
    // getWorkOrder() {
    //     this.state.workorder = DBHelper.getById('WorkOrder', this.state.workorder_id)
    // }

    getCompartments() {
        let truck_id = DBHelper.get('Driver', 0).truck_id;
        let fuel_id = DBHelper.getById('Task', this.state.task_id).fuel_id
        let compartments = DBHelper.getAll('Compartment').filtered('truck_id =' + truck_id + ' AND fuel_id =' + fuel_id);

        return compartments
    }

    getStateCompartments() {
        let compartments = this.getCompartments()
        let temp = []
        for (let i = 0; i < compartments.length; i++) {
            temp.push({ id: compartments[i].id , amount: compartments[i].current_amount})
        }

        return temp
    }

    getView() {
        console.log('called')
        var view = []

        let compartments = this.getCompartments()
        if (this.state.compartments == null) {
            this.state.compartments = this.getStateCompartments()
        }

        for (let i = 0; i < compartments.length; i++) {
            view.push(
                <View style={styles.rowLayout} key={i}>
                    <TouchableOpacity style={styles.compartmentContainer} onPress={() => this.minusAmount(i)}>
                        <Text style={styles.mainText}>Compartment {compartments[i].sequence}</Text>
                        <Text style={styles.secondaryText}>Fuel Type: {this.getFuelType(compartments[i].fuel_id)}</Text>
                        <Text style={styles.secondaryText}>Amount: {this.state.compartments[i]['amount']}</Text>
                    </TouchableOpacity>
                </View>
            )
        }

        return view;
    }

    getFuelType(fuel_id) {
        return DBHelper.getById('Fuel', fuel_id).name
    }

    resetChanges() {
        let compartments = this.getStateCompartments()
        this.setState({
            dispensed: this.props.navigation.state.params.dispensed,
            compartments: compartments,
        })
    }

    minusAmount(array_id) {
        let compartments = this.state.compartments
        if (compartments[array_id]['amount'] > this.state.dispensed) {
            console.log('>')
            let temp = compartments[array_id]['amount']
            compartments[array_id]['amount'] = (temp - this.state.dispensed)
            this.setState({
                compartments: compartments,
                dispensed: 0,
            })
            console.log(this.state.compartments[array_id]['amount'] + ' : ' + this.state.dispensed)
        } else if (compartments[array_id]['amount'] < this.state.dispensed) {
            console.log('<')
            let temp = compartments[array_id]['amount']
            compartments[array_id]['amount'] = 0
            this.setState({
                compartments: compartments,
                dispensed: (this.state.dispensed - temp),
            })
            console.log(this.state.compartments[array_id]['amount'] + ' : ' + this.state.dispensed)
        } else {
            console.log('nothin')
            //do nothing
        }
    }

    alertConfirm() {
        if (this.state.dispensed == 0) {
            Alert.alert(
                'Confirmation',
                'Are you sure of the changes you have made?',
                [
                //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    { text: 'Yes', onPress: () => this.confirmChanges()},
                    { text: 'No', onPress: () => console.log('No Pressed') },
                ],
                { cancelable: false }
            )
        }
    }

    confirmChanges() {

        let compartments = this.state.compartments
        for (let i = 0; i < compartments.length; i++) {

            let old_cp = DBHelper.getById('Compartment', compartments[i]['id'])
            //console.log(old_cp)
            let temp = []
            temp.push(old_cp.id)
            temp.push(old_cp.sequence)
            temp.push(old_cp.truck_id)
            temp.push(old_cp.fuel_id)
            temp.push(old_cp.allowed_capacity)
            temp.push(old_cp.current_amount)

            temp[5] = compartments[i]['amount']
            console.log(temp['current_amount'])
            DBHelper.updateById('Compartment', compartments[i]['id'], temp)
            //console.warn(DBHelper.getById('Compartment', compartments[i]['id']).current_amount)
        }

        
        let old_task = DBHelper.getById('Task', this.state.task_id)
        //console.log(old_task)
        let temp = []
        temp.push(old_task.id)
        temp.push(old_task.workorder_id)
        temp.push(old_task.unit_number)
        temp.push(old_task.amount)
        temp.push(old_task.fill)
        temp.push(old_task.fuel_id)
        temp.push(old_task.rfid)
        temp.push(old_task.status)

        let old = temp[3]
        temp[3] = (old + this.props.navigation.state.params.dispensed)
        DBHelper.updateById('Task', this.state.task_id, temp);

        this.props.navigation.navigate('Scanning', { wo_id: this.state.workorder_id })
    }


    render() {
        return (
            <View style={styles.container}>    
                <View style={styles.content}>
                    <View style={styles.rowLayout}>
                        <View style={styles.left}>
                            <Text style={styles.dispensedLabel}>Dispensed</Text>
                        </View>
                        <View style={styles.right}>
                            <View style={styles.dispensedContainer}>
                                <Text style={styles.dispensedNumber}>{this.state.dispensed}</Text>
                            </View>
                        </View>
                    </View>

                    {this.getView()}

                    <View>
                        <TouchableOpacity style={styles.startButtonContainer} onPress={() => this.alertConfirm()}>
                            <Text style={styles.startText}>Confirm Changes</Text>
                        </TouchableOpacity>
                    </View>

                    <View>
                        <TouchableOpacity style={[styles.startButtonContainer, {backgroundColor: '#EC644B'}]} onPress={() => this.resetChanges()}>
                            <Text style={styles.startText}>Reset Changes</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Header />
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        height: window.height - (70),
        width: window.width,
        top: 70,
    },



    topContainer: {
        backgroundColor: '#d9d9d9',
        height: ((window.height - (45 + 70)) / 8) * 3,
        width: window.width,
        flexDirection: 'row',
    },
    middleContainer: {
        //backgroundColor: '#ffc547',
        height: ((window.height - (45 + 70)) / 8) * 3,
        width: window.width,
        justifyContent: 'center',
        alignItems: 'center',
    },

    rowLayout: {
        flexDirection: 'row',
        paddingBottom: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    left: {
        flex: 0.4,
        alignItems: 'flex-end',
        paddingRight: 10,
        justifyContent: 'center',
    },
    right: {
        flex: 0.6,
        alignItems: 'flex-start',
        paddingLeft: 10,
    },
    dispensedLabel: {
        color: '#607d8b',
        fontSize: 18,
    },
    dispensedContainer: {
        margin: 20,
        backgroundColor: '#e6e6e6',
        width: 130,
        height: 30,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    dispensedNumber: {
        color: '#607d8b',
        fontSize: 18,
    },    

    compartmentContainer: {
        backgroundColor: '#78909c',
        borderRadius: 5,
        borderColor: '#78909c',
        width: window.width - 20,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainText: {
        color: 'white',
        fontSize: 16,
        fontWeight: '700',
    },
    secondaryText: {
        color: 'white',
        fontSize: 14,
        fontWeight: '600',
    },

    startButtonContainer: {
        backgroundColor: '#ffa726',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        width: window.width - 60,
        borderRadius: 5,
        margin: 5,
    },
    startText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: '700',
    },
});
