import React, { Component}  from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, TouchableOpacity, Geolocation, Alert, TextInput, KeyboardAvoidingView, ScrollView, Keyboard } from 'react-native';


import { CheckboxField, Checkbox } from 'react-native-checkbox-field';

import DBHelper from '../../../DBHelper';
import Header from './Header';


/**
 * Parent Component
 *
 * The Checklist component extends the react component and dynamically displays either the work order or
 * truck checklists specified within the checklist sqlite table.
 *
 * @author Duc Do
 */
export default class LeaveNote extends Component {

    /**
     * Defines the state and calls certain function before renderingthe view.
     *
     * @param {object} props - Contains navigation methods and information about previous component
     */
    constructor(props) {
        super(props)

        this.state = { //when component view is rendered use setState({})
            trip_type: null,
            workorder_id: props.navigation.state.params.wo_id, //is set if checklist table has it
            workorder_status: props.navigation.state.params.wd_status, //is set if checklist table has it
            checklist_id: null, //is set if checklist table has it
            items: null, //REMOVE
            new_items: null, //all items within a note before
            new_items_workorder: null, //all items within a note workorder
            text: null, //RENAME: to note
            total_checked: 0, //total_checked notes


        };
        console.log("NoteOpenTK workorder_id >>"  +this.state.workorder_id)
        this.getItems() //looks for items of checklist in database
    }


    /**
     * Gets an array of Items pertaining to state's checklist_id from via. Database
     * Creates a answer format example : [false, 'N'] ... should be changed
     *
     * NOTE: should be called after checklist_id state is set
     * @throws - Failed to get checklist_id within state
     */
    getItems() {
        try {
            if (this.state.workorder_id != null) {
                //Gets an array of items by checklist_id from via. database
                //Sorted by the sequence so that you can have a higharcy
                //let items = DBHelper.getAll('Checkitems').filtered('list_id =' + this.state.checklist_id).sorted('sequence')
                let items = DBHelper.getAll('WorkOrder').filtered('id =' + this.state.workorder_id).sorted('sequence')
                if (items != null) {
                    var data = []
                    var data_workorder = []
                    for (let i = 0; i < items.length; i++) {
                        let note_before = items[i].end_note
                        let note_workorder = null
                        if(items[i].workorder_note_sit3 == 1){
                            note_workorder = items[i].workorder_note

                        }

                        if (note_before != '') {
                            //yes or no answer structure
                            data.push([false, 'N'])
                        }
                        if (note_workorder != null) {
                            //yes or no answer structure
                            data_workorder.push([false, 'N'])
                        }
                    }
                    this.state.new_items = data //saves answer format to state
                    this.state.new_items_workorder = data_workorder //saves answer format to state
                    console.log("total start note items=" + data.length + "/" + data_workorder.length )
                } else {
                    //should be called unless, fms has caused the error
                    throw('Didnt get any items?')
                }
            } else {
                //should never be called unless someone goes extremely wrong!
                throw('Failed to get within state')
            }
        } catch (error) {
            //this.props.navigation.navigate('ErrorHandler', {error: error, type: 'Internal', class: NoteOpenTK.name})
        }

        // let type = 'text'
        // if (type == 'yesno') {
        //     this.state.new_items = [[true, 'Y'], [false, 'N'], [true, 'Y']]
        // } else if (type == 'text') {
        //     this.state.new_items = [['stuff can go here'], [''], ['lots of stuff here\n tooooooooooooooo']]
        // } else {
        //     this.state.new_items = [[true, 'D', false, 'R'], [false, 'D', false, 'R'], [false, 'D', false, 'R']]
        // }

        //[{ type: 'yesno', data: [true, 'Y'] } , [false, 'N'], [true, 'Y'], ['stuff can go here'], [''], [true, 'D', false, 'R'], ]

    }

    //NEW dynamic data
    // {
    // 'checklist': {
    //     'id': 21,
    //     'type': 'workorder/truck'
    //     'name' : 'location checklist',
    //     'items' : [
    //         {
    //             'id': 34234234,
    //             'description': 'Air Brake System',
    //             'type': 'selection', 'text', 'checks', 'dropdown'
    //                                 //'answers': 'defected'
    //             //'answers': 'yes'
    //             'values': ['D', 'R']
    //         },
    //     ]
    // }

    //     [{id: 23 /*workorder_id or truck_id*/, checklist_id: 1}]

    /**
     * Updates the answer structure stored within the state with the selected
     * button or inputted text
     *
     * @param {int} array_id //identifier within the answer state array (or database)
     * @param {string} who //only set for defected or repaired (which is select)
     * @param {string} text //only set for text inputt
     */
    updateItem(array_id) {
        //Array of item answer structures
        var items = this.state.new_items
        console.log("updateItem >>"  +this.state.new_items)
        console.log("updateItem >>total note items array_id=" + array_id )
        //array_id = 0
        //Changes the answers and saves the new item answer structures

        if (items[array_id][1] == 'Y') {
            items[array_id][0] = false
            items[array_id][1] = 'N'
        } else {
            items[array_id][0] = true
            items[array_id][1] = 'Y'
        }



        this.setState({
            new_items: items,

        })
    }
    /**
     * Updates the answer structure stored within the state with the selected
     * button or inputted text
     *
     * @param {int} array_id //identifier within the answer state array (or database)
     * @param {string} who //only set for defected or repaired (which is select)
     * @param {string} text //only set for text inputt
     */
    updateItem_WorkOrder(array_id) {
        //Array of item answer structures

        var items_workorder = this.state.new_items_workorder
        console.log("updateItem_WorkOrder >>"  +this.state.items_workorder)
        console.log("updateItem_WorkOrder >>total note items array_id=" + array_id)
        //array_id = 0
        //Changes the answers and saves the new item answer structures

        if (items_workorder[array_id][1] == 'Y') {
            items_workorder[array_id][0] = false
            items_workorder[array_id][1] = 'N'
        } else {
            items_workorder[array_id][0] = true
            items_workorder[array_id][1] = 'Y'
        }


        this.setState({

            new_items_workorder: items_workorder
        })
    }
    /**
     * Submitting the array of item answer structures to the database.
     * Since both array.length is equaivalent you can match arrays and compare
     * The answers will be stored inside of the workorder or truck table column.
     */
    submitList() {

        console.log("Note Workorder List ------>>"  +this.state.new_items_workorder)

        console.log("Note  List ---------->>"  +this.state.new_items)
        let note_array = this.state.new_items
        let note_workorder_array = this.state.new_items_workorder
        let flag = false
        for (let i = 0; i < note_array.length; i++) {
            if(note_array[i][1] == "N"){
                flag = true


            }
        }
        for (let i = 0; i < note_workorder_array.length; i++) {
            if(note_workorder_array[i][1] == "N" ){
                flag = true


            }
        }

        if(flag == true ){
            var notes = ''


            notes = 'Please select all note!'


            Alert.alert(
                'Note Confirm',
                notes,
                [
                    { text: 'Ok', onPress: () => console.log() }
                ],
                { cancelable: false }
            )

        }else{
            //DBHelper.updateElementAndById('WorkOrder', this.state.workorder_id, 'status', 4);
            //console.log("ducdvd upate New status=" + DBHelper.getById('WorkOrder', this.state.workorder_id).status)

            this.props.navigation.navigate('Note', {wo_id: this.state.workorder_id, editing: false, Worder_status: this.state.workorder_status})

        }


    }

    /**
     * Gets all of the items within the checklist which is displayed differently depending on the type
     * and is then stored within the array to be displayed
     *
     * @return {Array} - Checklist item's views to be displayed
     */
    getItemsView_Note() {
        try {
            var item_array = []
            let items = DBHelper.getAll('WorkOrder').filtered('end_note != "" and id =' + this.state.workorder_id).sorted('sequence')
            let note_id = 0
            let note_id_workorder = 0
            let count = 0
            if (items.length > 0) {
                for (let i = 0; i < items.length; i++) {

                    let note_before = items[i].end_note


                    if (note_before != '') {
                        console.log("updateItem note_id = " + note_id)
                        item_array.push(
                            <View style={[styles.rowLayout, styles.rowStyles]} key={count} >
                                <View style={styles.left}>
                                    <Text>{note_before}</Text>
                                </View>
                                <View style={styles.right}>
                                    <Checkbox
                                        onSelect={() => this.updateItem(i)}
                                        disabled={false}
                                        disabledColor='#247fd2'
                                        selected={this.state.new_items[note_id][0]}
                                        defaultColor='white'
                                        selectedColor='#446CB3'
                                        checkboxStyle={styles.checkboxStyle}
                                    >
                                        <Text>{this.state.new_items[note_id][1]}</Text>
                                    </Checkbox>
                                </View>
                            </View>
                        )
                        note_id = note_id + 1
                        count = count + 1
                    }


                }
            }
            return item_array //Checklist items that will be display
        } catch (error) {
            //this.props.navigation.navigate('ErrorHandler', { error: error, type: 'Internal', class: NoteOpenTK.name })
        }
    }
    /**
     * Gets all of the items within the checklist which is displayed differently depending on the type
     * and is then stored within the array to be displayed
     *
     * @return {Array} - Checklist item's views to be displayed
     */
    getItemsView_NoteWorkOrder() {
        try {
            var item_array = []
            let items = DBHelper.getAll('WorkOrder').filtered('workorder_note != "" and workorder_note_sit3 == 1 and id =' + this.state.workorder_id).sorted('sequence')
            let note_id = 0
            let note_id_workorder = 0
            let count = 0
            if (items.length > 0) {
                for (let i = 0; i < items.length; i++) {


                    let note_workorder = items[i].workorder_note




                    if (note_workorder != "") {
                        console.log("updateItem note_id_workorder = " + note_id_workorder)
                        item_array.push(
                            <View style={[styles.rowLayout, styles.rowStyles]} key={count}>
                                <View style={styles.left}>
                                    <Text>{note_workorder}</Text>
                                </View>
                                <View style={styles.right}>
                                    <Checkbox
                                        onSelect={() => this.updateItem_WorkOrder(i)}
                                        disabled={false}
                                        disabledColor='#247fd2'
                                        selected={this.state.new_items_workorder[note_id_workorder][0]}
                                        defaultColor='white'
                                        selectedColor='#446CB3'
                                        checkboxStyle={styles.checkboxStyle}
                                    >
                                        <Text>{this.state.new_items_workorder[note_id_workorder][1]}</Text>
                                    </Checkbox>
                                </View>
                            </View>
                        )
                        note_id_workorder = note_id_workorder + 1
                        count = count + 1
                    }
                }
            }
            return item_array //Checklist items that will be display
        } catch (error) {
            //this.props.navigation.navigate('ErrorHandler', { error: error, type: 'Internal', class: NoteOpenTK.name })
        }
    }
    render() {
        return (
            <View behavior="padding" style={styles.container}>
                <View style={styles.content}>
                    <ScrollView style={styles.scrollContainer}>
                        {/* Wrapped in a TouchableOpacity to close the keyboard */}
                        <TouchableOpacity activeOpacity={1} onPress={() => Keyboard.dismiss()}>
                            <KeyboardAvoidingView behavior='padding'>
                                {/* All of the checklist get displayed here */}
                                <View style={styles.itemContainer}>

                                    {this.getItemsView_Note()}
                                    {this.getItemsView_NoteWorkOrder()}
                                </View>


                                {/* Confirmation Button */}
                                <View>
                                    <TouchableOpacity style={styles.startButtonContainer} onPress={() => this.submitList()}>
                                        <Text style={styles.startText}>Confirm Notes</Text>
                                    </TouchableOpacity>
                                </View>
                            </KeyboardAvoidingView>
                        </TouchableOpacity>
                    </ScrollView>
                </View >

                {/* Checklist header */}
                <Header nav={this.props.navigation} wo_id={this.state.workorder_id}/>
            </View>
        );
    }
}

//Gets the size of the mobile device (in pixel's)
const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1, //like bootstraps xs-lg-12 but without dropping down
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        alignItems: 'center',
        width: window.width,
        top: 70
    },
    scrollContainer: {
        height: window.height - 70,
    },
    rowLayout: {
        flexDirection: 'row',
        paddingBottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    rowStyles: {
        borderBottomWidth: 1,
        borderColor: '#BDC3C7',
        margin: 8,
        marginBottom: 0,
        paddingBottom: 8,
    },

    left: {
        flex: 0.5,
        alignItems: 'center',
        paddingRight: 10,
        justifyContent: 'center',
    },
    right: {
        flex: 0.5,
        alignItems: 'center',
        paddingLeft: 10,

    },

    mainText: {
        color: 'white',
        fontSize: 16,
        fontWeight: '700',
    },
    secondaryText: {
        color: 'white',
        fontSize: 14,
        fontWeight: '600',
    },

    startButtonContainer: {
        backgroundColor: '#ffa726',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        width: window.width - 60,
        borderRadius: 5,
        margin: 20,
    },
    startText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: '700',
    },


    textContainer: {
        margin: 5,
        width: (window.width / 20) * 9,

    },
    textField: {
        padding: 2,
        backgroundColor: '#ECECEC',
        borderRadius: 5,
        height: 60,
        fontSize: 15,
    },

    checkboxStyle: {
        margin: 2,
        width: 26,
        height: 26,
        borderWidth: 2,
        borderColor: '#ddd',
        borderRadius: 5
    },

    itemContainer: {
        paddingTop: 10,
    }
});
