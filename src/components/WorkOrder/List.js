import React, { Component} from 'react';
import { Animated, Platform, StyleSheet, View, Image, Text, Button, StatusBar, Dimensions, ScrollView, TouchableOpacity } from 'react-native';
import renderIf from '../../config/renderif';
import DBHelper from '../../DBHelper';
import SortableList from 'react-native-sortable-list';
import Row from './Row';

/**
 * Child Component to: WorkOrder.js
 * Parent Component to: Row.js
 *
 * Provides the WorkOrder view with a sortable list view
 * which filters by todo and completed workorders
 */
export default class List extends Component {

    constructor(props) {
        super(props)
        this.state = {
            trip_id: null,
            array_order: null,
            initial_workorders: null,
        }
        this.activeView = 1;
        this.getActiveTrip()
    }

    /**
     * Return the left tab bar color which changes if
     * the tab is active.
     *
     * @return {String}
     */
    getLeftColor() {
        if (this.activeView == 1) {
            return '#FFFFFF';
        } else {
            return '#CFCFCF';
        }
    }

    /**
     * Disables the left tab bar if active
     *
     * @return {Boolean}
     */
    getLeftClickable() {
        if (this.activeView == 1) {
            return true;
        } else {
            return false;
        }
    }    

    /**
     * Return the color of the badge.
     *
     * @return {Boolean}
     */
    getLeftBadgeColor() {
        if (this.activeView == 1) {
            return '#ffa726';
        } else {
            return '#bababa';
        }
    }

    /**
     * Return the right tab bar color which changes if
     * the tab is active.
     *
     * @return {String}
     */
    getRightColor() {
        if (this.activeView == 2) {
            return '#FFFFFF';
        } else {
            return '#CFCFCF';
        }
    }

    /**
     * Disables the right tab bar if active
     *
     * @return {Boolean}
     */
    getRightClickable() {
        if (this.activeView == 2) {
            return true;
        } else {
            return false;
        }
    }    

    /**
     * Renders a row for the sortablelist view
     */
    _renderRow = ({data, active}) => {
        return <Row nav={this.props.nav} data={data} active={active} editable={this.checkIsEditable()}/>
    }

    /**
     * Gets the current active trip from the database
     */
    getActiveTrip() {
        DB_active_trip = DBHelper.getAll('Trip').filtered('active == 1')[0]
        this.state.trip_id = DB_active_trip.id
    }

    /**
     * @returns {Array} of workorders that are used to display the sortable list.
     */
    getWorkOrder() {
        var data = []
        data = DBHelper.getAll('WorkOrder').filtered('(status != "4" AND status != "5") AND trip_id ="' + this.state.trip_id + '"').sorted('sequence')
        if (data != null) {
            let temp = []
            for (let i = 0; i < data.length; i++) {
                temp.push(data[i].id)
            }
            this.state.initial_workorders = temp
        }
        //console.log(data);
        return data
    }

    /**
     * @returns {Int} number of not complete workorders.
     */
    getNumberOfUncompleted() {
        let data = DBHelper.getAll('WorkOrder').filtered('(status != "4" AND status != "5") AND trip_id ="' + this.state.trip_id + '"').sorted('sequence')
        return data.length
    }

    /**
     * @returns {Array} of completed workorders that are used to display the sortable list.
     */
    getCompletedWorkOrder() {
        var data = []
        data = DBHelper.getAll('WorkOrder').filtered('(status = "4" OR status = "5") AND trip_id ="' + this.state.trip_id + '"')
        return data
    }

    /**
     * Changes the active tab to the selected tab.
     */
    changeTabs() {
        console.log("Function can result in performance issues")
        if (this.activeView == 1) {
            this.activeView = 2
            console.log('\nSwitching to Completed Tab')
        } else {
            this.activeView = 1
            console.log('\nSwitching to Todo Tab')
        }
        this.forceUpdate()
    }

    /**
     * Releasing a work order to rearrange them in the sortable list (When sorting the list)
     */
    releaseOrder(array) {
        //data = DBHelper.getAll('WorkOrder').filtered('status != "4" AND trip_id ="' + this.state.trip_id + '"').sorted('status')

        //let workorders = this.getWorkOrder()
        if (this.state.array_order != null) {
            let order = this.state.array_order
            let workorder_ids = this.state.initial_workorders
            console.log(workorder_ids)
            for (let i = 0; i < order.length; i++) {
                DBHelper.updateElementAndById('WorkOrder', workorder_ids[order[i]], 'sequence', (i + 1));

                DBHelper.updateElementAndById('WorkOrder', workorder_ids[order[i]], 'status', 3)
                console.log('order' + order[i]);
                //console.log(workorder_ids[i])
                console.log('Update: ' + workorder_ids[ order[i] ])
                //DBHelper.updateElementAndById('WorkOrder', workorders[order[i]].id, 'status', 3)
            }

            DBHelper.updateElementAndById('WorkOrder', workorder_ids[order[0]], 'status', 1)
            DBHelper.updateElementAndById('WorkOrder', workorder_ids[order[1]], 'status', 2)

            this.state.array_order = null
            this.forceUpdate()
        }
    }

    /**
     * saves the state of the new sequence of workorders
     *
     * @param {Array} array - the new order of work orders 
     */
    changingOrder(array) {
        this.state.array_order = array
    }

    /**
     * Determines if the trip has a workorder with the status of "Open Ticket"
     * If so it will disable the ability to Sort the list until the ticket is finished.
     */
    checkIsEditable() {
        try {
            let openTicket = DBHelper.getAll('WorkOrder').filtered('status = "-1" AND trip_id ="' + this.state.trip_id + '"')[0]
            //console.log(openTicket)
            if (openTicket != null) {
                return false
            }
            return true
        } catch (error) { }
        return true
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.buttonLeftContainer} disabled={this.getLeftClickable()} onPress={() => this.changeTabs()}>
                        <View style={styles.buttonLeft}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <View style={{justifyContent: 'center'}}>
                                    <Text style={{ color: this.getLeftColor(), fontWeight: '800', fontSize: 15 }}>
                                        To Do
                                    </Text>
                                </View>
                                <View style={{marginLeft: 5, backgroundColor: this.getLeftBadgeColor(), width: 40, height: 40, borderRadius: 20, justifyContent: 'center', alignItems: 'center'}}>
                                    <Text style={{color: '#FFFFFF', width: 35, textAlign: 'center', fontWeight: '700', fontSize: 16}}>
                                        {this.getNumberOfUncompleted()}
                                    </Text>
                                </View>

                            </View>
                            {renderIf(this.activeView == 1)(
                                <Text style={styles.bar}></Text>
                            )}
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonRightContainer} disabled={this.getRightClickable()} onPress={() => this.changeTabs()}> 
                        <View style={styles.buttonRight}>
                            <Text style={{ color: this.getRightColor(), fontWeight: '800', fontSize: 15 }}>
                                Completed
                            </Text>
                            
                            {renderIf(this.activeView == 2)(
                                <Text style={styles.bar}></Text>
                            )}
                        </View>
                    </TouchableOpacity>
                </View>
                {renderIf(this.activeView == 1)(
                    <SortableList
                        style={styles.list}
                        contentContainerStyle={styles.contentContainer}
                        data={this.getWorkOrder()}
                        onChangeOrder={(array) => this.changingOrder(array)}
                        onReleaseRow={(array) => this.releaseOrder(array)}
                        renderRow={this._renderRow}
                        sortingEnabled={this.checkIsEditable()}
                    />
                )}

                {renderIf(this.activeView == 2)(
                    <SortableList
                        style={styles.list}
                        contentContainerStyle={styles.contentContainer}
                        data={this.getCompletedWorkOrder()}
                        renderRow={this._renderRow}
                        sortingEnabled={false}
                    />
                )}
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        position: 'absolute',
        backgroundColor: '#E2E2E3',
        alignItems: 'center',
        height: window.height - (45 + 70),
        width: window.width,
        bottom: 45,
    },
    buttonContainer: {
        flexDirection: 'row',
        height: 60,
    },
    buttonLeftContainer: {
        flex: 0.5,
        backgroundColor: '#79909B',
    },
    buttonLeft: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
    },
    buttonRightContainer: {
        flex: 0.5,
        backgroundColor: '#656F78',
    },
    buttonRight: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
    },
    bar: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#F98B24',
        height: 4,
        width: window.width/2,
    },
    list: {
        flex: 1,
    },
    contentContainer: {
        width: window.width,

        ...Platform.select({
            ios: {
                paddingHorizontal: 5,
            },

            android: {
                paddingHorizontal: 0,
            }
        })
    },
});