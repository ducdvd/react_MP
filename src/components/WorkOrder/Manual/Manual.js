import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, TouchableOpacity, Geolocation, Alert} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import { CheckboxField, Checkbox } from 'react-native-checkbox-field';
import Timer from 'react-native-timer';
import renderIf from '../../../config/renderif';
import DBHelper from '../../../DBHelper';
import Header from './Header';
import Footer from '../../Footer/Footer';
import * as Progress from 'react-native-progress';
import Prompt from 'react-native-prompt';

/**
 * Parent Component
 *
 */
export default class Manual extends Component {

    constructor(props) {
        super(props)
        this.state = {
            workorder_id: props.navigation.state.params.wo_id, //Parameter passed from the previous view
            current_tank_id: props.navigation.state.params.task_id, //Parameter passed from the previous view
            status: 'scan',
            promptVisible: false,
            geolocation: null,

            //CHECKLIST
            geofence: false,
            rfid: false,
            in_work_order: false,

            //DISPENSING
            amount: 'Scanning...',
            dispensed: '0',
            no_fuel: false,
            //OBJECTS
            //-------
                //INITIAL OBJECTS
                workorder: null,
                truck_id: null,
            
                //TIMER OBJECTS
                task: null,

                //TASK OBJECTS
                fuel: null,
                compartments: null,

                //SELECTED OBJECTS
                compartment: null,
                default_compartment: [0, '1'],
            //-------
        };
        this.getWorkOrder() //Gets a workorder from the database
        //this.getCompartmentData() //Gets the compartment information from the current truck


        this.state.task = DBHelper.getById('Task', props.navigation.state.params.task_id) //Samples in task 1 form the database
        if(DBHelper.getById('Task', props.navigation.state.params.task_id).fill == -1){
            console.log(this.state.task.unit_number +':count unit fuel:' + DBHelper.getById('Unit', this.state.task.unit_number).name + '-' + DBHelper.getById('Task', props.navigation.state.params.task_id).amount)
            this.state.amount = DBHelper.getById('Unit', this.state.task.unit_number).max_fuel - DBHelper.getById('Task', props.navigation.state.params.task_id).amount //Samples in task 1 form the database

        }else{

            this.state.amount = DBHelper.getById('Task', props.navigation.state.params.task_id).fill - DBHelper.getById('Task', props.navigation.state.params.task_id).amount //Samples in task 1 form the database

        }







        //Some of these should not be called when using actual data.
        this.getCompartmentData() //Gets the compartment data related to the type of fuel
    }

    //Gets the workorder information and truck information
    getWorkOrder() {
        this.state.workorder = DBHelper.getById('WorkOrder', this.state.workorder_id)
        this.state.truck_id = DBHelper.get('Driver', 0).truck_id
    }

    /**
     * The method that gets called on each timer or thread tick.
     * Determins if the compartment and task or unit amount is <= 0
     * Determines if the compartment is <= 0
     * and if the task or unit are set to "Fill the Unit"
     */
    addFuel() {
        let fuel = this.state.dispensed
        let dispensed = fuel + 50
    }

    getFuelName() {
        if (this.state.fuel != null) {
            return this.state.fuel.name
        } else {
            return 'Scanning...'
        }
    }
    //------------------------------------------------------

    getUnitType() {
        if (this.state.task != null) {
            this.state.fuel = DBHelper.getById('Fuel', this.state.task.fuel_id)
        } else {
            this.state.fuel = null
        }
    }

    getCompartmentData() {
        var data = new Array
        if (this.state.task != null) {
            this.getUnitType()
            let fueltype = this.state.fuel
            let cps = DBHelper.getAll('Compartment').filtered('truck_id =' + this.state.truck_id + ' AND fuel_id =' + fueltype.id)
            if (cps.length != 0) {
                
                for (let i = 0; i < cps.length; i++) {
                    data.push(cps[i].sequence)
                }
                //this.state.compartment = cps[0] //set to realm object
                console.log('SETTING THE STATE!')


                this.state.compartment = cps[0]
                this.state.default_compartment = [0, '' + cps[0].sequence]
                this.state.compartments = data

                // this.setState({
                //     compartment: cps[0],
                //     default_compartment: [0, '' + cps[0].sequence],
                //     compartments: data,
                // })


                // this.state.default_compartment = [0, '' + cps[0].sequence]
                //this.state.compartments = data //ERROR
                return
            } else {
                //NO FUEL
                this.state.no_fuel = true
                alert('NO COMPARTMENTS WITH THAT FUEL TYPE')
            }
        }
        let cps = DBHelper.getAll('Compartment').filtered('truck_id = ' + this.state.truck_id)
        if (cps.length != 0) {
            for (let i = 0; i < cps.length; i++) {
                data.push(cps[i].sequence)
            }
            this.state.default_compartment = [0, '1']
        } else {
            this.props.navigation.navigate('ErrorHandler', {error: 'Something went wrong when getting compartments', type: 'Internal', class: Scanning.name})
        }
        this.state.compartments = data
        console.log('==getCompartmentData==')
        console.log(this.state.compartments)
    }
    getFuelAmount() {
        let Fuel_Amount = 0
        if(this.state.no_fuel){
            return 0;
        }else{
            //return this.state.compartment.current_amount - parseInt(this.state.dispensed)
            Fuel_Amount = this.state.compartment.current_amount - parseInt(this.state.dispensed)
            if(Fuel_Amount < 0){

                return 'Invalid Amount '
            }else{
                return Fuel_Amount
            }

        }
    }
    selectCompartment(index, value) {
        //SMART - select the first compartment that matches the type of gas
        if (this.state.compartments != null) {
            var cps = null
            if (this.state.task != null && this.state.fuel != null) {
                cps = DBHelper.getAll('Compartment').filtered('truck_id =' + this.state.truck_id + ' AND fuel_id =' + this.state.fuel.id)
            } else {
                cps = DBHelper.getAll('Compartment').filtered('truck_id = ' + this.state.truck_id)
            }
            this.state.compartment = cps[index] //set to realm object
            this.setState({
                default_compartment: [parseInt(index), '' + cps[index].sequence]
            })
        } else {
            console.warn('selection failed')
        }
    }

    /**
     * Provides an alert if you have dispensed fuel to the task or unit.
     * Double checks if the driver is finished with the task or unit.
     */
    finishedWithUnit() {
        Alert.alert(
            'Finished with Unit',
            'Are you sure your finished with this unit?',
            [
                { text: 'Yes', onPress: () => this.leaveView() },
                { text: 'No', onPress: () => console.log() }
            ],
            { cancelable: false }
        )
    }

    /**
     * @returns the maximum amount of fuel that the task or unit can handle.
     */
    getAmount() {
        if (this.state.task != null) {
            //if (this.state.task.fill == -1) {
                //return 'Fill'

             //   this.state.amount = DBHelper.get('Unit', this.state.task.unit_number).max_fuel
             //   return this.state.amount

            //}
            return this.state.amount
        }
    }

    /**
     * @returns the amount of fuel left in the compartment in a percentage
     */
    getPercentageOfFuel() {

        let PerOfFuel = 0

        if (this.state.compartment != null) {
            var percentage = 0;
            if (this.state.dispensed > 0) {
                percentage = ((this.state.compartment.current_amount - this.state.dispensed) / this.state.compartment.allowed_capacity) * 100
            } else {
                percentage = (this.state.compartment.current_amount / this.state.compartment.allowed_capacity) * 100
            }
            PerOfFuel = Math.round(percentage)
            if(PerOfFuel >= 0 ){
                return Math.round(percentage) + '%'
            }else{
                return 'Invalid Amount ' + '%'
            }

        } 
    }


    /**
     * Alerts the driver that you are current wishing to leave the
     * Scanning View.
     */
    leaveScanning() {
        // if (this.state.compartment != null && this.state.task != null) {
        //         Alert.alert(
        //             'Finished with Unit',
        //             'Are you sure your finished with this unit?',
        //             [
        //                 { text: 'Yes', onPress: () => this.props.navigation.navigate('Details', {workorder_id: this.state.workorder_id})},
        //                 { text: 'No', onPress: () => console.log() }
        //             ],
        //             { cancelable: false }
        //         )
        //         return
        // }
        this.props.navigation.navigate('Details', {workorder_id: this.state.workorder_id})
    }

    submitAmount(value) {
        //this.setState({ promptVisible: false })
        this.setState({
            dispensed: value,
            promptVisible: false,
        })


/*
        if (parseInt(value) > 0) {
            if(this.state.compartment.current_amount > parseInt(value)){
                this.setState({ promptVisible: false })
                if((parseInt(value) + this.state.task.amount) <= this.state.amount ){
                    this.setState({
                        dispensed: value,

                    })
                }else{
                    this.setState({ promptVisible: false })
                    //this.props.navigation.navigate('Manual', {wo_id: this.state.workorder_id, task_id : this.state.current_tank_id})
                    Alert.alert(
                        'Alert',
                        'Invalid Amount Value',
                        [
                            { text: 'Ok', onPress: () => console.log() },
                        ],
                        { cancelable: false }
                    )

                }

            }else{
                this.setState({ promptVisible: false })
                //this.props.navigation.navigate('Manual', {wo_id: this.state.workorder_id, task_id : this.state.current_tank_id})
                Alert.alert(
                    'Alert',
                    'Invalid Amount Value',
                    [
                        { text: 'Ok', onPress: () => console.log() },
                    ],
                    { cancelable: false }
                )


            }

        }else{

            Alert.alert(
                'Alert',
                'Number value required',
                [
                    { text: 'Ok', onPress: () => console.log() },
                ],
                { cancelable: false }
            )

        }

*/


    }

    inputAmount() {
        if(this.state.no_fuel){
            this.setState({ promptVisible: false })
        }else{
            if (this.state.compartment.current_amount == null || this.state.compartment.current_amount <= 0) {
                Alert.alert(
                    'Alert',
                    'Selected compartment is empty',
                    [
                        { text: 'Ok', onPress: () => console.log() },
                    ],
                    { cancelable: false }
                )
                return
            } else {
                this.setState({ promptVisible: true })
            }
        }

    }

    confirm() {
        if (this.state.dispensed > 0) {
            if(this.state.compartment.current_amount >= parseInt(this.state.dispensed)){

                if(parseInt(this.state.dispensed)  <= this.state.amount ){
                    DBHelper.updateElementAndById('Compartment', this.state.compartment.id, 'current_amount', this.state.compartment.current_amount - parseInt(this.state.dispensed))
                    DBHelper.updateElementAndById('Task', this.state.task.id, 'amount', parseInt(this.state.dispensed) + this.state.task.amount)
                    this.setState({
                        amount: this.state.amount - parseInt(this.state.dispensed),
                        dispensed: '0',
                    })
                    this.forceUpdate()
                    this.props.navigation.navigate('Details', {workorder_id: this.state.workorder_id,view_id: 2})


                    let tripInfo = DBHelper.getAll("Trip").filtered("active =1")[0];
                    let driver_info = DBHelper.getById('Driver',1)
                    let Inventory = [
                        DBHelper.getAll('Inventory').length + 1,
                        this.getTimeStamp(),
                        parseFloat(driver_info.id),
                        parseFloat(tripInfo.id),
                        0,
                        parseFloat(this.state.workorder_id),
                        parseFloat(this.state.workorder.location_id),
                        parseFloat(this.state.task.unit_number),
                        parseFloat(this.state.task.id),
                        0,
                        0,
                        parseFloat(driver_info.truck_id),
                        parseFloat(this.state.compartment.id),
                        0,
                        parseFloat(this.state.task.fuel_id),
                        'dispense',
                        parseInt(this.state.dispensed) //
                    ]


                    console.log("------Inventory-------")

                    console.log(Inventory)
                    console.log("------end Inventory-------")
                    DBHelper.add('Inventory', Inventory)


                }else{
                    this.props.navigation.navigate('Manual', {wo_id: this.state.workorder_id, task_id : this.state.current_tank_id})
                    Alert.alert(
                        'Alert',
                        'Invalid amount to the Unit',
                        [
                            { text: 'Ok', onPress: () => console.log() },
                        ],
                        { cancelable: false }
                    )

                }

            }else{
                this.props.navigation.navigate('Manual', {wo_id: this.state.workorder_id, task_id : this.state.current_tank_id})
                Alert.alert(
                    'Alert',
                    '"Invalid amount from the Compartment.',
                    [
                        { text: 'Ok', onPress: () => console.log() },
                    ],
                    { cancelable: false }
                )


            }

        }else{
            Alert.alert(
                'Alert',
                'Invalid amount to the Unit.',
                [
                    { text: 'Ok', onPress: () => console.log() },
                ],
                { cancelable: false }
            )

        }
    }
    /**
     * Get current time stamp
     *
     */
    getTimeStamp() {
        var now = new Date();
        return ((now.getMonth() + 1) + '/' +
            (now.getDate()) + '/' +
            now.getFullYear() + " " +
            now.getHours() + ':' +
            ((now.getMinutes() < 10)
                ? ("0" + now.getMinutes())
                : (now.getMinutes())) + ':' +
            ((now.getSeconds() < 10)
                ? ("0" + now.getSeconds())
                : (now.getSeconds())));
    }
    /**
     * Get confirmButtonColor
     *
     */
    confirmButtonColor() {
        if (this.state.dispensed > 0) {
            return 'orange'
        } else {
            return 'grey'
        }
    }
    /**
     * Get render
     *
     */
    render() {
        return (
            <View style={styles.container}>    
                <View style={styles.content}>
                    <View style={styles.topContainer}>
                        {/* UNIT INFORMATION! */}
                        <View style={styles.unitContainer}>
                            <Text style={styles.unitLabel}>Unit {this.state.task.unit_number}</Text>

                            <Text style={styles.unitFuelLabel}>{this.getFuelName()}</Text>
                            {renderIf(this.state.task.fill != -1)(
                                <Text style={styles.unitFillLabel}>{this.state.task.amount + parseInt(this.state.dispensed)} / {this.state.task.fill} L</Text>
                            )}
                            {renderIf(this.state.task.fill == -1)(
                                <Text style={styles.unitFillLabel}>Fill Unit</Text>
                            )}
                            {renderIf(this.state.task.fill != -1)(
                                <View>
                                    {/*Progress Bar*/}
                                    <Progress.Bar
                                        style={styles.amountBar}
                                        width={window.width/2 - 10}
                                        progress={(this.state.task.amount + parseInt(this.state.dispensed)) / this.state.task.fill}
                                        color={'#ffa726'}
                                        unfilledColor={'#78909c'}
                                    />
                                </View>
                            )}
                            {renderIf(this.state.task.fill == -1)(
                                <View>
                                    <Text style={styles.unitDispensedLabel}>Dispensed: {this.state.task.amount}</Text>
                                </View>
                            )}
                        </View>

                        {/* COMPARTMENT INFORMATION */}
                        <View style={styles.compartmentContainer}>
                            <Text style={[styles.compartmentLabel, {fontSize: 18, fontWeight: '600'}]}>Compartment</Text>
                            <View style={styles.dropdownContainer}>
                                <ModalDropdown
                                    style={styles.dropdown}
                                    textStyle={styles.dropdownText}
                                    dropdownStyle={styles.dropdownBox}
                                    dropdownTextStyle={styles.dropdownOption}
                                    dropdownTextHighlightStyle={styles.dropdownSelectedOption}
                                    options={this.state.compartments}
                                    defaultIndex={this.state.default_compartment[0]}
                                    defaultValue={this.state.default_compartment[1]}
                                    onSelect={(index, value) => this.selectCompartment(index, value)}
                                ><Text style={styles.dropText}>{'' + this.state.default_compartment[1]}</Text></ModalDropdown>
                            </View>
                            {/* Change to exact amount? */}
                            <Text style={[styles.compartmentLabel, {fontWeight: '600'}]}>{this.getPercentageOfFuel()}</Text> 
                            <Text style={[styles.compartmentLabel, {fontWeight: '600'}]}>{this.getFuelAmount()} L</Text>
                        </View>
                    </View>

                    {/* Bottom */}
                    <View style={styles.bottomContainer}>
                        {/* <View style={[styles.rowLayout, {marginTop: 50, marginBottom: 10}]}>
                            <TouchableOpacity style={styles.finishButton} onPress={() => this.finishedWithUnit()}>
                                <Text style={styles.finishText}>Finish Filling Unit</Text>
                            </TouchableOpacity>
                        </View>  */}
                        <View style={styles.rowLayout}>

                            <Text style={styles.amountLabel}>Amount</Text>
                            <View style={styles.amountContainer}>
                                <Text 
                                    style={styles.amountNumber}
                                    onPress={() => this.inputAmount()}
                                >
                                    {this.state.dispensed}
                                </Text>
                            </View>   
                            {/* <View style={styles.left}>
                                
                            </View>
                            <View style={styles.middle}>
                                
                            </View>
                            <View style={styles.right}>

                            </View> */}
                        </View>
                        <View style={styles.rowLayout}>
                            <Text style={styles.maximumLabel}>Maximum</Text>
                            <View style={styles.maximumContainer}>
                                <Text style={styles.maxmimumNumber}>{this.getAmount()}</Text>
                            </View>
                            {/* <View style={styles.left}>
                                
                            </View>
                            <View style={styles.right}>

                            </View> */}
                        </View>
                        {/* Confirmation Button */}
                        <View style={[styles.rowLayout, {marginTop: 20}]}>
                            <TouchableOpacity style={[styles.confirmButton, {backgroundColor: this.confirmButtonColor()}]} onPress={() => this.confirm()}>
                                <Text style={styles.confirmText}>Confirm</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <Header nav={() => this.leaveScanning()}/>
                <Footer
                    nav={this.props.navigation}
                    truck={1}
                    map={1}
                    workorder={1}
                    settings={1}
                    dispensed={this.state.dispensed}
                    compartment={this.state.compartment}
                    task={this.state.task}
                />
                <Prompt
                    title='Amount of Fuel Dispensed'
                    titleStyle={{textAlign: 'center'}}
                    placeholder=''
                    defaultValue=''
                    visible={ this.state.promptVisible }
                    onCancel={ () => this.setState({
                        promptVisible: false,
                    }) }
                    onSubmit={
                        (value) => this.submitAmount(value)
                    }
                    textInputProps={{keyboardType: 'number-pad'}}
                />
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        height: window.height - (45 + 70),
        width: window.width,
        bottom: 45,
    },
    topContainer: {
        backgroundColor: '#d9d9d9',
        height: ((window.height - (45 + 70)) / 9) * 3,
        width: window.width,
        flexDirection: 'row',
    },
    bottomContainer: {
        paddingTop: 10,
        backgroundColor: 'white',
        height: ((window.height - (45 + 70)) / 9) * 6,
        width: window.width,
        alignItems: 'center',
    },
    unitContainer: {
        flex: 0.6,
        backgroundColor: '#d9d9d9',
        height: ((window.height - (45 + 70)) / 9) * 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    compartmentContainer: {
        flex: 0.4,
        backgroundColor: '#e8e8e8',
        height: ((window.height - (45 + 70)) / 9) * 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    rowLayout: {
        flexDirection: 'row',
        paddingBottom: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    left: {
        flex: 0.5,
        alignItems: 'flex-end',
        paddingRight: 10,
        justifyContent: 'center',
    },
    right: {
        flex: 0.5,
        alignItems: 'flex-start',
        paddingLeft: 10,
    },
    amountLabel: {
        color: '#607d8b',
        fontSize: 18,
        marginRight: 18,
    },
    maximumLabel: {
        color: '#607d8b',
        fontSize: 18,
        marginRight: 5,
    },
    amountContainer: {
        backgroundColor: 'white',
        width: 200,
        height: 30,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'grey',
        justifyContent: 'center',
        alignItems: 'center',
    },
    maximumContainer: {
        backgroundColor: '#e6e6e6',
        width: 200,
        height: 30,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    amountNumber: {
        color: '#607d8b',
        fontSize: 18,
        width: 180,
        textAlign: 'center',
    },
    maxmimumNumber: {
        color: '#607d8b',
        fontSize: 18,
    },
    dropdownContainer: {
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        //width: 30,
    },
    dropText: {
        textAlign: 'center',
        padding: 10,
    },
    dropdown: {
        //Could add styles later
        width: 50,
    },
    dropdownBox: {
        height: 125,
        borderRadius: 5,
        width: 50,
        alignItems: 'center',
    },
    dropdownText: {
        fontSize: 14,
        fontWeight: '700',
        color: 'black',
        textAlign: 'center',
    },
    dropdownOption: {
        color: 'black',
        fontWeight: '700',
        fontSize: 14,
        paddingLeft: 15,
        paddingRight: 15,
    },
    dropdownSelectedOption: {
        color: 'black',
        fontWeight: '700',
        fontSize: 14,
        paddingLeft: 15,
        paddingRight: 15,
    },
    compartmentLabel: {
        color: '#607d8b',
        fontSize: 13,
    },
    unitLabel: {
        color: '#607d8b',
        fontSize: 20,
        fontWeight: '600'
    },
    unitFuelLabel: {
        color: '#607d8b',
        fontSize: 14,
        fontWeight: '500',
    },
    unitFillLabel: {
        color: '#607d8b',
        fontSize: 14,
        fontWeight: '500',
    },
    unitDispensedLabel: {
        color: '#607d8b',
        fontSize: 14,
        fontWeight: '700',
    },
    finishButton: {
        backgroundColor: '#EF4836',
        borderRadius: 5,
        borderColor: '#EC644B',
        width: 140,
        height: 25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    finishText: {
        color: 'white',
        fontSize: 14,
        fontWeight: '700',
    },
    confirmButton: {
        //backgroundColor: 'orange',
        borderRadius: 5,
        borderColor: '#EC644B',
        width: (window.width / 3) * 2,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    confirmText: {
        color: 'white',
        fontSize: 16,
        fontWeight: '700',
    }
});
