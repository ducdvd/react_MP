import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, TouchableOpacity, Geolocation, Alert, TextInput, KeyboardAvoidingView, Keyboard} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

import { CheckboxField, Checkbox } from 'react-native-checkbox-field';

import renderIf from '../../../config/renderif';
import DBHelper from '../../../DBHelper';

import Header from './Header';

/**
 * Parent Component
 *
 * Displays the previous note or allows the driver to add a note to
 * the selected Work Order.
 */
export default class Note extends Component {

    constructor(props) {
        super(props)
        this.state = {
            workorder_id: props.navigation.state.params.wo_id,
            text: '',
        };
        this.getPreviousText()
    }
    
    /**
     * Gets the previous note that was left by the driver
     */
    getPreviousText() {
        let old_note = this.state.text
        let note = DBHelper.getById('WorkOrder', this.state.workorder_id).note
        if (note != false && note != null && note != '') {
            this.state.text = note
        } else {
            this.state.text = old_note
        }
    }

    /**
     * Determines if the driver is either editing or adding a note to the workorder.
     * Also navigates accordingly if he is editing it.
     *
     * NOTE: it will go back to Details view if you are adding a note. Not when
     * you are leaving a note when completing a workorder.
     */
    updateNote() {
        DBHelper.updateElementAndById('WorkOrder', this.state.workorder_id, 'note', this.state.text)
        if (this.props.navigation.state.params.editing) {
            this.props.navigation.navigate('Details', {workorder_id: this.state.workorder_id})
        } else {


            DBHelper.updateElementAndById('WorkOrder', this.state.workorder_id, 'status', parseInt(this.props.navigation.state.params.Worder_status));
            this.props.navigation.navigate('WorkOrder')
        }
    }

    render() {
        return (
            <View behavior="padding" style={styles.container}>
                <View style={styles.content}>
                    <TouchableOpacity activeOpacity={1} onPress={() => Keyboard.dismiss()}>
                        <View>

                                <Text style={styles.startText}></Text>

                        </View>
                        <View style={styles.textContainer}>
                            <TextInput
                                style={styles.textField}
                                editable={true}
                                multiline={true}
                                onChangeText={(text) => this.setState({text: text})}
                                placeholder='note here...'
                                placeholderTextColor='#BFBFBF'
                                value={this.state.text}
                            >
                            </TextInput>
                        </View>
                        <View>
                            <TouchableOpacity style={styles.startButtonContainer} onPress={() => this.updateNote()}>
                                <Text style={styles.startText}>Confirm Note</Text>
                            </TouchableOpacity>
                        </View>

                    </TouchableOpacity>   
                </View >
                {/* Unit header */}
                <Header nav={this.props.navigation} wo_id={this.props.navigation.state.params.wo_id}/>
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        alignItems: 'center',
        width: window.width,
        top: 70
    },
    rowLayout: {
        flexDirection: 'row',
        paddingBottom: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    left: {
        flex: 0.4,
        alignItems: 'flex-end',
        paddingRight: 10,
        justifyContent: 'center',
    },
    right: {
        flex: 0.6,
        alignItems: 'flex-start',
        paddingLeft: 10,
    },
  
    mainText: {
        color: 'white',
        fontSize: 16,
        fontWeight: '700',
    },
    secondaryText: {
        color: 'white',
        fontSize: 14,
        fontWeight: '600',
    },

    startButtonContainer: {
        backgroundColor: '#ffa726',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        width: window.width - 60,
        borderRadius: 5,
        margin: 20,
    },
    startText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: '700',
    },
    textContainer: {
        margin: 0,
        //width: window.width - 40,
    },
    textField: {
        padding: 5,
        backgroundColor: '#ECECEC',
        borderRadius: 5,
        height: 150,
        fontSize: 15,
    }
});
