import React, { Component } from 'react';
import {
    Animated,
    Easing,
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    View,
    Dimensions,
    Platform,
    Button,
    TouchableOpacity
} from 'react-native';
import SortableList from 'react-native-sortable-list';

import DBHelper from '../../DBHelper';

import Icon2 from 'react-native-vector-icons/Octicons';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import renderIf from '../../config/renderif';

/**
 * Child Component to: List.js
 *
 * Provides sortablelist view with customized row's
 */
export default class Row extends Component {

    constructor(props) {
        super(props);

        this._active = new Animated.Value(0);

        this._style = {
            ...Platform.select({
                ios: {
                    transform: [{
                        scale: this._active.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 1.1],
                        }),
                    }],
                    shadowRadius: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [2, 10],
                    }),
                },

                android: {
                    transform: [{
                        scale: this._active.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 1.07],
                        }),
                    }],
                    elevation: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [2, 6],
                    }),
                },
            })
        };

        this.localData = [];
    }

    //LIBRARY SPECIFIC FUNCTION
    componentWillReceiveProps(nextProps) {
        if (this.props.active !== nextProps.active) {
            Animated.timing(this._active, {
                duration: 300,
                easing: Easing.bounce,
                toValue: Number(nextProps.active),
            }).start();
        }
    }

    /**
     * @param {Array} data - A single Work order inforamtion
     */
    setLocalData(data) {
        this.localData = data;
    }

    /**
     * @returns the title and specific styles to a given status of a work order
     */
    getStatusStyle() {
        /**
         * Array:
         *  0 - Status Box Style
         *  1 - Background Color
         *  2 - Text
         *  3 - Font Style
         */
        console.log("ducdvd status data=" )
        console.log(this.localData)

        if(this.localData.status != 4 && this.localData.status != 5){
            if (this.localData.status == -1) {
                return [styles.open, '#446CB3', 'In Progress', { fontWeight: '600', color: '#FFFFFF' }];
            } else {
                if (this.localData.status == 1 || this.localData.sequence == 1 ) {
                    //console.log("status 1");
                    return [styles.routing, '#cde673', 'Routing', { fontWeight: '600', color: '#555555' }];
                } else if (this.localData.status == 2 || this.localData.sequence == 2) {
                    //console.log("status 2");
                    return [styles.nextjob, '#ffa726', 'Next Job', { fontWeight: '600', color: '#262626' }];
                } else if (this.localData.status == 3) {
                    //console.log("status 3");
                    return [styles.waiting, '#FFFFFF', 'Waiting'];
                } else {
                    console.warn('invalid status type: ' + this.localData.status)
                    return [styles.invalid, 'red', 'Invalid'];
                }
            }



        }else{

            if (this.localData.status == 4) {
                //console.log("status 4");
                return [styles.completed, '#FFFFFF', 'Completed', { fontWeight: '600', color: '#b2d100' }];
            }else if (this.localData.status == 5) {
                //console.log("status 4");
                return [styles.completed, '#FFFFFF', 'Skipped', { fontWeight: '600', color: '#e21f29' }];
            }

        }

/*

        if ((this.localData.status == 1 || this.localData.sequence == 1 )  && this.localData.status != 4) {
            //console.log("status 1");
            return [styles.routing, '#cde673', 'Routing', { fontWeight: '600', color: '#555555' }];
        } else if ((this.localData.status == 2 || this.localData.sequence == 2 )  && this.localData.status != 4) {
            //console.log("status 2");
            return [styles.nextjob, '#ffa726', 'Next Job', { fontWeight: '600', color: '#262626' }];
        } else if (this.localData.status == 3) {
            //console.log("status 3");
            return [styles.waiting, '#FFFFFF', 'Waiting'];
        } else if (this.localData.status == 4) {
            //console.log("status 4");
            return [styles.completed, '#FFFFFF', 'Completed', { fontWeight: '600', color: '#b2d100' }];
        }else if (this.localData.status == 5) {
            //console.log("status 4");
            return [styles.completed, '#FFFFFF', 'Skiped', { fontWeight: '600', color: '#b2d100' }];
        } else if (this.localData.status == -1) {
            return [styles.open, '#446CB3', 'Ticket Open', { fontWeight: '600', color: '#FFFFFF' }];
        } else {
            console.warn('invalid status type: ' + this.localData.status)
            return [styles.invalid, 'red', 'Invalid'];
        }
*/

    }

    /**
     * @param {Int} client_id
     * @returns the name for the given client
     */
    getClient(data_WOrder) {
        let Clients;

        if(data_WOrder.vendor_id == ''){
            Clients = DBHelper.getById("Locations", data_WOrder.location_id);
        }else{

            Clients = DBHelper.getById("Vendor", data_WOrder.vendor_id);
        }

        return Clients.name
    }

    /**
     * Called when you select the row form the list
     *
     * @param {Int} id - Workorder Id
     */
    viewDetails(id) {
        this.props.nav.navigate('Details', { workorder_id: id });
        //this.props.navigation.navigate('Fueling', { cp_id: compartment_id, index: i});
        console.log("HELLO FROM BUTTON: " + id);
    }

    /**
     * Determines if you can sort or rearrange the sortablelist view.
     * (Doesn't display the button for rearranging)
     */
    renderMoveable() {
        if (this.localData.status != 4 && this.localData.status != 5 && this.props.editable) {
            return true
        }
    }

    /**
     * Adds a (R) to the title if the workorder type if "ReFuel"
     *
     * @param {String} type - type of work order
     */
    isRefinery(type) {
        if (type == 'ReFuel') {
            return '(R)'
        }
        return
    }

    render() {
        const { data, active } = this.props;
        this.setLocalData(data);

        console.log("---->ducdvd----data: " + data);

        return (
            <Animated.View style={[
                styles.row,
                this._style,
                {backgroundColor: this.getStatusStyle()[1]}
            ]}>

                <View style={{ flex: 0.9, justifyContent: 'space-between'}}>
                    <TouchableOpacity
                        onPress={() => this.viewDetails(data.id)}
                        style={{ flexDirection: 'row'}}
                    >
                        <View style={styles.companyContainer}>
                            <Text style={[styles.company, this.getStatusStyle()[3]]}>{this.isRefinery(data.type)} {this.getClient(data)}</Text>
                        </View>
                        {/*{data.company}*/}
                        <View style={styles.statusContainer}>
                            <View style={this.getStatusStyle()[0]}>
                                <Text style={[styles.status, this.getStatusStyle()[3]]}>{this.getStatusStyle()[2]}</Text>
                                {/*{data.status}*/}
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>

                {renderIf(this.renderMoveable())(
                    <View style={{ flex: 0.1, alignItems: 'center' }}>
                        <Icon name={'menu'} size={20} style={styles.icon}></Icon>
                    </View>
                )}


            </Animated.View>
        );
    }
}

const window = Dimensions.get('window');

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: 10,
        paddingLeft: 14,
        paddingRight: 2,
        //height: 60,
        flex: 1,
        marginTop: 5,
        marginBottom: 0,
        borderRadius: 4,


        ...Platform.select({
            ios: {
                width: window.width - 5 * 2,
                shadowColor: 'rgba(0,0,0,0.2)',
                shadowOpacity: 1,
                shadowOffset: {height: 2, width: 2},
                shadowRadius: 2,
            },

            android: {
                width: window.width - 5 * 2,
                elevation: 0,
                marginHorizontal: 30,
            },
        })
    },
    companyContainer: {
        flex: 0.7,
        justifyContent: 'center',
    },
    company: {
        flexWrap: 'wrap',
        color: '#607d8b',
        fontSize: 14,
    },
    statusContainer: {
        flex: 0.3,
        alignItems: 'center',
        justifyContent: 'center',
    },
    box: {
        width: 90,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: 'grey',
        borderWidth: 2,
    },
    routing: {
        width: 90,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: '#FFFFFF',
        borderWidth: 2,
    },
    nextjob: {
        backgroundColor: 'white',
        width: 90,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: '#546e7a',
        borderWidth: 2,
    },
    waiting: {
        width: 90,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: '#c2c2c2',
        borderWidth: 2,
    },
    open: {
        width: 90,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: '#c2c2c2',
        borderWidth: 2,
    },
    completed: {
        width: 90,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: '#b2d100',
        borderWidth: 2,
    },
    status: {
        textAlign: 'center',
        color: '#78909C',
        fontWeight: '600',
        fontSize: 13,
    },
    icon: {
        color: 'grey',
    }
});