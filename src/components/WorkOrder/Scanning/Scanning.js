import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, TouchableOpacity, Geolocation, Alert} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

import { CheckboxField, Checkbox } from 'react-native-checkbox-field';

import Timer from 'react-native-timer';

import renderIf from '../../../config/renderif';
import DBHelper from '../../../DBHelper';

import Header from './Header';
import Footer from '../../Footer/Footer';

/**
 * Parent Component
 *
 * Displays the scanning screen that allows the driver to add
 * fuel to a task or unit.
 */
export default class Scanning extends Component {

    constructor(props) {
        super(props)
        this.state = {
            workorder_id: props.navigation.state.params.wo_id, //Parameter passed from the previous view

            status: 'scan',

            geolocation: null,

            //CHECKLIST
            geofence: false,
            rfid: false,
            in_work_order: false,

            //DISPENSING
            amount: 'Scanning...',
            dispensed: 0,

            //OBJECTS
            //-------
                //INITIAL OBJECTS
                workorder: null,
                truck_id: null,
            
                //TIMER OBJECTS
                task: null,

                //TASK OBJECTS
                fuel: null,
                compartments: null,

                //SELECTED OBJECTS
                compartment: null,
                default_compartment: [0, '1'],
            //-------
        };
        this.getWorkOrder() //Gets a workorder from the database
        this.getCompartmentData() //Gets the compartment information from the current truck

        //START SAMPLING!
        this.openSampleTimer() //Open a thread that inputs sample data into the application.
    }

    //-------------------------------------------------------
    //----------------------SAMPLE DATA----------------------

    //Presents a event that would happen when a rfid is scanned
    openSampleTimer() {
        Timer.setInterval(
            'sample',
            () => this.getLocation(),
            2000
        )
    }

    /**
     * Closes the timer or thread for sampling in data.
     */
    closeSampleTimer() {
        Timer.clearInterval('sample');
    }

    /**
     * Gets the current location and determines if the location is within
     * the geofence of the workorder. Alos it inputs the sample data into the
     * application for scanning to "Work"
     */
    getLocation() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                let ingeo = false
                let radius = 20
                if (this.state.workorder != null) {
                    let cords = this.state.workorder.coordinate.split(',');

                    //Use this for actual locations
                    // let longdiff = Math.abs(position.coords.longitude - cords[0])
                    // let latdiff = Math.abs(position.coords.latitude - cords[1])

                    //Use for Office
                    let longdiff = position.coords.longitude - (-122.406417)
                    let latdiff = position.coords.latitude - (37.785834)

                    //Use for Showing
                    // let longdiff = 19
                    // let latdiff = 19
                    //console.log('diff: ' + longdiff + ' : ' + latdiff)
                    console.log("cords: " + position.coords.longitude + ' : ' + position.coords.latitude);
                    if (longdiff < radius && latdiff < radius) {
                        ingeo = true
                    }
                }
                this.setState({
                    geofence: ingeo,
                    geolocation: position.coords,

                    //SAMPLE PART
                    rfid: true, //Sets the rfid checklist to true
                    in_work_order: true, //Sets the in_work_order checklist to true

                    task: DBHelper.get('Task', 1), //Samples in task 1 form the database
                    amount: DBHelper.get('Task', 1).fill - DBHelper.get('Task', 1).amount, //Samples in task 1 form the database
                    //fuel type will be found in getCompartmentData()
                })

                //Some of these should not be called when using actual data.
                this.getCompartmentData() //Gets the compartment data related to the type of fuel
                this.checkStatus() //Checks the status of the button to set it to the correct Button type
                this.closeSampleTimer() //Closes the timer or thread that sample in the data
                this.checkAmounts() //Checks that the amount of fuel in the task or unit is not already full.
            },
            function (error) {
                this.props.navigation.navigate('ErrorHandler', {error: error, type: 'Timer', class: Scanning.name})
            },
            {
                enableHighAccuracy: true
            }
        );
    }

    /**
     * Check if the task or unit is already filled.
     */
    checkAmounts() {
        if (this.state.task != null) {
            if (this.state.task.fill != -1) {
                let diff = this.state.task.fill - this.state.task.amount
                if (diff == 0) {
                    Alert.alert(
                        'Filled',
                        'The scanned unit is already finished',
                        [
                            //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                            //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                            { text: 'Ok', onPress: () => [this.clearUnit(), this.openSampleTimer()]},
                        ],
                        { cancelable: false }
                    )
                }
            }
        }
    }

    //-------------------------------------------------------


    //INITIAL
    //Gets the workorder information and truck information
    getWorkOrder() {
        this.state.workorder = DBHelper.getById('WorkOrder', this.state.workorder_id)
        this.state.truck_id = DBHelper.get('Driver', 0).truck_id
    }

    //DETERMINE STATUS
    getStatus() {
        if (this.state.status == 'scan') {
            return [{ color: 'white' }, { backgroundColor: '#4183D7' }, 'SCANNING'];
        } else if (this.state.status == 'start') {
            return [{ color: '#3b3b3b' }, { backgroundColor: '#ffc547' }, 'START'];
        } else if (this.state.status == 'stop') {
            return [{ color: '#3b3b3b' }, { backgroundColor: '#ff9800' }, 'STOP'];
        }
        return null;
    }

    //DETERMINE STATUS BUTTON TYPE
    /**
     * @returns the method needed to be called when the given button is pressed.
     */
    getStatusButtonMethod() { //no state inside
        if (this.state.status == 'scan') {
            this.checkStatus()
        } else if (this.state.status == 'start') {
            this.startFueling()
        } else if (this.state.status == 'stop') {
            this.stopFueling()
        }
    }

    //CHECKS THE CURRENT CHECKLIST TO SEE IF ITS ALL TRUE
    checkStatus() { // has states
        if (this.state.geofence && this.state.rfid && this.state.in_work_order) {
            if (this.state.status != 'start' && this.state.status != 'stop' ) {
                this.setState({
                    status: 'start'
                })
            }
        } else {
            if (this.state.status == 'stop') {
                this.stopFueling()
            }
            this.setState({
                status: 'scan'
            })
        }
    }

    /**
     * Start the timer or thread that continuously adds fuel into
     * the dispensed part of the view.
     */
    startFueling() {
        if (this.state.compartment != null) {
            if (this.state.compartment.current_amount <= 0) {
                Alert.alert(
                    'No Fuel',
                    'There is no fuel in Compartment: ' + this.state.compartment.sequence,
                    [
                        { text: 'Ok', onPress: () => console.log() }
                    ],
                    { cancelable: false }
                )
                Timer.clearInterval('add');
            } else {
                this.setState({
                    status: 'stop'
                })
                Timer.setInterval(
                    'add',
                    () => this.addFuel(),
                    100
                )
            }
        } else {
            console.warn('compartment == null')
            //no compartment selected
        }
    }

    /**
     * Stop the timer or thread that adds fuel to the dispensed part of the view.
     */
    stopFueling() {
        // if (this.state.compartment != null && this.state.dispensed > 0) {
        //     let compartment = this.state.compartment
        //     DBHelper.updateElementAndById('Compartment', compartment.id, 'current_amount', (compartment.current_amount - this.state.dispensed))
        //     DBHelper.updateElementAndById('Task', this.state.task.id, 'amount', (this.state.task.amount + this.state.dispensed))
        // }
        Timer.clearInterval('add');
        this.setState({
            status: 'start',
        })
    }

    /**
     * The method that gets called on each timer or thread tick.
     * Determins if the compartment and task or unit amount is <= 0
     * Determines if the compartment is <= 0
     * and if the task or unit are set to "Fill the Unit"
     */
    addFuel() {
        let fuel = this.state.dispensed
        let dispensed = fuel + 50
        if (this.state.compartment != null) {

            this.setState({
                dispensed: dispensed
            })
            //RARE BUT POSSIBLE
            //Check for compartment and task or unit amount is 0
            if (this.state.compartment.current_amount <= dispensed && (this.state.task.fill != -1 && this.state.amount <= dispensed) ) {
                //BOTH COMPARTMENT AND AMOUNT ARE 0 (ZERO)
                Timer.clearInterval('add');
                
                DBHelper.updateElementAndById('Compartment', this.state.compartment.id, 'current_amount', 0)
                DBHelper.updateElementAndById('Task', this.state.task.id, 'amount', this.state.task.fill)

                Alert.alert(
                    'Completed Task',
                    'You have completed the task and' + ' Compartment: ' + this.state.compartment.sequence + ' has ran out of fuel',
                    [
                        { text: 'Ok', onPress: () => console.log() }
                    ],
                    { cancelable: false }
                )
                this.clearUnit()
            //Check for compartment is 0
            } else if (this.state.compartment.current_amount <= dispensed) {
                Timer.clearInterval('add');

                DBHelper.updateElementAndById('Compartment', this.state.compartment.id, 'current_amount', 0)
                DBHelper.updateElementAndById('Task', this.state.task.id, 'amount', (this.state.task.amount + this.state.dispensed))
                Alert.alert(
                    'Out Of Fuel',
                    'Compartment: ' + this.state.compartment.sequence + ' has ran out of fuel',
                    [
                        { text: 'Ok', onPress: () => console.log() }
                    ],
                    { cancelable: false }
                )
                this.setState({
                    dispensed: 0,
                    amount: this.state.task.fill - this.state.task.amount,
                    status: 'start',
                })

                
            } else {
                //If the task and unit is not "Fill the Unit" it will check if the amount in the task or unit is finished (amount == maximum)
                if (this.state.task.fill != -1) {
                    if (this.state.amount <= dispensed) {
                        Timer.clearInterval('add');

                        DBHelper.updateElementAndById('Compartment', this.state.compartment.id, 'current_amount', (this.state.compartment.current_amount - dispensed))
                        DBHelper.updateElementAndById('Task', this.state.task.id, 'amount', this.state.task.fill)

                        Alert.alert(
                            'Completed Task',
                            'You have completed the task',
                            [
                                { text: 'Ok', onPress: () => console.log() }
                            ],
                            { cancelable: false }
                        )
                        this.setState({
                            status: 'start',
                        })
                        this.clearUnit()
                        this.openSampleTimer()
                    }
                }
            }
        } else {
            //no compartment selected
        }
    }

    //----------------- GENERAL METHODS --------------------
    //------------------------------------------------------
    getTextColor(state) {
        if (state) {
            return 'white'
        }
        return '#607d8b'
    }

    getFuelName() {
        if (this.state.fuel != null) {
            return this.state.fuel.name
        } else {
            return 'Scanning...'
        }
    }
    //------------------------------------------------------

    getUnitType() {
        if (this.state.task != null) {
            this.state.fuel = DBHelper.getById('Fuel', this.state.task.fuel_id)
        } else {
            this.state.fuel = null
        }
    }

    getCompartmentData() {
        var data = new Array
        if (this.state.task != null) {
            this.getUnitType()
            let fueltype = this.state.fuel
            let cps = DBHelper.getAll('Compartment').filtered('truck_id =' + this.state.truck_id + ' AND fuel_id =' + fueltype.id)
            if (cps.length != 0) {
                
                for (let i = 0; i < cps.length; i++) {
                    data.push(cps[i].sequence)
                }
                //this.state.compartment = cps[0] //set to realm object
                console.log('SETTING THE STATE!')
                this.setState({
                    compartment: cps[0],
                    default_compartment: [0, '' + cps[0].sequence],
                    compartments: data,
                })
                // this.state.default_compartment = [0, '' + cps[0].sequence]
                //this.state.compartments = data //ERROR
                return
            } else {
                //NO FUEL
                //alert('NO COMPARTMENTS WITH THAT FUEL TYPE')
                this.clearUnit()
            }
        }
        let cps = DBHelper.getAll('Compartment').filtered('truck_id = ' + this.state.truck_id)
        if (cps.length != 0) {
            for (let i = 0; i < cps.length; i++) {
                data.push(cps[i].sequence)
            }
            this.state.default_compartment = [0, '1']
        } else {
            this.props.navigation.navigate('ErrorHandler', {error: 'Something went wrong when getting compartments', type: 'Internal', class: Scanning.name})
        }
        this.state.compartments = data
    }

    selectCompartment(index, value) {
        //SMART - select the first compartment that matches the type of gas
        if (this.state.compartments != null) {
            var cps = null
            if (this.state.task != null && this.state.fuel != null) {
                cps = DBHelper.getAll('Compartment').filtered('truck_id =' + this.state.truck_id + ' AND fuel_id =' + this.state.fuel.id)
            } else {
                cps = DBHelper.getAll('Compartment').filtered('truck_id = ' + this.state.truck_id)
            }
            this.state.compartment = cps[index] //set to realm object
            this.setState({
                default_compartment: [parseInt(index), '' + cps[index].sequence]
            })
        } else {
            console.warn('selection failed')
        }
    }

    /**
     * Clears scanning information to sample in another task or unit (new RFID)
     */
    clearUnit() {
        Timer.clearInterval('add');
        this.setState({
            rfid: false,
            in_work_order: false,
            status: 'scan',
            compartments: null,
            fuel: null,
            task: null,
            amount: 'Scanning...',
            dispensed: 0,
        })
        this.forceUpdate()
    }

    /**
     * @returns true your not allowed to switch to a different unit
     */
    canSwitchUnits() {
        if (this.state.status == 'stop' || this.state.status == 'start') {
            return true
        } else {
            return false
        }
    }

    /**
     * Provides an alert if you have dispensed fuel to the task or unit.
     * Double checks if the driver is finished with the task or unit.
     */
    finishedWithUnit() {
        if (this.state.dispensed > 0) {
            Alert.alert(
                'Finished with Unit',
                'Are you sure your finished with this unit?',
                [
                    { text: 'Yes', onPress: () => this.switchUnits() },
                    { text: 'No', onPress: () => console.log() }
                ],
                { cancelable: false }
            )
        } else {
            this.openSampleTimer()
            this.clearUnit()
        }
    }

    /**
     * Clears the sample data and starts up the timer or thread for sampling in
     * a unit.
     */
    switchUnits() {
        let compartment = this.state.compartment
        DBHelper.updateElementAndById('Compartment', compartment.id, 'current_amount', (compartment.current_amount - this.state.dispensed))
        DBHelper.updateElementAndById('Task', this.state.task.id, 'amount', (this.state.task.amount + this.state.dispensed))
        Timer.clearInterval('add');
        this.clearUnit()
        this.openSampleTimer()
    }

    /**
     * @returns the maximum amount of fuel that the task or unit can handle.
     */
    getAmount() {
        if (this.state.task != null && this.state.status != 'scan') {
            if (this.state.task.fill == -1) {
                return 'Fill'
            }
            return this.state.amount
        } else {
            return 'Scanning...'
        }
    }

    /**
     * @returns true if the compartment if being used.
     */
    disableDropDrown() {
        if (this.state.status == 'stop' || this.state.dispensed > 0) {
            return true
        }
        return false
    }

    /**
     * @returns the amount of fuel left in the compartment in a percentage
     */
    getPercentageOfFuel() {
        if (this.state.compartment != null) {
            var percentage = 0;
            if (this.state.dispensed > 0) {
                percentage = ((this.state.compartment.current_amount - this.state.dispensed) / this.state.compartment.allowed_capacity) * 100
            } else {
                percentage = (this.state.compartment.current_amount / this.state.compartment.allowed_capacity) * 100
            }    
            return Math.round(percentage) + '%'
        } 
    }

    /**
     * Alerts the driver that you are current wishing to leave the
     * Scanning View.
     */
    leaveScanning() {
        if (this.state.compartment != null && this.state.task != null) {
            if (this.state.dispensed > 0) {
                Alert.alert(
                    'Finished with Unit',
                    'Are you sure your finished with this unit?',
                    [
                        { text: 'Yes', onPress: () => this.leaveView() },
                        { text: 'No', onPress: () => console.log() }
                    ],
                    { cancelable: false }
                )
                return
            }
        }
        this.props.navigation.navigate('Details', {workorder_id: this.state.workorder_id})
    }

    /**
     * Updates the database with the new dispensed amount before leaving the Scanning View.
     */
    leaveView() {
        let compartment = this.state.compartment
        let task = this.state.task
        DBHelper.updateElementAndById('Compartment', compartment.id, 'current_amount', (compartment.current_amount - this.state.dispensed))
        DBHelper.updateElementAndById('Task', task.id, 'amount', (task.amount + this.state.dispensed))
        Timer.clearInterval('add')
        this.props.navigation.navigate('Details', {workorder_id: this.state.workorder_id})
    }

    render() {
        return (
            <View style={styles.container}>    
                <View style={styles.content}>
                    <View style={styles.topContainer}>
                        <View style={styles.checkContainer}>
                            <Checkbox
                                onSelect={() => {}}
                                disabled={false}
                                disabledColor='#247fd2'
                                selected={this.state.geofence}
                                defaultColor='white'
                                selectedColor='#26A65B'
                                checkboxStyle={styles.checkboxStyle}
                                >
                                <Text style={{ color: this.getTextColor(this.state.geofence)}}>Geofence</Text>
                            </Checkbox> 
                            <Checkbox
                                onSelect={() => {}}
                                disabled={false}
                                disabledColor='#247fd2'
                                selected={this.state.rfid}
                                defaultColor='white'
                                selectedColor='#26A65B'
                                checkboxStyle={styles.checkboxStyle}
                                >
                                <Text style={{ color: this.getTextColor(this.state.rfid)}}>RFID</Text>
                            </Checkbox> 
                            <Checkbox
                                onSelect={() => {}}
                                disabled={false}
                                disabledColor='#247fd2'
                                selected={this.state.in_work_order}
                                defaultColor='white'
                                selectedColor='#26A65B'
                                checkboxStyle={styles.checkboxStyle}
                                >
                                <Text style={{ color: this.getTextColor(this.state.in_work_order) }}>In Work Order</Text>
                            </Checkbox>  
                        </View>
                        <View style={styles.compartmentContainer}>
                            <Text style={[styles.compartmentLabel, {fontSize: 18, fontWeight: '600'}]}>Unit Fuel Type:</Text>
                            <Text style={[styles.compartmentLabel]}>{this.getFuelName()}</Text> 


                            <Text style={[styles.compartmentLabel, {fontSize: 18, fontWeight: '600'}]}>Compartment</Text>
                            <View style={styles.dropdownContainer}>
                                <ModalDropdown
                                    disabled={this.disableDropDrown()}    
                                    style={styles.dropdown}
                                    textStyle={styles.dropdownText}
                                    dropdownStyle={styles.dropdownBox}
                                    dropdownTextStyle={styles.dropdownOption}
                                    dropdownTextHighlightStyle={styles.dropdownSelectedOption}
                                    options={this.state.compartments}
                                    defaultIndex={this.state.default_compartment[0]}
                                    defaultValue={this.state.default_compartment[1]}
                                    onSelect={(index, value) => this.selectCompartment(index, value)}
                                ><Text style={styles.dropText}>{'' + this.state.default_compartment[1]}</Text></ModalDropdown>
                            </View>
                            <Text style={[styles.compartmentLabel, {fontWeight: '600'}]}>{this.getPercentageOfFuel()}</Text>

                        </View>
                    </View>
                    <View style={styles.middleContainer}>
                        <TouchableOpacity style={[styles.buttonContainer, this.getStatus()[1]]} onPress={() => this.getStatusButtonMethod()}>
                            <Text style={[styles.buttonText, this.getStatus()[0]]}>{this.getStatus()[2]}</Text>
                        </TouchableOpacity>
                    </View>
                    {renderIf(this.canSwitchUnits()) (
                        <View style={styles.rowLayout}>
                            <TouchableOpacity style={styles.finishButton} onPress={() => this.finishedWithUnit()}>
                                <Text style={styles.finishText}>Finish Filling Unit</Text>
                            </TouchableOpacity>
                        </View> 
                    )}
                    <View style={styles.bottomContainer}>
                        <View style={styles.rowLayout}>
                            <View style={styles.left}>
                                <Text style={styles.amountLabel}>Amount</Text>
                            </View>
                            <View style={styles.right}>
                                <View style={styles.amountContainer}>
                                    <Text style={styles.amountNumber}>{this.state.dispensed}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.rowLayout}>
                            <View style={styles.left}>
                                <Text style={styles.maximumLabel}>Maximum</Text>
                            </View>
                            <View style={styles.right}>
                                <View style={styles.maximumContainer}>
                                    <Text style={styles.maxmimumNumber}>{this.getAmount()}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <Header nav={() => this.leaveScanning()}/>
                <Footer
                    nav={this.props.navigation}
                    truck={1}
                    map={1}
                    workorder={1}
                    settings={1}
                    dispensed={this.state.dispensed}
                    compartment={this.state.compartment}
                    task={this.state.task}
                />
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        height: window.height - (45 + 70),
        width: window.width,
        bottom: 45,
    },
    topContainer: {
        backgroundColor: '#d9d9d9',
        height: ((window.height - (45 + 70)) / 9) * 3,
        width: window.width,
        flexDirection: 'row',
    },
    middleContainer: {
        //backgroundColor: '#ffc547',
        height: ((window.height - (45 + 70)) / 8) * 3,
        width: window.width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonContainer: {
        width: window.width / 2.5,
        height: window.width / 2.5,
        borderRadius: window.width / 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        fontSize: window.width / 14,
        fontWeight: '600',
    },
    bottomContainer: {
        backgroundColor: 'white',
        height: ((window.height - (45 + 70)) / 8) * 2,
        width: window.width,
        alignItems: 'center',
    },
    checkContainer: {
        flex: 0.6,
        backgroundColor: '#d9d9d9',
        height: ((window.height - (45 + 70)) / 9) * 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    compartmentContainer: {
        flex: 0.4,
        backgroundColor: '#e8e8e8',
        height: ((window.height - (45 + 70)) / 9) * 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    rowLayout: {
        flexDirection: 'row',
        paddingBottom: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    left: {
        flex: 0.4,
        alignItems: 'flex-end',
        paddingRight: 10,
        justifyContent: 'center',
    },
    right: {
        flex: 0.6,
        alignItems: 'flex-start',
        paddingLeft: 10,
    },
    amountLabel: {
        color: '#607d8b',
        fontSize: 18,
    },
    maximumLabel: {
        color: '#607d8b',
        fontSize: 18,
    },
    amountContainer: {
        backgroundColor: '#e6e6e6',
        width: 130,
        height: 30,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    maximumContainer: {
        backgroundColor: '#e6e6e6',
        width: 130,
        height: 30,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    amountNumber: {
        color: '#607d8b',
        fontSize: 18,
    },
    maxmimumNumber: {
        color: '#607d8b',
        fontSize: 18,
    },
    dropdownContainer: {
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        //width: 30,
    },
    dropText: {
        textAlign: 'center',
        padding: 10,
    },
    dropdown: {
        //Could add styles later
        width: 50,
    },
    dropdownBox: {
        height: 125,
        borderRadius: 5,
        width: 50,
        alignItems: 'center',
    },
    dropdownText: {
        fontSize: 14,
        fontWeight: '700',
        color: 'black',
        textAlign: 'center',
    },
    dropdownOption: {
        color: 'black',
        fontWeight: '700',
        fontSize: 14,
        paddingLeft: 15,
        paddingRight: 15,
    },
    dropdownSelectedOption: {
        color: 'black',
        fontWeight: '700',
        fontSize: 14,
        paddingLeft: 15,
        paddingRight: 15,
    },
    compartmentLabel: {
        color: '#607d8b',
        fontSize: 13,
    },
    checkboxStyle: {
        margin: 2,
        width: 110,
        height: 26,
        borderWidth: 2,
        borderColor: '#ddd',
        borderRadius: 5
    },
    finishButton: {
        backgroundColor: '#EF4836',
        borderRadius: 5,
        borderColor: '#EC644B',
        width: 140,
        height: 25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    finishText: {
        color: 'white',
        fontSize: 14,
        fontWeight: '700',
    }
});
