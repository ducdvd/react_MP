import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions } from 'react-native';


/**
 * Child Component to: Signature.js
 *
 * Custom header suited to the Signature View
 */
export default class Header extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Signature</Text>
                <Text style={styles.bar}></Text>
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        position: 'absolute',
        backgroundColor: '#5F646D',
        alignItems: 'center',
        flex: 1,
        height: 55,
        bottom: window.height - 55,
        width: window.width,
        justifyContent: 'center',
    },
    text: {
        color: '#FFF',
        paddingTop: 10,
        fontWeight: '800',
        fontSize: 17,
    },
    bar: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#575C64',
        height: 0,
        width: window.width,
    }
});
