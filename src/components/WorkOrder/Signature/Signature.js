import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, TouchableOpacity, Geolocation} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

import SignaturePad from 'react-native-signature-pad';
import Timer from 'react-native-timer';

import renderIf from '../../../config/renderif';
import DBHelper from '../../../DBHelper';

import Header from './Header';

/**
 * Parent Component
 *
 * Allows the driver to recieve a signature.
 */
export default class Signature extends Component {

    constructor(props) {
        super(props)
        this.state = {
            base64: null,
            reset: false,
            timerOn: false,
        };
    }

    /**
     * There has been an error with the signature pad.
     */
    _signaturePadError = (error) => {
        console.error(error);
    };

    /**
     * Detect a change in the signature pad and updates the base64
     * version of the signature to the state
     */
    _signaturePadChange = ({base64DataUrl}) => {
        //console.log("Got new signature: " + base64DataUrl);
        //console.log('testing amount')
        this.setState({
            base64: base64DataUrl
        })
    };

    /**
     * Start a timer or thread that will clear the signature pad of
     * the signature.
     */
    resetSignature() {
        this.openTimer()
    }

    /**
     * Returns the signature view to the render() method.
     */
    getSignaturePad() {
        if (this.state.reset) {
            //this.state.reset = false
            //this.forceUpdate()
            console.log('reset')
            return (
                <View style={styles.resetContainer}>
                    <Text>Clearing...</Text>
                </View>
            )
        } else {
            console.log('signature')
            return (
                <SignaturePad
                    onError={this._signaturePadError}
                    onChange={this._signaturePadChange}
                    //style={{flex: 1, backgroundColor: 'white'}}    
                    style={styles.signaturePad}
                />
            )
        }
    }

    /**
     * Clears the signature form the signaturepad.
     */
    clearView() {
        console.log('tick: ' + this.state.timeOn)
        if (this.state.timerOn) {
            this.setState({
                reset: false,
                timerOn: false,
            })
            this.closeTimer()
        } else {
            this.setState({
                reset: true,
                timerOn: true,
            })
        }

    }

    /**
     * Open the timer or thread for clearing the signaturepad.
     */
    openTimer() {
        Timer.setInterval(
            'reset',
            () => this.clearView(),
            100
        )
    }

    /**
     * Closes the tiemr or thread for clearing the signaturepad.
     */
    closeTimer() {
        Timer.clearInterval('reset');
    } 

    /**
     * Navigates back to the Details view.
     */
    cancel() {
        this.props.navigation.navigate('Details', {workorder_id: this.props.navigation.state.params.workorder_id})
    }

    /**
     * Adds the signature to the document table. If there was a
     * signature previously added it will update that given row within the database.
     */
    add() {
        if (this.props.navigation.state.params.id != null) {
            let id = this.props.navigation.state.params.id
            if(this.state.base64 == null){
                this.state.base64 = '';
            }
            DBHelper.updateElementAndById('Documents', id, 'data', this.state.base64)
        } else {
            let workorder_id = this.props.navigation.state.params.workorder_id

            let table_length = DBHelper.getAll('Documents').length
            console.log('table length: ' + table_length)
            if(this.state.base64 == null){
                this.state.base64 = '';
            }
            let doc = [table_length, workorder_id, 0, 'signature', this.state.base64, '',1]
            DBHelper.add('Documents', doc)
        }
        this.props.navigation.navigate('Details', {workorder_id: this.props.navigation.state.params.workorder_id})
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <View style={styles.rowLayout}>
                        <View style={styles.column}>
                            <TouchableOpacity onPress={() => this.cancel()}>  
                                <Text style={styles.columnText}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.column}>
                            <TouchableOpacity onPress={() => this.resetSignature()}>  
                                <Text style={styles.columnText}>Clear</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.column}>
                            <TouchableOpacity onPress={() => this.add()}>  
                                <Text style={styles.columnText}>Add</Text>
                            </TouchableOpacity>
                        </View>
                    </View>    
                    {this.getSignaturePad()}
                </View>
                <Header />
            </View>
        );
    }
}

const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        height: window.height - (55),
        width: window.width,
        top: 55,
    },
    signaturePad: {
        width: window.width,
        height: window.height - 70
    },
    rowLayout: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#575C64',
        height: 30,
    },
    columnText: {
        padding: 10,
        color: 'white',
        fontWeight: '700',
    },
    column: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
    },
    bigColumn: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
    },
    resetContainer: {
        width: window.width,
        height: window.height - 70,
        justifyContent: 'center',
        alignItems: 'center',
    }
});
