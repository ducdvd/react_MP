import React, { Component}  from 'react';
import { StyleSheet, View, Image, Text, Button, Dimensions, TouchableOpacity, Geolocation, Alert, TextInput, KeyboardAvoidingView, ScrollView, Keyboard } from 'react-native';

import ModalDropdown from 'react-native-modal-dropdown';
import { CheckboxField, Checkbox } from 'react-native-checkbox-field';
import renderIf from '../../../config/renderif';
import DBHelper from '../../../DBHelper';
import Header from './Header';



export default class Unit extends Component {

    /**
     * Defines the state and calls certain function before renderingthe view.
     *
     * @param {object} props - Contains navigation methods and information about previous component
     */
    constructor(props) {
        super(props)

        this.state = { //when component view is rendered use setState({})
            workorder_id: '' + props.navigation.state.params.wo_id, //from previous view
            
            //DB Object
            workorder: null,

            //form items
            name: '',
            number: '',
            client: null,
            client_id: null,
            location: '',
            location_id: '',
            maximum: '',
            minimum: '',
            type: null,
            fuel_type: null,

        // -- WORKORDER INFORMATION -- //
            // workorder: '', //adding a unit to a specific workorder, so this is disabled.
            amount: '',
            fill: false,

            rfid: '6D2kD23D53D'
        };
        this.getWorkOrder();
        console.log("wo_id Unit = " + this.props.navigation.state.params.wo_id);
    }

    getWorkOrder() {
        this.state.workorder = DBHelper.getById('WorkOrder', this.state.workorder_id);
        let Clients;

        if(this.state.workorder.vendor_id == ''){
            Clients = DBHelper.getById("Locations", this.state.workorder.location_id);
            this.state.location_id = this.state.workorder.location_id
            this.state.client_id = Clients.client_id
        }else{

            Clients = DBHelper.getById("Vendor", this.state.workorder.vendor_id);
            this.state.location_id = this.state.workorder.vendor_id
            this.state.client_id = this.state.workorder.vendor_id
        }
        this.state.location = Clients.address

    }

    getClient() {
        let Clients;

        if(this.state.workorder.vendor_id == ''){
            Clients = DBHelper.getById("Locations", this.state.workorder.location_id);
        }else{

            Clients = DBHelper.getById("Vendor", this.state.workorder.vendor_id);
        }


        this.state.client = Clients
        return [Clients.id, Clients.name]
    }

    getAllClients() {
        client_list = []
        clients = DBHelper.getAll('Clients')
        for (let i = 0; i < clients.length; i++) {
            client_list.push(clients[i].name)
        }
        
        return client_list;
    }

    /**
     * Called when a compartment is selected from compartment dropdown.
     * Updatest the selected compartment within the state.
     *
     * @param {Int} index 
     * @param {String} value 
     */
    selectClient(index, value) {
        let client = DBHelper.get('Clients', index) //done
        this.setState({
             client: client
        })
    }

    getUnitType() {
//UNCOMMENT WHEN DUMMY API IS UPDATED!
        // unit_list = []
        // unit_types = DBHelper.getAll('Unit_Types')
        // for (let i = 0; i < unit_types.length; i++) {
        //     unit_list.push(unit_types[i].name)
        // }
        // return unit_list;
        
//REMOVE WHEN DUMMY API IS UPDATED
        return ['Truck', 'Container']
    }

    /**
     * Called when a compartment is selected from compartment dropdown.
     * Updatest the selected compartment within the state.
     *
     * @param {Int} index 
     * @param {String} value 
     */
    selectUnitType(index, value) {
//UNCOMMENT WHEN DUMMY API IS UPDATED
        // let unit_type = DBHelper.get('Unit_Types', index) //done
        // this.setState({
        //      type: unit_type
        // })

//REMOVE WHEN DUMMY API IS UPDATED
        this.setState({
            type: index,
        })
    }

    getFuelType() {
        fuel_list = []
        fuel_types = DBHelper.getAll('Fuel')
        for (let i = 0; i < fuel_types.length; i++) {
            fuel_list.push(fuel_types[i].name)
        }
        return fuel_list;
    }

    /**
     * Called when a compartment is selected from compartment dropdown.
     * Updatest the selected compartment within the state.
     *
     * @param {Int} index 
     * @param {String} value 
     */
    selectFuelType(index, value) {
        let fuel = DBHelper.get('Fuel', index) //done
        this.setState({
             fuel_type: fuel
        })
    }

    fillUnit() {
        if (this.state.fill) {
            return [false, styles.disabledField]
        } else {
            return [true, ]
        }
    }

    clickedCheckBox() {
        if (this.state.fill) {
            this.setState({
                fill: false
            })
        } else {
            this.setState({
                fill: true
            })
        }
    }

    getCheckBoxText() {
        if (this.state.fill) {
            return 'Y'
        } else {
            return 'N'
        }
    }

    addUnit() {
        if (!this.checkInputList()) {
            // id: 'int',
            // name: 'string',
            // location: 'string',
            // maximum: 'int',
            // minimum: 'int',
            // type: 'int',
            // rfid: 'string'


            let unit = [
                DBHelper.getAll('Unit').length + 1,
                this.state.client_id.toString(),
                this.state.location_id.toString(),
                this.state.type.toString(),
                this.state.fuel_type.id.toString(),
                this.state.name.toString(),
                this.state.maximum.toString(),
                this.state.minimum.toString(),

                this.state.rfid.toString(), //fake rfid
            ]


            console.log("------Unit-------")
            console.log(unit)
            console.log("------end Unit-------")
            DBHelper.add('Unit', unit)

            // id: 'int',    // primary key
            // workorder_id: 'int', // could be foreign key
            // unit_number: 'int',
            // amount: 'int',
            // fill: 'int',
            // fuel_id: 'int',
            // rfid: 'string',
            // status: 'string'
            var fill = 0
            if (this.state.fill) {
                fill = -1
            } else {
                fill = parseFloat(this.state.amount)
            }

            let task = [
                DBHelper.getAll('Task').length + 2,
                this.state.workorder_id,
                this.state.number,
                0,
                fill,
                this.state.fuel_type.id,
                //this.state.rfid,
                '0',
            ]

            console.log("------task-------")
            console.log(task)
            console.log("------end task-------")

            DBHelper.add('Task', task)

            this.props.navigation.navigate('Details', {workorder_id: this.props.navigation.state.params.wo_id})
        }
    }

    checkInputList() {
        var error = false
        var incorrect = ''
        if (this.state.name == '' || this.state.name == null) {
            error = true
            incorrect = 'Missing Unit Name\n'
        }
        if (this.state.number == '' || this.state.number == null) {
            error = true
            incorrect = incorrect + 'Missing Unit Number\n'
        } else {
            try {
                let integer = parseInt(this.state.number)
                if (isNaN(integer)) {
                    error = true
                    incorrect = incorrect + 'Invalid Unit Number\n'
                }
            } catch(error) {
                error = true
                incorrect = incorrect + 'Invalid Unit Number\n'
            }
        }

        var validMax = true
        var validMin = true
        if (this.state.maximum == '' || this.state.maximum == null) {
            error = true
            incorrect = incorrect + 'Missing Maximum\n'
            validMax = false
        } else {
            try {
                let integer = parseFloat(this.state.maximum)
                if (isNaN(integer)) {
                    error = true
                    incorrect = incorrect + 'Invalid Maximum\n'
                    validMax = false
                }
            } catch(error) {
                error = true
                incorrect = incorrect + 'Invalid Maximum\n'
                validMax = false
            }
        }
        if (this.state.minimum == '' || this.state.minimum == null) {
            error = true
            incorrect = incorrect + 'Missing Minimum\n'
            validMin = false
        } else {
            try {
                let integer = parseFloat(this.state.minimum)
                if (isNaN(integer)) {
                    error = true
                    incorrect = incorrect + 'Invalid Minimum\n'
                    validMin = false
                }
            } catch(error) {
                error = true
                incorrect = incorrect + 'Invalid Minimum\n'
                validMin = false
            }
        }

        if (validMax && validMin) {
            let max = parseFloat(this.state.maximum)
            let min = parseFloat(this.state.minimum)
            if (max < min) {
                error = true
                incorrect = incorrect + 'Maximum cannot be less than Minimum\n'
            } else if (max == min) {
                error = true
                incorrect = incorrect + 'Maximum cannot be equal to Minimum\n'
            }
        }

        if (this.state.type == null) {
            error = true
            incorrect = incorrect + 'Missing Unit Type\n'
        }
        if (this.state.fuel_type == null) {
            error = true
            incorrect = incorrect + 'Missing Fuel Type\n'
        }

        var validAmount = true
        if (this.state.fill == false && (this.state.amount == '' || this.state.amount == null)) {
            error = true
            incorrect = incorrect + 'Missing Amount'
            validAmount = false
        } else if (this.state.fill == false) {
            try {
                let integer = parseFloat(this.state.amount)
                if (isNaN(integer)) {
                    error = true
                    incorrect = incorrect + 'Invalid Amount\n'
                    validAmount = false
                }
            } catch(error) {
                error = true
                incorrect = incorrect + 'Invalid Unit Amount\n'
                validAmount = false
            }
        }
        if (this.state.fill == false && validAmount && validMax && validMin) {
            let amount = parseFloat(this.state.amount)
            let max = parseFloat(this.state.maximum)
            let min = parseFloat(this.state.minimum)

            if (amount > max) {
                error = true
                incorrect = incorrect + 'Amount cannot be greater than Maximum\n'
            } else if (amount < min) {
                error = true
                incorrect = incorrect + 'Amount cannot be less than Minimum\n'
            }
        }


        if (error) {
            Alert.alert(
                'Failed to Add Unit',
                'Errors: \n' + incorrect,
                [
                    { text: 'Ok', onPress: () => console.log() }
                ],
                { cancelable: false }
            )
        }
        return error
    }

    render() {
        return (
            <View behavior="padding" style={styles.container}>
                <View style={styles.content}>
                    <ScrollView style={styles.scrollContainer}>
                        {/* Wrapped in a TouchableOpacity to close the keyboard */}
                        <TouchableOpacity activeOpacity={1} onPress={() => Keyboard.dismiss()}>  
                            <KeyboardAvoidingView behavior='padding'>
                                {/* All of the checklist get displayed here */}
                                <View style={styles.itemContainer}>
                                    <View style={styles.rowLayout}>
                                        <View style={styles.left}>
                                            <Text>Unit Name:</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <TextInput
                                                style={styles.textField}
                                                onChangeText={(text) => this.setState({name: text})}
                                                editable={true}
                                                placeholderTextColor='#BFBFBF'
                                                placeholder='Name'
                                                value={this.state.name}
                                            >
                                            </TextInput>
                                        </View>
                                    </View>
                                    <View style={styles.rowLayout}>
                                        <View style={styles.left}>
                                            <Text>Unit Number:</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <TextInput
                                                style={styles.textField}
                                                onChangeText={(text) => this.setState({number: text})}
                                                editable={true}
                                                placeholderTextColor='#BFBFBF'
                                                placeholder='#'
                                                value={this.state.number}
                                                keyboardType='numeric'
                                            >
                                            </TextInput>
                                        </View>
                                    </View>
                                    <View style={styles.rowLayout}>
                                        <View style={styles.left}>
                                            <Text>Client:</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <ModalDropdown
                                                style={[styles.dropdown, styles.disabledView]}
                                                textStyle={styles.dropdownText}
                                                dropdownStyle={styles.dropdownBox}
                                                dropdownTextStyle={styles.dropdownOption}
                                                dropdownTextHighlightStyle={styles.dropdownSelectedOption}
                                                options={this.getAllClients()}
                                                defaultIndex={this.getClient()[0] - 1}
                                                defaultValue={this.getClient()[1]}
                                                onSelect={(index, value) => this.selectClient(index, value)}
                                                disabled={true}
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.rowLayout}>
                                        <View style={styles.left}>
                                            <Text>Location:</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <TextInput
                                                style={[styles.textField, styles.disabledField]}
                                                onChangeText={(text) => this.setState({location: text})}
                                                editable={false}
                                                placeholderTextColor='#BFBFBF'
                                                placeholder='Current Location'
                                                value={this.state.location}
                                            >
                                            </TextInput>
                                        </View>
                                    </View>
                                    <View style={styles.rowLayout}>
                                        <View style={styles.left}>
                                            <Text>Maximum:</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <TextInput
                                                style={styles.textField}
                                                onChangeText={(text) => this.setState({maximum: text})}
                                                editable={true}
                                                placeholderTextColor='#BFBFBF'
                                                placeholder='#'
                                                value={this.state.maximum}
                                                keyboardType='numeric'
                                            >
                                            </TextInput>
                                        </View>
                                    </View>
                                    <View style={styles.rowLayout}>
                                        <View style={styles.left}>
                                            <Text>Minimum:</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <TextInput
                                                style={styles.textField}
                                                onChangeText={(text) => this.setState({minimum: text})}
                                                editable={true}
                                                placeholderTextColor='#BFBFBF'
                                                placeholder='#'
                                                value={this.state.minimum}
                                                keyboardType='numeric'
                                            >
                                            </TextInput>
                                        </View>
                                    </View>
                                    <View style={styles.rowLayout}>
                                        <View style={styles.left}>
                                            <Text>Type:</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <ModalDropdown
                                                style={styles.dropdown}
                                                textStyle={styles.dropdownText}
                                                dropdownStyle={styles.dropdownBox}
                                                dropdownTextStyle={styles.dropdownOption}
                                                dropdownTextHighlightStyle={styles.dropdownSelectedOption}
                                                options={this.getUnitType()}
                                                defaultIndex={-1}
                                                defaultValue={'Select a Unit Type'}
                                                onSelect={(index, value) => this.selectUnitType(index, value)}
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.rowLayout}>
                                        <View style={styles.left}>
                                            <Text>Fuel Type:</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <ModalDropdown
                                                style={styles.dropdown}
                                                textStyle={styles.dropdownText}
                                                dropdownStyle={styles.dropdownBox}
                                                dropdownTextStyle={styles.dropdownOption}
                                                dropdownTextHighlightStyle={styles.dropdownSelectedOption}
                                                options={this.getFuelType()}
                                                defaultIndex={-1}
                                                defaultValue={'Select a Fuel Type'}
                                                onSelect={(index, value) => this.selectFuelType(index, value)}
                                            />
                                        </View>
                                    </View>


                                    {/* WORKORDER INFORMATION */}
                                    <View style={styles.infoContainer}>
                                        <View style={styles.rowLayout}>
                                            <View style={styles.left}>
                                                <Text>Work Order:</Text>
                                            </View>
                                            <View style={styles.right}>
                                                <TextInput
                                                    style={[styles.textField, styles.disabledField]}
                                                    onChangeText={(text) => this.setState({workorder_id: text})}
                                                    editable={false}
                                                    placeholderTextColor='#BFBFBF'
                                                    placeholder='#'
                                                    value={this.state.workorder_id}
                                                    keyboardType='numeric'
                                                >
                                                </TextInput>
                                            </View>
                                        </View>
                                        <View style={styles.rowLayout}>
                                            <View style={styles.left}>
                                                <Text>Amount:</Text>
                                            </View>
                                            <View style={styles.right}>
                                                <TextInput
                                                    style={[styles.textField, this.fillUnit()[1]]}
                                                    onChangeText={(text) => this.setState({amount: text})}
                                                    editable={this.fillUnit()[0]}
                                                    placeholderTextColor='#BFBFBF'
                                                    placeholder='#'
                                                    value={this.state.amount}
                                                    keyboardType='numeric'
                                                >
                                                </TextInput>
                                            </View>
                                        </View>
                                        <View style={styles.rowLayout}>
                                            <View style={styles.left}>
                                                <Text>Fill:</Text>
                                            </View>
                                            <View style={[styles.right, {alignItems: 'flex-start'}]}>
                                                <Checkbox
                                                    onSelect={() => this.clickedCheckBox()}
                                                    disabled={false}
                                                    disabledColor='#247fd2'
                                                    selected={this.state.fill}
                                                    defaultColor='white'
                                                    selectedColor='#446CB3'
                                                    checkboxStyle={styles.checkboxStyle}
                                                >
                                                    <Text>{this.getCheckBoxText()}</Text>
                                                </Checkbox>
                                            </View>
                                        </View>
                                    </View>

                                    {/* SCANNING RFID */}
                                    {/* <View style={styles.rfidContainer}>
                                        <TouchableOpacity style={styles.rfidButtonContainer} onPress={() => console.log('READ RFID')}>
                                            <Text style={styles.rfidText}>Scan RFID</Text>
                                        </TouchableOpacity>

                                        <Text>0A32F5H3J832D2...</Text>
                                    </View> */}
                                </View>

                                {/* Confirmation Button */}
                                <View style={styles.confirmContainer}>
                                    <TouchableOpacity style={styles.confirmButtonContainer} onPress={() => this.addUnit()}>
                                        <Text style={styles.confirmText}>Add Unit</Text>
                                    </TouchableOpacity>
                                </View>
                            </KeyboardAvoidingView>
                        </TouchableOpacity>
                    </ScrollView>
                </View >

                {/* Unit header */}
                <Header nav={this.props.navigation} wo_id={this.props.navigation.state.params.wo_id}/>
            </View>
        );
    }
}

//Gets the size of the mobile device (in pixel's)
const window = Dimensions.get('window');

const styles= StyleSheet.create({
    container: {
        flex: 1, //like bootstraps xs-lg-12 but without dropping down
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        position: 'absolute',
        alignItems: 'center',
        width: window.width,
        top: 70
    },
    scrollContainer: {
        height: window.height - 70,
    },
    rowLayout: {
        flexDirection: 'row',
        paddingBottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 10,
    },
    rowStyles: {
        borderBottomWidth: 1,
        borderColor: '#BDC3C7',
        margin: 8,
        marginBottom: 0,
        paddingBottom: 8,
    },

    left: {
        flex: 0.4,
        alignItems: 'center',
        paddingRight: 10,
        justifyContent: 'center',
    },
    right: {
        flex: 0.6,
        alignItems: 'center',
        paddingLeft: 10,
    },
  
    mainText: {
        color: 'white',
        fontSize: 16,
        fontWeight: '700',
    },
    secondaryText: {
        color: 'white',
        fontSize: 14,
        fontWeight: '600',
    },

    confirmButtonContainer: {
        backgroundColor: '#ffa726',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: window.width - window.width/3,
        borderRadius: 5,
        margin: 20,
    },
    confirmText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: '700',
    },
    confirmContainer: {
        width: window.width,
        alignItems: 'center',
    },

    textContainer: {
        margin: 5,
        width: (window.width / 20) * 9,
    },
    textField: {
        padding: 2,
        paddingLeft: 5,
        backgroundColor: 'white',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#ECECEC',
        height: 30,
        fontSize: 15,
    },

    disabledView: {
        backgroundColor: 'grey',
    },
    disabledField: {
        backgroundColor: 'grey',
        color: '#BFBFBF',
    },

    checkboxStyle: {
        margin: 2,
        width: 26,
        height: 26,
        borderWidth: 2,
        borderColor: '#ddd',
        borderRadius: 5
    },

    itemContainer: {
        paddingTop: 10,
        width: window.width,
        alignItems: 'center',
    },
    infoContainer: {
        paddingTop: 10,
        width: window.width,
        backgroundColor: '#e6e6e6',
        borderWidth: 1,
        borderColor: 'grey',
    },


    dropdownContainer: {
        backgroundColor: '#e6eef2',
        borderRadius: 5,
        height: 40,
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 10,
        paddingRight: 5,
        paddingLeft: 5,
    },

    dropdown: {
        //Could add styles later
        borderWidth: 1,
        borderColor: '#ECECEC',
        paddingLeft: 0,
        width: (window.width / 20) * 11,
    },
    dropdownBox: {
        marginTop: 5,
        padding: 2,
        width: (window.width / 20) * 11,
        borderRadius: 5,
    },
    dropdownText: {
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 5,
        fontSize: 15,
        color: '#BFBFBF',
    },
    dropdownOption: {
        fontSize: 12,
    },
    dropdownSelectedOption: {
        color: 'black',
        fontWeight: '700',
        fontSize: 12,
    },
});
