import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button, Alert} from 'react-native';

import List from './List';
import Header from './Header';
import Footer from '../Footer/Footer';
import DBHelper from '../../DBHelper';
/**
 * Parent Component
 *
 * Displays a list of workorder with two tabs from child
 */
export default class WorkOrder extends Component {

    constructor(props) {
        super(props)
    }

    /**
     * Checks if the driver wishes to end his current shift or trip.
     */
    alertEndingShift() {
        Alert.alert(
            'Ending Trip',
            'Are you sure you wish to end your Trip?',
            [
                { text: 'Yes', onPress: () => this.endCurrentShift() },
                { text: 'No', onPress: () => console.log() },

            ],
            { cancelable: false }
        )
    }

    /**
     * Resets the data within the database if the driver
     * wishes to end his current trip.
     */
    endCurrentShift() {
        //TEMPORARY UNTIL INTEGRATION WITH FMS
        //this.props.navigation.navigate('Refresh')
        //let default_Driver = DBHelper.getAll('Driver')[0]
        //let default_Driver = DBHelper.getById('Trip',1)

        let default_Driver = DBHelper.getAll('Trip').filtered('active =1')[0]

        let data = DBHelper.getAll('WorkOrder').filtered('status != "4" AND trip_id ="' + default_Driver.id + '"')
        if (data.length > 0) {

            //DBHelper.updateElementAndById('Trip', default_Driver.id, 'status', 'incompleted')
            DBHelper.updateElementAndById('Trip', default_Driver.id, 'status', 'completed')

            //This is to compelete the selected workorder and asks the driver to leave a note when closing.
            //this.completeLeaveNote()
        }else{

            DBHelper.updateElementAndById('Trip', default_Driver.id, 'status', 'completed')
        }
        this.props.navigation.navigate('WorkOrder')

    }

    render() {
        return (
            <View style={styles.container}>
                {/*Child component list*/}
                <List nav={this.props.navigation}/>
                {/*Child component header*/}
                <Header endDay={() => this.alertEndingShift()}/>
                {/*Child component footer*/}
                <Footer nav={this.props.navigation} truck={1} map={1} workorder={0} settings={1}/>
            </View>
        );
    }
}

const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E2E2E3',
        alignItems: 'center',
    },
});
