import React, { Component} from 'react';
import { StyleSheet, View, Image, Text, Button } from 'react-native';
import DBHelper from '../DBHelper';

/**
 *
 * THIS NOT USED IN THE CURRENT VERSION AT ALL!
 *
 */
export default class Debug extends Component {
  constructor(props) {
    super(props)
  }

  testing1() {
    console.log("testing1");
    DBHelper.printAllUsers();
  }

  testing2() {
    console.log("testing2");
    DBHelper.addUser('TeiWeb', 'TEIweb', 'Tei', 'Web', 'Web Solutions', 52);
  }

  testing3() {
    console.log("testing3");
    DBHelper.updateUser(0, 'Dalton')
  }

  testing4() {
    console.log("testing4");
    DBHelper.printUser(0);
  }

  testing5() {
    console.log("testing5");
    DBHelper.removeAllUsers();
  }

  testing6() {
    console.log("testing6");
    DBHelper.removeUser(0);
  }

  render() {
    //DEBUG: must comment this out when going strait to debug screen
    //const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        {/* Login form goes here, needs to be created as a component */}
        <Text style={styles.title}>
          Debug Pannel
        </Text>
        <Button
          onPress={() => this.testing1()}
          title="Print Number of Users"
          color="#FFF"
        />
        <Button
          onPress={() => this.testing2()}
          title="Add a User"
          color="#FFF"
        />
        <Button
          onPress={() => this.testing3()}
          title="Update User"
          color="#FFF"
        />
        <Button
          onPress={() => this.testing4()}
          title="Printing User 0"
          color="#FFF"
        />
        <Button
          onPress={() => this.testing5()}
          title="Remove All Users"
          color="#FFF"
        />
        <Button
          onPress={() => this.testing6()}
          title="Remove User 0"
          color="#FFF"
        />
      </View>
    );
  }
}

const styles= StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333333',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: '#F9690E',
    marginTop: 10,
    width: 300,
    textAlign: 'center',
    opacity: 1,
    fontSize: 30
  },
});