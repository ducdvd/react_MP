'use strict';


/**
 * External function when provided with a boolean function (true or false)
 * It will allow the component wrapped within this function (renderif)
 * to be displayed (true) or not displayed at all (false)
 *
 * Example:
 *    //Outside render()
 *    functionTrue() {
 *      return true;
 *    }
 *
 *    //Inside render()
 *    {renderif(functionTrue()(
 *        <Text>This will be displayed</Text>
 *    )}
 * 
 *
 * @param {*true or false function} input 
 */
const isFunction = input => typeof input === 'function';
export default predicate => elemOrThunk =>
  predicate ? (isFunction(elemOrThunk) ? elemOrThunk() : elemOrThunk) : null;