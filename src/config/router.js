import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';

import { Easing, Animated} from 'react-native';

import Login from '../../src/components/Login/Login';
import Map from '../../src/components/Map/Map';
import Truck from '../../src/components/Truck/Truck';
import Checklist from '../../src/components/Checklist/Checklist';
import AddingFuel from '../../src/components/Truck/Fueling/AddingFuel';
import Change from '../../src/components/Truck/Change/Change';
import WorkOrder from '../../src/components/WorkOrder/WorkOrder';
import Details from '../../src/components/WorkOrder/Details/Details';
import Scanning from '../../src/components/WorkOrder/Scanning/Scanning';
import Manual from '../../src/components/WorkOrder/Manual/Manual';
import Signature from '../../src/components/WorkOrder/Signature/Signature';
import Dispensed from '../../src/components/WorkOrder/Dispensed/Dispensed';
import Note from '../../src/components/WorkOrder/Note/Note';
import NoteMain from '../../src/components/WorkOrder/NoteMain/NoteMain';
import NoteOpenTK from '../../src/components/WorkOrder/NoteOpenTK/NoteOpenTK';
import NoteCloseTK from '../../src/components/WorkOrder/NoteCloseTK/NoteCloseTK';
import LeaveNote from '../../src/components/WorkOrder/LeaveNote/LeaveNote';
import ResertDB from '../../src/components/ResertDB/ResertDB';
import Settings from '../../src/components/Settings/Settings';
import Print from '../../src/components/Print/PrintWO';
import Refresh from '../../src/components/Refresh/Refresh';
import About from '../../src/components/Settings/About/About';
import Terms from '../../src/components/Settings/Terms/Terms';
import Privacy from '../../src/components/Settings/Privacy/Privacy';
import Debug from './debug';
import ErrorHandler from '../../src/components/ErrorHandler/ErrorHandler';
import Unit from '../../src/components/WorkOrder/Unit/Unit';
import Trip from '../../src/components/Trip/Trip';
import Preview from '../../src/components/Trip/Preview/Preview';

/*
    Allows mobile application to switch to and from views
    Must import and include the new Componet to the App for the application to
    be able to navigate to that given Component (Parent Component)

    Documentation:
        https://reactnavigation.org/docs/intro/
        https://facebook.github.io/react-native/docs/navigation.html
*/
export const App = StackNavigator({
    Login: { //Idenifier
        screen: Login, //View or Component import'ed
        navigationOptions: { //Additional options
            header: null, //Disables the top banner (with the idenifier)
        },
    },
    Map: {
        screen: Map,
        navigationOptions: {
            header: null,
        },
    },
    Truck: {
        screen: Truck,
        navigationOptions: {
            header: null,
        },
    },
    WorkOrder: {
        screen: WorkOrder,
        navigationOptions: {
            header: null,
        },
    },
    Settings: {
        screen: Settings,
        navigationOptions: {
            header: null,
        },
    },
    Debug: {
        screen: Debug,
        navigationOptions: {
            header: null,
        },
    },
    Fueling: {
        screen: AddingFuel,
        navigationOptions: {
            header: null,
        },
    },
    Change: {
        screen: Change,
        navigationOptions: {
            header: null,
        }
    },
    Details: {
        screen: Details,
        navigationOptions: {
            header: null,
        }
    },
    Scanning: {
        screen: Scanning,
        navigationOptions: {
            header: null,
        }
    },
    Manual: {
        screen: Manual,
        navigationOptions: {
            header: null,
        }
    },
    Dispensed: {
        screen: Dispensed,
        navigationOptions: {
            header: null,
        }
    },
    Note: {
        screen: Note,
        navigationOptions: {
            header: null,
        }
    },
        NoteMain: {
            screen: NoteMain,
            navigationOptions: {
                header: null,
            }
        },
        NoteOpenTK: {
            screen: NoteOpenTK,
            navigationOptions: {
                header: null,
            }
        },
        NoteCloseTK: {
            screen: NoteCloseTK,
            navigationOptions: {
                header: null,
            }
        },
        LeaveNote: {
            screen: LeaveNote,
            navigationOptions: {
                header: null,
            }
        },
        ResertDB: {
            screen: ResertDB,
            navigationOptions: {
                header: null,
            }
        },
        Checklist: {
        screen: Checklist,
        navigationOptions: {
            header: null,
        }
    },
    Signature: {
        screen: Signature,
        navigationOptions: {
            header: null,
        }
    },
    About: {
        screen: About,
        navigationOptions: {
            header: null,
        }
    },
    Terms: {
        screen: Terms,
        navigationOptions: {
            header: null,
        }
    },
    Privacy: {
        screen: Privacy,
        navigationOptions: {
            header: null,
        }
    },
    Refresh: {
        screen: Refresh,
        navigationOptions: {
            header: null,
        }
    },
    ErrorHandler: {
        screen: ErrorHandler,
        navigationOptions: {
            header: null,
        }
    },
    Print: {
        screen: Print,
        navigationOptions: {
            header: null,
        }
    },
    Unit: {
        screen: Unit,
        navigationOptions: {
            header: null,
        }
    },
    Trip: {
        screen: Trip,
        navigationOptions: {
            header: null,
        }
    },
    Preview: {
        screen: Preview,
        navigationOptions: {
            header: null,
        }
    }
},
    {
        transitionConfig: () => ({
            transitionSpec: {
                duration: 250,
                easing: Easing.out(Easing.poly(4)),
                timing: Animated.timing,
            },

            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps
                const { index } = scene

                // const height = layout.initHeight

                // const translateY = position.interpolate({
                //     inputRange: [index - 1, index, index + 1],
                //     outputRange: [height, 0, 0],
                // })

                const opacity = position.interpolate({
                    inputRange: [index - 1, index - 0.99, index],
                    outputRange: [0, 1, 1],
                })

                return { opacity }
            },
        }),
    }
);


